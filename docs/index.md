# Auth Management Package

This package is intended to be an encapsulated framework for handling multi-tenancy management of users and the groups/clients/organizations that they belong to.

It can operate in one of 3 modes:
- single-user : This means we'll have a single organization that all clients belong to, and there's a 1:1 mapping of users to clients (each client has only a single user).
  - If someone tries to sign-up when they already have an account - they'll be told to sign-in, and prevented from signing up for a new account.
- multi-user  : This means we'll have a single organization that all clients belong to, and there's a 1:many mapping of clients to users (each client can have multiple users).
  - If someone wants to sign up for a new account, they get a new client and a new user, and are the primary admin for that client.
  - If they want to sign up for a worker account on an existing client, they must receive an invite from the existing client.
- corporate   : This means we'll have multiple organizations, and each organization can have multiple clients, and each client can have multiple users.
  - If someone signs up for a new account, they get a new organization and a new client and a new user, and are the primary admin for the organization and that client.
  - Rules for multi-user above apply for the single client that they have.
  - They can create new clients and 'switch' to those clients, and then it's just like the multi-user rules above.

The mode that we operate in will be single-user by default, but can be overridden via config settings.

## Code layout
- Inside `Core` you'll find the base interfaces and abstract classes that everything is built upon.
- The application is built around 'Services' (could also be considered 'Modules').  Each service/module has its own root-level folder underneath `src/Modules`.  For example, the Widgets service/module would be in `src/Modules/Widgets`.  We chose to do it this way so that our directory structure is packaged by component, rather than by infrastructure.  This is more brain-friendly and allows for someone to quickly locate/identify code related to a particular service.  Also makes it easy to split a service/module off to a true microservice with its own codebase later, if desired.
  - Underneath the top-level service/module the structure looks like this:
    - `Entities` folder
      - This contains any Entity-specific classes.  These should extend the `src/Core/BaseEntity.php` class.  E.g. you could have a `src/Modules/Widgets/Entities/Widget.php` class in here.
    - `Internal` folder
      - This contains any Repository-specific or Filter-specific classes.  These should extend the `src/Core/BaseRepository.php` class.  E.g. you could have `src/Modules/Widgets/Internal/WidgetRepository.php` class in here.
      - Any ancillary classes related to or helping the Service class (that are only meant to be consumed/used by the Service) could also be contained here.
  - The Service specific classes. E.g. `src/Modules/Widgets/WidgetService.php` reside at the top of the `src/Modules/Widget` folder.