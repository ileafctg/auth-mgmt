<?php
namespace IleafCtg\AuthMgmt;

class AuthMgmtConfigOptions {
    
    const AUTH_MGMT_MODE_SINGLE_USER = 'single-user';
    const AUTH_MGMT_MODE_MULTI_USER  = 'multi-user';
    const AUTH_MGMT_MODE_CORPORATE   = 'corporate';
    
    const AUTH_MGMT_DEFAULT_ORG_ID = 1; // This is set up as part of the 0_initialization.php migration script in resources/migrations. You ran that, right?
    
}