<?php

namespace IleafCtg\AuthMgmt\Core\Exceptions;

use Exception;

class NotImplementedYetException extends ApplicationException {
    public function __construct($message = "Not implemented yet", $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}