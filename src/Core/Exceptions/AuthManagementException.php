<?php

namespace IleafCtg\AuthMgmt\Core\Exceptions;

use Exception;

class AuthManagementException extends Exception {
    
    public function __construct($message, $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }
    
}