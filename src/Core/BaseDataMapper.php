<?php

namespace IleafCtg\AuthMgmt\Core;

use IleafCtg\AuthMgmt\Core\BaseDataMapperSupplemental\iDataMapper;
use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\iEntity;
use IleafCtg\AuthMgmt\Core\Exceptions\ApplicationException;

abstract class BaseDataMapper implements iDataMapper {
    
    /** @var array entity to repo DTO mappings of fields.  This is what is used to either hydrate an entity or
     *  to construct a repo record.
     */
    protected array $entityToRepoDtoFieldMap = [];
    /** @var array fields that must be present on the repo side of the entity to repo DTO mappings */
    protected array $requiredRepoDtoFields = [];
    
    /** @var array Fields that must be present on the entity side of the repo to entity DTO mappings */
    protected array $requiredEntityDtoFields = [];
    
    protected static $instances;
    
    
    
    /**
     * To prevent new-ing up, we'll make this private, so that instance() must be used.
     */
    private function __construct() {
        
    }
    
    public static function instance() {
        $class = get_called_class();
        
        if (!isset(static::$instances[$class])) {
            // Instantiate the singleton
            static::$instances[$class] = new static();
        }
        
        return static::$instances[$class];
    }
    
    
    
    /**
     * Returns the DTO field corresponding to the entity field name.
     * @param $entityFieldName
     *
     * @return string
     */
    public function getEntityToRepoFieldName(string $entityFieldName): string {
        return $this->entityToRepoDtoFieldMap[$entityFieldName];
    }
    
    
    /**
     * Takes the given entity and transforms it into a Repo DTO, using what is defined in $fields
     *
     * @param iEntity $entity
     *
     * @return array
     * @throws ApplicationException
     */
    public function entityToRepoDto(iEntity $entity): array {
        $out = [];
        
        foreach ($this->entityToRepoDtoFieldMap as $entityField => $repoField) {
            // If not set, we'll skip, which should have the end result of leaving any existing value as is
            // when the DTO is used by the repo layer to persist the data.  If there needs to be a default value,
            // that should be handled at the Entity level in its entityToRepo() override/extender.
            if (isset($entity->$entityField)) {
                $out[$repoField] = $entity->$entityField;
            }
        }
        
        // Enforce the presence of whatever fields are considered 'required' for the repo DTO.
        foreach ($this->requiredRepoDtoFields as $requiredField) {
            if (!isset($out[$requiredField])) {
                throw new ApplicationException("Required field {$requiredField} in your Repo DTO is not set");
            }
        }
        
        return $out;
    }
    
    
    
    /**
     * Takes the given $data and transforms it into an entity DTO, using what is defined in $fields.
     *
     * @param array $data
     *
     * @return array
     * @throws ApplicationException
     */
    public function repoToEntityDto(array $data): array {
        $out = [];
        
        foreach ($this->entityToRepoDtoFieldMap as $entityField => $repoField) {
            $out[$entityField] = $data[$repoField];
        }
        
        foreach ($this->requiredEntityDtoFields as $requiredField) {
            if (!isset($out[$requiredField])) {
                throw new ApplicationException("Required field {$requiredField} in your Entity DTO is not set");
            }
        }
        
        return $out;
    }
    
    
}