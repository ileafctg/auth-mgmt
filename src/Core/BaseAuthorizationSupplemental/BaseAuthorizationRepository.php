<?php

namespace IleafCtg\AuthMgmt\Core\BaseAuthorizationSupplemental;

use Aura\Sql\ExtendedPdoInterface;
use IleafCtg\AuthMgmt\Core\BaseDataMapperSupplemental\iDataMapper;
use IleafCtg\AuthMgmt\Core\Exceptions\ApplicationException;
use IleafCtg\AuthMgmt\Modules\GroupMgmt\Entities\GroupDataMapper;
use IleafCtg\PdoSimple\PdoSimple;

class BaseAuthorizationRepository {
    
    protected PdoSimple $pdoSimple;
    
    public function __construct(ExtendedPdoInterface $pdo) {
        if (!empty($pdo)) {
            $this->pdoSimple = new PdoSimple($pdo);
        }
    }
    
    public function getUserAuthInfo(int $userId, int $clientId) {
        // Get the user details, client details
        
        $query = " 
            SELECT
                mt_users.id,
                mt_users.first_name,
                mt_users.last_name,
                mt_users.group_id,
                mt_users.client_id,
                mt_clients.name as client_name,
                mt_clients.organization_id,
                mt_organizations.name as organization_name
                   
            FROM mt_users
            JOIN mt_clients ON (mt_users.client_id = mt_clients.id)
            JOIN mt_organizations ON (mt_clients.organization_id = mt_organizations.id)
            
            WHERE mt_users.enabled = 1
             AND mt_users.deleted = 0
             AND mt_clients.deleted = 0
             AND mt_organizations.deleted = 0
             AND mt_users.client_id = :client_id
             AND mt_users.id = :user_id
        ";
        
        $where = [
            'user_id' => $userId,
            'client_id' => $clientId,
        ];

        $details = $this->pdoSimple->getPdoInstance()->fetchAssoc($query, $where);    
        
        if (!$details) {
            throw new ApplicationException("Invalid user_id/client_id {$userId}/{$clientId}, could not fetch user auth info");
        }
        
        // FIXME: Get the roles and permissions associated with the user.
        
        return $details;
    }
    
    
}