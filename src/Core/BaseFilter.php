<?php

namespace IleafCtg\AuthMgmt\Core;

use DateTime;
use IleafCtg\AuthMgmt\Core\BaseRepoSupplemental\iFilter;
use IleafCtg\AuthMgmt\Core\Exceptions\ApplicationException;

/**
 * @property array $ids
 * @property bool $deleted
 * @property DateTime $modifiedSince
 *                                  
 * @property string $orderBy
 */
abstract class BaseFilter implements iFilter {
    
    public array $ids = [];
    public bool $deleted = false;
    public DateTime $modifiedSince;
    
    public string $orderBy = '';
    protected int $pageNumber = 1;
    protected int $limit = 50;
    
    const MAX_LIMIT = 50;
    
    
    
    /**
     * Set pageNumber.
     * 
     * @param int $pageNumber
     *
     * @throws ApplicationException
     */
    public function setPageNumber(int $pageNumber) {
        if ($pageNumber <= 0) {
            throw new ApplicationException("You must specify a page number of 1 or higher");
        }    
        $this->pageNumber = $pageNumber;
    }
    
    
    
    /**
     * Get the page number of results you want returned when using this filter to query the repository layer.
     * 
     * @return int
     */
    public function getPageNumber(): int {
        return ($this->pageNumber > 0 ? $this->pageNumber : 1);
    }
    
    
    
    /**
     * Set the filter limit.
     * 
     * @param int $limit
     *
     * @throws ApplicationException
     */
    public function setLimit(int $limit) {
        if ($limit > BaseFilter::MAX_LIMIT) {
            throw new ApplicationException("Max limit you may request is " . BaseFilter::MAX_LIMIT);
        }
        $this->limit = $limit;
    }
    
    
    /**
     * Get the limit to be used with this filter when querying the repository layer.
     * 
     * @return int
     */
    public function getLimit(): int {
        return ($this->limit <= static::MAX_LIMIT ? $this->limit : static::MAX_LIMIT);
    }
    
    
}