<?php

namespace IleafCtg\AuthMgmt\Core;


use DateTime;
use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\BaseEntityTrait;
use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\iEntity;
use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\PropInfo;

abstract class BaseEntity implements iEntity {
    // Utilize our magic setter/getter logic
    use BaseEntityTrait;
    
    /** @var array Used to store property read/write accessibility and property types */
    protected $propertyMap = [];
    
    // Common properties to every entity.
    /** @var int */
    protected $id;
    /** @var DateTime */
    protected $ctime;
    /** @var DateTime */
    protected $mtime;
    
    public function __construct() {
        $this->ensurePropertyMap();
    }
    
    public function initProps() {
        $this->addProp('id', PropInfo::INT, PropInfo::READ_ONLY);
        $this->addProp('ctime', PropInfo::DATETIME, PropInfo::READ_ONLY);
        $this->addProp('mtime', PropInfo::DATETIME, PropInfo::READ_ONLY);
    }
    
    
    public static function hydrateFromArray($data): iEntity {
        $entity = new static();
        if ($data !== null) {
            $entity->bulkAssignProps($data);
        }
        return $entity;
    }
    
}