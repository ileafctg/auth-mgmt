<?php

namespace IleafCtg\AuthMgmt\Core;

use IleafCtg\AuthMgmt\Core\Exceptions\ApplicationException;
use IleafCtg\AuthMgmt\Utils\Parser;
use RuntimeException;

class MemorySession implements Auth\iSession {
    /** @var array */
    private $storage;
    
    /**
     * @var string
     */
    private $id = '';
    
    /**
     * @var bool
     */
    private $started = false;
    
    /**
     * @var string
     */
    private $name = '';
    
    /**
     * @var array
     */
    private $config = [];
    
    /**
     * @var array
     */
    private $cookie = [];
    
    /**
     * The constructor.
     */
    public function __construct()
    {
        $this->storage = [];
        
        $this->setCookieParams(0, '/', '', false, true);
        
        $config = [];
        foreach ((array)ini_get_all('session') as $key => $value) {
            $config[substr($key, 8)] = $value['local_value'];
        }
        
        $this->setOptions($config);
    }
    
    
    
    /**
     * {@inheritdoc}
     */
    public function start(): void
    {
        if (!$this->id) {
            $this->regenerateId();
        }
        
        $this->started = true;
    }
    
    
    
    /**
     * {@inheritdoc}
     */
    public function isStarted(): bool
    {
        return $this->started;
    }
    
    
    
    /**
     * {@inheritdoc}
     */
    public function regenerateId(): void
    {
        $this->id = str_replace('.', '', uniqid('sess_', true));
    }
    
    
    
    /**
     * {@inheritdoc}
     */
    public function destroy(): void
    {
        $this->clear();
        $this->regenerateId();
    }
    
    
    
    /**
     * {@inheritdoc}
     */
    public function getId(): string
    {
        return $this->id;
    }
    
    
    
    /**
     * {@inheritdoc}
     */
    public function setId(string $id): void
    {
        if ($this->isStarted()) {
            throw new RuntimeException('Cannot change session id when session is active');
        }
        
        $this->id = $id;
    }
    
    
    
    /**
     * {@inheritdoc}
     */
    public function getSessionName(): string
    {
        return $this->name;
    }
    
    
    
    /**
     * {@inheritdoc}
     */
    public function setSessionName(string $name): void
    {
        if ($this->isStarted()) {
            throw new RuntimeException('Cannot change session name when session is active');
        }
        $this->name = $name;
    }
    
    
    
    /**
     * {@inheritdoc}
     */
    public function has($key): bool
    {
        if (empty($this->storage)) {
            return false;
        }
        
        $current = &$this->storage;
        
        $flatArray = Parser::dottedStringToFlatArray($key);
        foreach ($flatArray as $key) {
            if (!isset($current[$key])) {
                return false;
            }
            $current = &$current[$key];
        }
        
        if (isset($current)) {
            return true;
        }
        return false;
    }
    
    
    
    /**
     * {@inheritdoc}
     */
    public function get($key)
    {
        if ($this->has($key)) {
            $current = &$this->storage;
            
            $flatArray = Parser::dottedStringToFlatArray($key);
            foreach ($flatArray as $key) {
                if (!isset($current[$key])) {
                    return null;
                }
                $current = &$current[$key];
            }
            return $current;
        }
        
        return null;
    }
    
    
    
    /**
     * {@inheritdoc}
     */
    public function all(): array
    {
        return (array)$this->storage;
    }
    
    
    
    /**
     * {@inheritdoc}
     */
    public function set($key, $value): void
    {
        $key = Parser::dottedStringToNestedArrayWithOptionalValue($key, $value);
        $this->storage = array_replace($this->storage, $key);
    }
    
    
    
    
    /**
     * {@inheritdoc}
     */
    public function remove(string $key): void
    {
        $this->set($key, null);
    }
    
    
    
    /**
     * {@inheritdoc}
     */
    public function clear(): void
    {
        $this->storage = [];
    }
    
    
    
    
    
    /**
     * {@inheritdoc}
     */
    public function save(): void
    {
        //No-op for an in-memory session - we're not persisting contents anywhere.
    }
    
    /**
     * {@inheritdoc}
     */
    public function setOptions(array $config): void
    {
        foreach ($config as $key => $value) {
            $this->config[$key] = $value;
        }
    }
    
    /**
     * {@inheritdoc}
     */
    public function getOptions(): array
    {
        return $this->config;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setCookieParams(
        int $lifetime,
        string $path = null,
        string $domain = null,
        bool $secure = false,
        bool $httpOnly = false
    ): void {
        $this->cookie = [
            'lifetime' => $lifetime,
            'path' => $path,
            'domain' => $domain,
            'secure' => $secure,
            'httponly' => $httpOnly,
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCookieParams(): array
    {
        return $this->cookie;
    }
    
}