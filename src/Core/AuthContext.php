<?php

namespace IleafCtg\AuthMgmt\Core;

use IleafCtg\AuthMgmt\Core\Auth\BaseAuthN;
use IleafCtg\AuthMgmt\Modules\UserMgmt\Entities\User;

/**
 * Helper class for interacting with the session of a loggedIn user
 */
class AuthContext {
    
    private static $instances;
    
    /** @var BaseAuthN */
    private $authN = null;
    
    
    private function __construct() {
        if (isset($_SESSION['auth']['authN'])) {
            $authN = $_SESSION['auth']['authN'];
            if ($authN instanceof BaseAuthN) {
                $this->authN = $_SESSION['auth']['authN'];
            }
        }
    }
    
    /**
     * Method for returning a singleton.
     *
     * @return AuthContext
     */
    public static function instance(): AuthContext {
        $class = get_called_class();
        
        if (!isset(static::$instances[$class])) {
            // Instantiate the singleton
            static::$instances[$class] = new static();
        }
        
        return static::$instances[$class];
    }
    
    
    
    /**
     * Returns the logged in user or null, if no logged in user.
     * 
     * @return User|null
     */
    public function getLoggedInUser() {
        if (isset($this->authN)) {
            return $this->authN->currentUser();
        }
        return null;
    }
    
    public function getAuthN() {
        return $this->authN;
    }
    
    public function setLoggedInContext(BaseAuthN $authN) {
        // Clear 'auth' key of session
        $_SESSION['auth'] = [];
        // Put our logged-in AuthN object in the auth key
        $_SESSION['auth']['authN'] = $authN;
        
        $this->authN = $authN;
        
    }
    
    public function unsetLoggedInContext() {
        unset($this->authN);
        // Clear 'auth' key of session
        $_SESSION['auth'] = [];
        
        // Unset instance(s) of this class.
        static::$instances = [];
    }
}