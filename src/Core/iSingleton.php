<?php

namespace IleafCtg\AuthMgmt\Core;

interface iSingleton {
    
    /**
     * Provide a singleton instance of this class.
     * 
     * @return static
     */
    public static function instance();
}