<?php

namespace IleafCtg\AuthMgmt\Core\BaseEntitySupplemental;

use DateTime;

/**
 * Interface that each Entity class should implement.
 * 
 * Common properties expected to be defined on every entity:
 *
 * @property-read int   $id
 * @property-read DateTime $ctime
 * @property-read DateTime $mtime
 */
interface iEntity {
    
    /**
     * Instantiate an entity and load all properties with the provided data.
     * 
     * @param array $data  The data to pre-load the entity properties with.  The array keys must match the entity property names.
     *                     
     * @return mixed
     */
    public static function hydrateFromArray(array $data):iEntity;
    
    public function initProps();
    
    public function getProps();
    
}