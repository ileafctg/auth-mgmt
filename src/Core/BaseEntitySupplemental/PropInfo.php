<?php

namespace IleafCtg\AuthMgmt\Core\BaseEntitySupplemental;


use IleafCtg\AuthMgmt\Core\Exceptions\ApplicationException;
use DateInterval;
use DateTime;
use Exception;
use IleafCtg\AuthMgmt\Utils\Parser;


/**
 * This class is used to define a property to be used in a property map. A property contains
 * the name, type, and read/write accessibility of a class property which will be exposed publicly.
 */
class PropInfo {
    
    const INT = 'int';
    const DOUBLE = 'double';
    const BOOL = 'bool';
    const STRING = 'string';
    const DATE = 'date';
    const DATETIME = 'datetime';
    const DATEINTERVAL = 'dateinterval';
    const ARRAY_TYPE = 'array_type';
    const MIXED = 'mixed';
    
    const READ_ONLY = 'read-only';
    const READ_WRITE = 'read-write';
    const WRITE_ONLY = 'write-only';
    
    const GET_METHOD_PREFIX = 'get_';
    const SET_METHOD_PREFIX = 'set_';
    
    private string $propertyName;
    
    private string $type;
    
    private string $accessibility;
    
    
    private static array $validTypes = [
        self::INT          => true,
        self::DOUBLE       => true,
        self::BOOL         => true,
        self::STRING       => true,
        self::DATE         => true,
        self::DATETIME     => true,
        self::DATEINTERVAL => true,
        self::ARRAY_TYPE   => true,
        self::MIXED        => true,
    ];
    
    
    private static array $validAccessibility = [
        self::READ_ONLY  => true,
        self::READ_WRITE => true,
        self::WRITE_ONLY => true,
    ];
    
    
    
    /**
     * Validate that a property type and accessibility are acceptable.
     * 
     * @param string $name
     * @param string $type
     * @param string $accessibility
     *
     * @throws ApplicationException
     */
    public static function validateProperty(string $name, string $type, string $accessibility) {
        // Validate that $type is in the valid set
        if (!array_key_exists($type, self::$validTypes) || !self::$validTypes[$type]) {
            $validTypes = join(', ', array_keys(self::$validTypes));
            throw new ApplicationException("The type [{$type}] for property [{$name}] is not in the set of valid types: [{$validTypes}]");
        }
    
        // Validate that $accessibility is in the valid set.
        if (!array_key_exists($accessibility, self::$validAccessibility) || !self::$validAccessibility[$accessibility]) {
            $validAccessibility = join(', ', array_keys(self::$validAccessibility));
            throw new ApplicationException("The accessibility [{$accessibility}] for property [{$name}] is not in the set of valid accessibility options: [{$validAccessibility}]");
        }
    }
    
    
    
    /**
     * Helper method to coerce the $value to the $type.
     *
     * @param string $type      Must be one of the PropInfo constants.
     * @param mixed  $value     The property value.
     *
     * @return mixed The type-casted value.
     *
     * @throws ApplicationException
     */
    public static function coerceType($type, $value) {
        // Leave a null value as-is, rather than coercing it to the default value for the given type.
        if ($value === null) {
            return null;
        }
        
        switch ($type) {
            case PropInfo::INT:
                return (int)$value;
            
            case PropInfo::DOUBLE:
                return (double)$value;
            
            case PropInfo::BOOL:
                return Parser::sanitizeTF($value, false);
            
            case PropInfo::STRING:
                return (string)$value;
            
            case PropInfo::ARRAY_TYPE:
                return (array)$value;
            
            case PropInfo::DATEINTERVAL:
                // Treat empty string as 
                if ($value === '') {
                    return null;
                }
                if (is_string($value)) {
                    try {
                        $dt = new DateInterval($value);
                        return $dt;
                    } catch (Exception $e) {
                        throw new ApplicationException("Failed to parse string '{$value}' as a DateInterval object.");
                    }
                }
                if ($value instanceof DateInterval) {
                    return $value;
                }
                throw new ApplicationException("Unexpected property value '{$value}' for type: [{$type}].");
            
            case PropInfo::DATE:
            case PropInfo::DATETIME:
                if (is_string($value)) {
                    $dt = date_create($value);
                    if ($dt === false) {
                        throw new ApplicationException("Failed to parse string '{$value}' as a DateTime object.");
                    }
                    return $dt;
                }
                if ($value instanceof DateTime) {
                    return $value;
                }
                throw new ApplicationException("Unexpected property value '{$value}' for type: [{$type}].");
            
            case PropInfo::MIXED:
                // Just return the value as-is, no type conversion.
                return $value;
            
            default:
                throw new ApplicationException("Invalid type [{$type}].");
        }
    }
    
}
