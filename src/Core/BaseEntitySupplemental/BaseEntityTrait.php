<?php

namespace IleafCtg\AuthMgmt\Core\BaseEntitySupplemental;

use IleafCtg\AuthMgmt\Core\Exceptions\ApplicationException;


/**
 * This trait can be used to expose properties with READ_ONLY, WRITE_ONLY, or READ_WRITE access.
 *
 * Properties can be exposed on a class in one of two ways:
 *   1. Define a private or protected member variable on the class that matches the exact name of the property.
 *      For example, to expose a property named 'my_property', the class would define a "protected $my_property".
 *      IMPORTANT NOTE: If the member variable is defined as 'private', this trait will have access to it ONLY if the trait is use'd
 *      on that class directly. If the trait is use'd on a parent class, but the property is being exposed on a child
 *      class, then the member variable must be protected. This is because the trait is acting withing the scope in
 *      which it was directly use'd.
 *
 *   2. Define getter and/or setter methods for the property. The methods must match a strict method signature
 *      in order to be found and used by this trait. For example, to expose a property named 'my_property',
 *      a getter method signature would be: "protected function get_my_property()", and must return a value.
 *      A setter method signature would be: "protected function set_my_property($value)", and should not return a value.
 *
 * Whichever of these cases is employed, each property MUST be added using addProp, which is used to define
 * the property type and level of accessibility (read-only, write-only, read-write).
 *
 * This trait supports isset($object->my_property) by using the read functionality.
 * If read access is not allowed for the property being accessed, an exception will be thrown.
 *
 * This trait supports unset($object->my_property) by using the write to set the value to null.
 * If write access is not allowed for the property being accessed, an exception will be thrown.
 */
trait BaseEntityTrait {
    
    
    /**
     * Initialize your props.
     *
     * Classes which use this trait MUST override this method in order to configure the Props using addProp()
     *
     * @throws ApplicationException
     */
    protected function initProps() {
        throw new ApplicationException("The method initProps must be overridden by any class using BaseEntityTrait. Missing from: " . static::class);
    }
    
    
    
    /**
     * Add a property
     *
     * @param $propName
     * @param $type
     * @param $accessibility
     *
     * @throws ApplicationException
     */
    protected function addProp($propName, $type, $accessibility) {
        PropInfo::validateProperty($propName, $type, $accessibility);
        
        $this->propertyMap[$propName] = [
            'type' => $type,
            'accessibility' => $accessibility
        ];
    }
    
    
    
    /**
     * Return whether a property exists in the property map.
     * 
     * @param string $propName
     *
     * @return bool
     */
    protected function hasProp($propName) {
        return array_key_exists($propName, $this->propertyMap);
    }
    
    
    
    /**
     * Get the info for a single property.
     * 
     * @param string $propName
     *
     * @return array
     * @throws ApplicationException
     */
    protected function getProp($propName) {
        if (!$this->hasProp($propName)) {
            throw new ApplicationException("The property {$propName} does not exist in the property map.");
        }
        return $this->propertyMap[$propName];
    }
    
    
    /**
     * Return whether or not the given propName is readable on the class.
     * 
     * @param string $propName
     *
     * @return bool
     */
    private function canRead($propName) {
        if (isset($this->propertyMap[$propName])) {
            if (isset($this->propertyMap[$propName]['accessibility'])) {
                return (($this->propertyMap[$propName]['accessibility'] === PropInfo::READ_WRITE) || ($this->propertyMap[$propName]['accessibility'] === PropInfo::READ_ONLY));
            }
        }
        return false;
    }
    
    
    
    /**
     * Return whether or not the given propName is readable on the class.
     *
     * @param string $propName
     *
     * @return bool
     */
    private function canWrite($propName) {
        if (isset($this->propertyMap[$propName])) {
            if (isset($this->propertyMap[$propName]['accessibility'])) {
                return (($this->propertyMap[$propName]['accessibility'] === PropInfo::READ_WRITE) || ($this->propertyMap[$propName]['accessibility'] === PropInfo::WRITE_ONLY));
            }
        }
        return false;
    }
    
    
    
    /**
     * Helper to ensure we've initialized our propertyMap.
     * @throws ApplicationException
     */
    private function ensurePropertyMap() {
        // Let's ensure that initProps() gets called.
        if (empty($this->propertyMap)) {
            $this->initProps();
        }
    }
    
    
    
    /**
     * Get Property Map
     * 
     * @return array
     * @throws ApplicationException
     */
    public function getProps() {
        $this->ensurePropertyMap();
        return $this->propertyMap;
    }
    
    /**
     * Gets the value of an internal property.
     *
     * In order to publicly GET an internal property, such as "$object->my_property",
     * the derived class must do one of the following (in order of precedence in which they are used):
     *   1. Define a "get_my_property()" function.
     *   2. Define the property within the property_map.
     *
     * @param string $propName Property name to access.
     *
     * @return mixed            Property value.
     *
     * @throws ApplicationException    If the property does not exist.
     */
    public function __get($propName) {
        // Let's ensure that initProps() gets called.
        $this->ensurePropertyMap();
        
        // This is a validation step which will throw an exception if the prop_name does not exist in the property_map.
        if (!isset($this->propertyMap[$propName])) {
            throw new ApplicationException("Invalid propertyName {$propName}");
        }
        
        $internalGetter = PropInfo::GET_METHOD_PREFIX . $propName;
        if (method_exists($this, $internalGetter)) {
            return $this->$internalGetter();
        }
        
        if ($this->canRead($propName)) {
            return $this->$propName;
        }
        
        throw new ApplicationException("Unable to get write-only property [{$propName}].");
    }
    
    
    
    /**
     * Sets the value of an internal property.
     *
     * In order to publicly SET an internal property, such as "$object->my_property = 'foobar'",
     * the derived class must define the property within the property_map with READ_WRITE accessibility.
     * Once that is true, by default the __set() method will do a simple assignment of the
     * value to the property (i.e. $this->$propName = $propValue;).  However, if more complex assignment
     * logic is desired/required, you may define a "set_myProperty($value)" method in the derived class.
     * If a "set_myProperty($value)" method is defined, it will be used.  Otherwise, simple assignment
     * will occur.
     * 
     * @param string $propName  The name of the property being set.
     * @param mixed  $propValue The new value for the property being set.
     *
     * @throws ApplicationException    If the property is read-only or does not exist.
     */
    public function __set($propName, $propValue) {
        // Let's ensure that initProps() gets called.
        $this->ensurePropertyMap();
        
        // This is a validation step which will throw an exception if the prop_name does not exist in the property_map.
        if (!isset($this->propertyMap[$propName])) {
            throw new ApplicationException("Invalid propertyName {$propName}");
        }
    
        // Automatically coerce the propValue to the type specified in the propertyMap
        $propValue = PropInfo::coerceType($this->propertyMap[$propName]['type'], $propValue);
        
        $internalSetter = PropInfo::SET_METHOD_PREFIX . $propName;
        if (method_exists($this, $internalSetter) && $this->canWrite($propName)) {
            $this->$internalSetter($propValue);
            return;
        }
    
        if ($this->canWrite($propName)) {
            $this->$propName = $propValue;
            return;
        }
        
        throw new ApplicationException("Unable to set read-only property [{$propName}]");
    }
    
    
    
    /**
     * Assign all object properties from the provided record.
     *
     * CAUTION: This method should primarily be used by the base framework classes for updating the entities properties
     * after some operations. DO NOT use this as a shorthand for updating properties on an object, as this
     * will bypass some standard logic, such as data validation and whether certain properties are writable.
     *
     * @param array $record      The record to load the object properties with. The array keys MUST match the object property names.
     *
     */
    public function bulkAssignProps(array $record) {
        foreach ($record as $propName => $value) {
            if (!$this->hasProp($propName)) {
                // Ignore any properties which do not exist in the property map.
                continue;
            }
        
            $propInfo = $this->getProp($propName);
            // TODO: Ensure we bypass magic methods when setting.
            
            // Coerce the value to the correct type.
            $this->$propName = PropInfo::coerceType($propInfo['type'], $value);
        }
    }
    
    
    /**
     * Checks if a property is set, i.e. defined and not null.
     *
     * In order for this magic method to work on an internal property, such as "isset($object->my_property)",
     * the derived class must define a "get_my_property()" function or define the property
     * within the property_map with READ accessibility.
     *
     * Note that if the property is not defined or is write-only, an exception will be thrown.
     *
     * @param string $propName Property name to access.
     *
     * @return bool  Whether the internal property is set (not null).
     *
     * @throws ApplicationException
     *
     * @see http://php.net/manual/en/function.isset.php
     */
    public function __isset($propName) {
        // Let's ensure that initProps() gets called.
        $this->ensurePropertyMap();
        
        // This is a validation step which will throw an exception if the prop_name does not exist in the property_map.
        if (!isset($this->propertyMap[$propName])) {
            throw new ApplicationException("Invalid propertyName {$propName}");
        }
        
        $internalGetter = PropInfo::GET_METHOD_PREFIX . $propName;
        if (method_exists($this, $internalGetter)) {
            return $this->$internalGetter() !== null;
        }
    
        if ($this->canRead($propName)) {
            return isset($this->$propName);
        }
        
        throw new ApplicationException("Unable to check isset on write-only property [{$propName}]");
    }
    
    
    
    /**
     * Sets an object property to null.
     *
     * In order for this magic method to work on an internal property, such as "unset($object->my_property)",
     * the derived class must define a "set_my_property($value)" function or define the property
     * within the property_map with WRITE accessibility.
     *
     * Note that if the property is not defined or is read-only, an exception will be thrown.
     *
     * @param string $propName The property name
     *
     * @throws ApplicationException
     *
     * @see http://php.net/manual/en/function.unset.php
     */
    public function __unset($propName) {
        // Let's ensure that initProps() gets called.
        $this->ensurePropertyMap();
        
        // This is a validation step which will throw an exception if the prop_name does not exist in the property_map.
        if (!isset($this->propertyMap[$propName])) {
            throw new ApplicationException("Invalid propertyName {$propName}");
        }
    
        $internalSetter = PropInfo::SET_METHOD_PREFIX . $propName;
        if (method_exists($this, $internalSetter)) {
            $this->$internalSetter(null);
            return;
        }
    
        if ($this->canWrite($propName)) {
            $this->$propName = null;
            return;
        }
    
        throw new ApplicationException("Unable to unset read-only property [{$propName}]");
    }
    
}
