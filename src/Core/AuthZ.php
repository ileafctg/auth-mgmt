<?php

namespace IleafCtg\AuthMgmt\Core;

use IleafCtg\AuthMgmt\Core\Auth\iAuthorizationPolicyResponse;
use IleafCtg\AuthMgmt\Core\Auth\iAuthZ;
use IleafCtg\AuthMgmt\Core\Auth\iPolicy;
use IleafCtg\AuthMgmt\Core\Auth\PermissionMap;
use IleafCtg\AuthMgmt\Core\Exceptions\ApplicationException;
use IleafCtg\AuthMgmt\Core\Exceptions\NotImplementedYetException;
use IleafCtg\AuthMgmt\Modules\GroupMgmt\Entities\Group;
use IleafCtg\AuthMgmt\Modules\UserMgmt\Entities\User;



/*
 * 
 * Authorization
 * ^^^^^^^^^^^^^
 *   $role = new Role();
 *   $role->setResourceActionLevelMap = 
 *        [ 'someResource' =>
 *            'actions' => [
 *                'action1',
 *                'action2',
 *            ],
 *            'levels' => [
 *                'client',
 *                'group',
 *            ]
 *        ];
 *   
 *   $actionResourceMap = $role->getActionResourceMap()
 *   $role->save()
 *   Role::delete($role)
 *   $role->addResourceAction($resource, $action)
 *   $role->removeResourceAction($resource, $action)
 *   $role->addResourceLevel($resource, $level)
 *   $role->removeResourceLevel($resource, $level)
 * 
 * - Role::userAddRole($user|$userId, $role|$roleId)
 * - Role::userRemoveRole($user|$userId, $role|$roleId)
 * 
 * - can($action, $resource)
 * - cannot($action, $resource)
 * - canAny([$action1, $action2, $action3], $resource)
 * - forUser(null|$userId|$user)
 * - forClient()
 * - forGroup($groupId)
 * - authorizeOrThrow($policy)
 * - authorize($policy): AuthorizationPolicyResponse
 */


class AuthZ implements iAuthZ {
    
    // Level is what determines the scope at which you can perform Actions for a given resource. Or, put another way,
    // 'level' describes whether or not a given resource may 'belong to' users at the given level/scope.
    // For example, if you have the 'manage' permission for the 'timesheets' resource at the 'client' level, then
    // you may manage timesheets belonging to resources for any user within the given users' client.
    
    // Organization level may never get used - leaving here as a placeholder.
    const PERM_LEVEL_ORGANIZATION = 'organization';
    // Client level.  Can do anything across the whole client.
    const PERM_LEVEL_CLIENT = 'client';
    // Group level. Can do anything across a given group.
    const PERM_LEVEL_GROUP = 'group';
    // User level.  Can do only for resources belonging to one's self.
    const PERM_LEVEL_SELF = 'self';
    
    const PERM_PERM_PERMIT = 'permit';
    const PERM_PERM_DENY = 'deny';
    
    // We'll define one wildcard action.  All other actions should be defined by the developer in a class that implements the iAuthResourcesAndActions interface.
    const PERM_ACTION_ANY = 'any';
    
    
    private PermissionMap $permissionMap;
    
    private $oneTimeLevel;
    private $oneTimeUserId;
    private $oneTimeGroupId;
    
    
    
    /**
     * @param PermissionMap $permissionMap
     */
    public function __construct(PermissionMap $permissionMap) {
        $this->permissionMap = $permissionMap;
    }
    
    
    
    /**
     * Returns true if the given user is permitted to perform the given action against the given resource.
     * 
     * @param string $action
     * @param string $resource
     *
     * @return bool
     * @throws ApplicationException
     */
    public function can(string $action, string $resource): bool {
        return $this->_isAuthorized($action, $resource);
    }
    
    
    
    /**
     * Validate the given resource,action,level.
     * 
     * @param string $resource
     * @param string $action
     * @param string $level
     *
     * @throws ApplicationException
     */
    private function validateResourceActionLevel(string $resource, string $action, string $level) {
        if (!$this->permissionMap->getValidResourcesAndActions()->isValidResourceAction($resource, $action)) {
            throw new ApplicationException("Invalid resource/action {$resource}/{$action}");
        }
        if (!$this->permissionMap->getValidResourcesAndActions()->isValidResourceLevel($resource, $level)) {
            throw new ApplicationException("Invalid resource/action {$resource}/{$level}");
        }
    }
    
    
    
    
    /**
     * Checks whether oneTimeLevel is set or not, and if so, returns the level and the corresponding
     * one-time user id or group id, if applicable.  Then it resets all one-time attributes so that we're
     * set up for the next call to forUser, forGroup, etc.
     * 
     * @return array  An array consisting of level, userId, and groupId
     */
    private function parseAuthOptions(): array {
        // If not set, we'll default to the CLIENT level
        $level = $this->oneTimeLevel ?? self::PERM_LEVEL_CLIENT;
    
        $userId = null;
        $groupId = null;
        
        if ($level === self::PERM_LEVEL_SELF) {
            $userId = $this->oneTimeUserId;
            // We'll need the user's groupId as well
            $groupId = $this->oneTimeGroupId;
        }
        else if ($level === self::PERM_LEVEL_GROUP) {
            $groupId = $this->oneTimeGroupId;
        }
        
        // Reset all to null to invoke default behavior on next call.
        $this->oneTimeLevel = null;
        $this->oneTimeUserId = null;
        $this->oneTimeGroupId = null;
        
        return [
            $level,
            $userId,
            $groupId
        ];
    }
    
    
    /**
     * Returns a simple true or false, to answer whether the caller may perform the given $action against
     * a $resource belonging to users at the given level.
     *
     * @param string $action
     * @param string $resource
     *
     * @return bool
     * @throws ApplicationException
     */
    protected function _isAuthorized(string $action, string $resource):bool {
        $result = false;
        
        list($level, $userId, $groupId) = $this->parseAuthOptions();
    
        // 'users' => [
        //      'any' => [
        //          'client' => 'deny',
        //          'group' => [
        //              5 => 'permit',
        //              7 => 'permit',
        //      ],
        //      'create' => [
        //          'group' => [
        //              5 => 'deny'
        //          ]
        //      ],
        //  ],
        //  'timesheets' => [
        //      'any' => [
        //          'self' => 'permit',
        //      ],
        //  ],
        //        
        
        // Ensure we have a valid action/level/resource being requested vs what is defined in our action/level/resource map
        $this->validateResourceActionLevel($resource, $action, $level);
    
        // If someone has called ->forUser(X), where X does not equal their own user, then it's basically the same as doing
        //      ->forGroup(Y), where Y is the group that user X belongs to.
        // So when evaluating forUser(X), we need to get their group_id, and transform this check into a
        // GROUP level check for the given group_id.
        if ($userId && $userId != $this->permissionMap->getPermissionUserId()) {
            if (!is_int($groupId)) {
                throw new ApplicationException("We should have a groupId set for this user, but instead have groupId: {$groupId}");
            }
            $level = self::PERM_LEVEL_GROUP;
        }
        
        // Compare the action/level/resource against what is defined for the user in their permission map
        if ($this->permissionMap->isPermitted($resource, $action, $level, $groupId)) {
            $result = true;    
        }
        return $result;
        
    }
    
    
    
    /**
     * Returns true if the given user is NOT permitted to perform the given action against the given resource.
     * 
     * @param string $action
     * @param string $resource
     *
     * @return bool
     * @throws ApplicationException
     */
    public function cannot(string $action, string $resource): bool {
        return !$this->_isAuthorized($action, $resource);
    }
    
    
    
    /**
     * Returns true if the given user is permitted to perform ANY of the given actions against the given resource.
     * 
     * @param array  $actions
     * @param string $resource
     *
     * @return bool
     * @throws ApplicationException
     */
    public function canAny(array $actions, string $resource): bool {
        $authorized = false;
        foreach ($actions as $action) {
            if ($this->can($action, $resource)) {
                $authorized = true;
                break;
            } 
        }
        
        return $authorized;
    }
    
    
    
    /**
     * Returns true if the given user is NOT permitted to perform ANY of the given actions against the given resource.
     * 
     * @param array  $actions
     * @param string $resource
     *
     * @return bool
     * @throws ApplicationException
     */
    public function cannotAny(array $actions, string $resource): bool {
        return !$this->canAny($actions, $resource);
    }
    
    
    
    /**
     * Calling forUser on our object specifies that our next call of can(), cannot(), canAny(), or cannotAny() should check at the SELF
     * level.  If forUser() is called with a $user, we'll check to see if we can perform the action against the resource on behalf of that $user.
     * Otherwise if forUser() is called without a $user, it is presumed that we're operating on behalf of self, not another user.
     * 
     * @param User|null $user
     *
     * @return iAuthZ
     */
    public function forUser($user = null): iAuthZ {
        $this->oneTimeLevel = self::PERM_LEVEL_SELF;
        
        if ($user) {
            $this->oneTimeUserId = $user->id;
            $this->oneTimeGroupId = $user->groupId;
        }
        
        // If $user is null, then it's presumed that we're operating on behalf of self, not another user.
        
        return $this;
    }
    
    
    
    /**
     * Calling forGroup on our object specifies that our next call of can(), cannot(), canAny(), or cannotAny() should
     * check at the GROUP level.  This means we'll be checking to see if we can perform a given action against a given
     * resource on behalf of a given group.
     *
     * @param Group|int $group
     *
     * @return iAuthZ
     * @throws ApplicationException
     */
    public function forGroup($group): iAuthZ {
        $this->oneTimeLevel = self::PERM_LEVEL_GROUP;
        if (is_numeric($group)) {
            $this->oneTimeGroupId = $group;
        }
        else {
            $this->oneTimeGroupId = $group->id;
        }
        
        // We should have a groupId set at this point, if not, something is wrong
        if (!is_numeric($this->oneTimeGroupId)) {
            throw new ApplicationException("Invalid groupId specified");
        }
        return $this;
    }
    
    
    
    /**
     * Calling forClient on our object specifies that our next call of can(), cannot(), canAny(), or cannotAny() should
     * check at the CLIENT level.  This means we'll be checking to see if we can perform a given action against a given
     * resource on behalf of a given client.
     * 
     * @return iAuthZ
     */
    public function forClient(): iAuthZ {
        $this->oneTimeLevel = self::PERM_LEVEL_CLIENT;
        
        return $this;
    }
    
    
    
    /**
     * Calling forOrganization on our object specifies that our next call of can(), cannot(), canAny(), or cannotAny() should
     * check at the ORGANIZATION level.  This means we'll be checking to see if we can perform a given action against a given
     * resource on behalf of a given organization.
     * 
     * @return iAuthZ
     */
    public function forOrganization(): iAuthZ {
        $this->oneTimeLevel = self::PERM_LEVEL_ORGANIZATION;
        
        return $this;
    }
    
    
    
    public function authorizeOrThrow(iPolicy $policy): iAuthorizationPolicyResponse {
        // TODO: Implement authorizeOrThrow() method.
        throw new NotImplementedYetException();
    }
    
    
    
    public function authorize(iPolicy $policy): iAuthorizationPolicyResponse {
        // TODO: Implement authorize() method.
    
        /**
         *
         *
         * If forUser($user) is not specified, then we use the logged in user's context automatically
         *      forOrg() - used to check at level of org
         *      forClient() - used to check at level of client.  This is the default, if no level specified
         *      forGroup($group) - used to check at the level of group, with the given $group
         *      forUser(?$user = null) - used to check at the level of user, with the given $user.  If empty, current user is used.
         *
         * can() and cannot() and canAny() and cannotAny() check session roles/perms and/or db-backed roles/perms and returns simple true or false.
         *      Can be used anywhere for anything
         *          I.e. allow/deny access to a route
         *               allow/deny access ad hoc somewhere
         *               within a policy
         *
         *      // client-level
         *      $result = Auth::can($action, $resource);
         *      $result = Auth::forClient()->can($action, $resource);
         *      $result = Auth::cannot($action, $resource);
         *      $result = Auth::forClient()->cannot($action, $resource);
         *      // user-level, for given user (current user if empty)
         *      $result = Auth::forUser($user)->can($action, $resource);
         *      $result = Auth::forUser()->cannot($action, $resource);
         *      $result = Auth::forUser($user)->canAny([$action1, $action2, $action3], $resource);
         *      $result = Auth::forUser()->cannotAny([$action1, $action2, $action3], $resource);
         *      // group-level, for given $group
         *      $result = Auth::forGroup($group)->can($action, $resource);
         *      // org-level
         *      $result = Auth::forOrg()->can($action, $resource);
         *
         *      If there's no valid combo of level-action-resource defined against what is called, then an exception will be thrown.
         *
         *
         * authorizeOrThrow() and authorize() are for evaluating policies and expect a class name and policy name (camelCase), and optionally some extra context.
         *          We'll look for the given method within the Class and execute it.
         *          Each policy class should extend a base AuthorizationPolicy Class, wherein we should also declare our AuthorizationException class and our AuthorizationResult class.
         *      authorizeOrThrow() will throw an exception if not authorized, else just return true otherwise.
         *      authorize() will return an AuthorizationResult object that supports a message property and isAllowed and isDenied properties.
         *
         *
         *      Auth::authorizeOrThrow(UserServiceAuthorization::Class, $policyAndMethodName, $extraContext);
         *
         *      $response = Auth::authorize(UserServiceAuthorization::Class, $policyAndMethodName, $extraContext);
         *      if ($response->isAllowed()) {
         *          Log::debug("Allowed: {$response->message}");
         *      }
         *      else if ($response->isDenied()) {
         *          throw new Exception("Problem with auth: {$response->message}");
         *      }
         */
        
        throw new NotImplementedYetException();
    }
}