<?php

namespace IleafCtg\AuthMgmt\Core;

use DateTime;
use DateTimeZone;
use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\iEntity;
use IleafCtg\AuthMgmt\Core\BaseRepoSupplemental\iFilter;
use IleafCtg\AuthMgmt\Core\BaseRepoSupplemental\iRepository;
use IleafCtg\AuthMgmt\Core\Exceptions\ApplicationException;
use IleafCtg\AuthMgmt\Core\Exceptions\NotFoundException;

abstract class BaseRepository implements iRepository {
    
    public function persist(iEntity $entity): array {
        $data = $this->dataMapper->entityToRepoDto($entity);
        
        $affectedId = 0;
        if (isset($data['id']) && is_numeric($data['id'])) {
            $this->pdoSimple->dbUpdateRecord(static::REPO_TABLE, ['id' => $data['id']], $data);
            $affectedId = $data['id'];
        }
        else {
            unset($data['id']);
            $this->pdoSimple->dbInsertRecord(static::REPO_TABLE, $data);
            $affectedId = $this->pdoSimple->getLastInsertId();
        }
        
        $record = $this->pdoSimple->dbGetRecord(static::REPO_TABLE, ['id' => $affectedId]);
        
        $dto = $this->dataMapper->repoToEntityDto($record);
        return $dto;
    }
    
    
    
    public function findById(int $id): array {
        $record = $this->pdoSimple->dbGetRecord(static::REPO_TABLE, ['id' => $id]);
        if ($record) {
            $dto = $this->dataMapper->repoToEntityDto($record);
            return $dto;
        }
        throw new NotFoundException("No item with id {$id} found");
    }
    
    
    public function fetch(iFilter $filter): array {

        $limit = $filter->getLimit();
        $page = $filter->getPageNumber();
        $filters = $filter->getFilters();
        
        $orderBy = $filter->getOrderBy();
        
        $whereStr = "1 = 1";
        $whereParams = [];
        foreach ($filters as $key => $value) {
            $whereParams[$key] = $value;
    
            // Assume that if an array, it should be an IN comparison
            if (is_array($value)) {
                $whereStr .= " AND {$key} IN (:{$key})";
            }
            else {
                // Accommodate for a gte or lte comparison
                preg_match("/^([><]=) (.*)/", $value, $matches);
                if ($matches) {
                    $whereStr .= " AND {$key} {$matches[1]} :{$key}";
                    $whereParams[$key] = $matches[2];
                }
                else {
                    $whereStr .= " AND {$key} = :{$key}";
                }
            }
        }
        
        $queryLimit = $limit + 1;
        $offset = ($page - 1) * $limit;
        
        $records = $this->pdoSimple->selectWhere(
            '*',
            static::REPO_TABLE,
            $whereStr,
            $whereParams,
            [
                'limit' => $queryLimit,
                'offset' => $offset,
                'order_by' => $orderBy,
            ]
        );
        
        $more = false;
        if (count($records) > $limit) {
            $more = true;
            array_pop($records);
        }
        
        $results = [];
        foreach ($records as $record) {
            $results[] = $this->dataMapper->repoToEntityDto($record);
        }
        
        return [
            'results' => $results,
            'more' => $more,
        ];
    }
}