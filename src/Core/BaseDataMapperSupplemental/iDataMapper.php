<?php

namespace IleafCtg\AuthMgmt\Core\BaseDataMapperSupplemental;

use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\iEntity;
use IleafCtg\AuthMgmt\Core\iSingleton;

interface iDataMapper extends iSingleton {
    
    /**
     * Takes the array of data returned by operations at the Repository layer and transforms them into the
     * defined data-transfer-object (DTO) for the given Entity type.
     * 
     * @param array $data
     *
     * @return array
     */
    public function repoToEntityDto(array $data): array;
    
    
    
    /**
     * Takes a given Entity type and transforms it into the defined data-transfer-object (DTO) for the given Entity type.
     * 
     * @param iEntity $entity
     *
     * @return array
     */
    public function entityToRepoDto(iEntity $entity): array;
    
    
    public function getEntityToRepoFieldName(string $entityFieldName): string;
    
    
}