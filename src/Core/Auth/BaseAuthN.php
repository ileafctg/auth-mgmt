<?php
namespace IleafCtg\AuthMgmt\Core\Auth;

use IleafCtg\AuthMgmt\Core\AuthZ;
use IleafCtg\AuthMgmt\Core\Exceptions\ApplicationException;
use IleafCtg\AuthMgmt\Modules\UserMgmt\Entities\User;

/**
 *  * Authentication
 * ^^^^^^^^^^^^^^
 *
 * Our base authentication class.  You should extend this class and implement the abstract methods to interface with your system in order to utilize the authentication/authorization framework.
 * 
 */
abstract class BaseAuthN implements iAuth {
    
    protected User $user;
    protected bool $isLoggedIn;
    protected iAuthZ $authZ;
    protected iSession $session;
    
    protected static $instances;
    
    
    
    /**
     * To prevent new-ing up, we'll make this private, so that instance() must be used.
     */
    private function __construct(iSession $session) {
        $this->isLoggedIn = false;
        $this->session = $session;
        // Start our session if it's not started already
        if (!$this->session->isStarted()) {
            $this->session->start();
        }
    }
    
    
    
    /**
     * Method for returning a singleton.
     *
     * @return BaseAuthN
     */
    public static function instance(iSession $session): BaseAuthN {
        $class = get_called_class();
    
        if (!isset(static::$instances[$class])) {
            // Instantiate the singleton
            static::$instances[$class] = new static($session);
        }
        
        // If our session has us already logged in, let's restore/use that as our authN instance
        if (static::$instances[$class]->session->has('auth.authN')) {
            $authN = static::$instances[$class]->session->get('auth.authN');
            if ($authN instanceof BaseAuthN) {
                static::$instances[$class] = $authN;
            }
        }
        
        return static::$instances[$class];
    }
    
    
    /**
     * Takes the given $user object and logs them in, setting up the session with the user's context, permissions, roles, etc.
     *
     * @param User $user
     *
     * @return bool
     */
    public function login(User $user) : bool {
        return $this->internalLogin($user);
    }
    
    
    
    /**
     * Retrieve the permission map associated with the user that is authenticating.
     *
     * @param User $user
     *
     * @return PermissionMap
     */
    abstract protected function getPermissionMap(User $user): PermissionMap;
    
    
    
    /**
     * Takes the given $user object and logs them in, setting up the session with the user's context, permissions, roles, etc.
     *
     * @param User $user
     *
     * @return bool
     */
    private function internalLogin(User $user): bool {
        $this->user = $user;
        
        $this->authZ = new AuthZ($this->getPermissionMap($user));
        
        $this->setSessionLoggedInContext($this);
        
        // Change state to show that we're logged in.
        $this->isLoggedIn = true;
        
        return $this->isLoggedIn();
    }
    
    
    
    /**
     * Sets the logged-in context in $_SESSION, so that we can utilize it later to keep a user logged in
     * in between requests.
     * 
     * @param BaseAuthN $authN
     */
    private function setSessionLoggedInContext(BaseAuthN $authN) {
        // Clear 'auth' key of session
        $this->unsetSessionLoggedInContext();
        
        // Put our logged-in AuthN object in the auth key
        $this->session->set('auth.authN', $authN);
    }
    
    
    
    /**
     * Unsets any info in the $_SESSION for the logged in user.
     */
    private function unsetSessionLoggedInContext() {
        // Clear 'auth' key of session
        $this->session->set('auth', []);
    }
    
    
    
    /**
     * Set a key in our session, in the area within or session storage that will be cleared if/when
     * the user logs out.  This is a convenience function, to ensure that the value is placed under the
     * 'auth.authN' key of our session storage.
     *
     * @param string $key
     * @param mixed  $value
     *
     * @return void
     * @throws ApplicationException
     */
    public function authSessionSet(string $key, $value) {
        if (empty($key)) {
            throw new ApplicationException("Key must not be empty.");
        }
        if (!preg_match("/^auth.authN./", $key)) {
            $key = "auth.authN.{$key}";
        }
        $this->session->set($key, $value);
    }
    
    
    
    /**
     * Get a key from our session, from the area within our session storage that is cleared if/when
     * the user logs out. This is a convenience function, to ensure that the value is retrieved from under the
     * 'auth.authN' key of our session storage.
     *
     * @param string $key
     *
     * @return mixed|null
     * @throws ApplicationException
     */
    public function authSessionGet(string $key) {
        if (empty($key)) {
            throw new ApplicationException("Key must not be empty.");
        }
        if (!preg_match("/^auth.authN./", $key)) {
            $key = "auth.authN.{$key}";
        }
        return $this->session->get($key);
    }
    
    
    
    
    
    
    /**
     * @return iSession
     */
    public function getSession() {
        return $this->session;
    }
    
    
    /**
     * Gets the associated AuthZ object for the authenticated user.
     * 
     * @return iAuthZ
     */
    public function getAuthZ(): iAuthZ {
        return $this->authZ;
    }
    
    /**
     * Helper method to impersonate a particular user, bypassing need to utilize their credentials.
     * @param User $user
     */
    public function impersonateUser(User $user) {
        if ($this->isLoggedIn()) {
            // Back up current logged in user info, if this is the first time we're switching away from our 'normal' login.
            // If there's something already in auth_backup, we'll assume that it's because we're doing consecutive impersonations
            // without ending our impersonation first.
            if (empty($this->session->get('auth_backup'))) {
                $this->session->set('auth_backup.authN', clone $this);
            }
            $this->unsetSessionLoggedInContext();
        }
        $this->internalLogin($user);
    }
    
    
    
    /**
     * Ends impersonation, if applicable.
     */
    public function endImpersonation() {
        $this->logout();
        // Restore backed up logged-in user, if exists.
        if (!empty($this->session->get('auth_backup.authN'))) {
            $this->internalLogin($this->session->get('auth_backup.authN')->currentUser());
            // NOTE: We don't restore anything particular to their state, if there was any, just log them back in.
            //       It might be worth exploring doing more here later, depending on what we have stored in the session.
            $this->session->set('auth_backup', null);
        }
    }
    
    /**
     * @inheritDoc
     */
    public function logout(): bool {
        unset($this->user);
        unset($this->authZ);
        $this->unsetSessionLoggedInContext();
        $this->isLoggedIn = false;
        
        return $this->isLoggedIn();
    }
    
    /**
     * @inheritDoc
     */
    public function isLoggedIn(): bool {
        return $this->isLoggedIn === true;
    }
    
    /**
     * @inheritDoc
     */
    public function currentUser(): User {
        return $this->user;
    }
    
    
}