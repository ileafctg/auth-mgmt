<?php

namespace IleafCtg\AuthMgmt\Core\Auth;

/*
 * BaseAuthResourcesAndActions
 * - Defines the allowed resources and accompanying actions.
 * - permissionMaps must use only these resources and actions.
 * 
 * - Levels is defined and cannot be changed.
 *  - they are: org, client, group, self
 * 
 * - permitOrDeny are only two values, and cannot be changed: permit OR deny
 * 
 * 
 * Someone should instantiate their Auth with an instance of their defined AuthResourcesAndActions class.  And should also have a 'getPermissionMap' method defined.
 * Inside of Auth, we'll pass the AuthResourcesAndActions class, and the permissionMap for a user.
 */

use IleafCtg\AuthMgmt\Core\AuthZ;
use IleafCtg\AuthMgmt\Core\Exceptions\ApplicationException;
use IleafCtg\AuthMgmt\Utils\Parser;
use function PHPUnit\Framework\isNull;

abstract class BaseAuthResourcesAndActions implements iAuthResourcesAndActions {
    
    // 'manage' is a shortcut to include all CRUD operations.  You could allow 'manage', and deny 'update', however, to get more fine-grained.
    const PERM_ACTION_MANAGE = 'manage';
    const PERM_ACTION_CREATE = 'create';
    const PERM_ACTION_READ   = 'read';
    const PERM_ACTION_UPDATE = 'update';
    const PERM_ACTION_DELETE = 'delete';
    // 'run' is used for resources that allow an execution or 'run' of things related to the resource.  For example, you could 'run' a report or 'run' a job or 'run' payroll.
    // 'run' implies the ability to 'read' a resource.
    const PERM_ACTION_RUN    = 'run';
    
    const PERM_ACTION_ANY    = 'any';
    
    private $validResourcesAndActions;
    
    protected $defaultResourcesAndActions = [
        ## Resources
        'users' => [
            'actions' => [
                self::PERM_ACTION_MANAGE,
                self::PERM_ACTION_CREATE,
                self::PERM_ACTION_READ,
                self::PERM_ACTION_UPDATE,
                self::PERM_ACTION_DELETE,
                
                // NOTE: 'any' (AuthZ::PERM_ACTION_ANY) is a wildcard action which will return true regardless of which other defined action is specified,
                // if the user has the 'any' action permitted.
            ],
            'levels' => [
                // If empty, we'll assume you can perform actions for this resource
                // at every level.  Otherwise honor what's defined here.
                AuthZ::PERM_LEVEL_CLIENT,
                AuthZ::PERM_LEVEL_GROUP,
            ]
        ],
    ];
    
    public function __construct($resourcesAndActions = null) {
        if (empty($resourcesAndActions)) {
            // Use our default
            $resourcesAndActions = $this->defaultResourcesAndActions;
        }
        
        $this->setResourcesAndActions($resourcesAndActions);
    }
    
    public function getResourcesAndActions(): array {
        return $this->validResourcesAndActions;
    }
    
    public function setResourcesAndActions($resourcesAndActions) {
        $this->validateResourcesAndActionsArray($resourcesAndActions);
        $this->validResourcesAndActions = $resourcesAndActions;
    }
    
    private function validateResourcesAndActionsArray(&$resourcesAndActions) {
        // Make sure that we have a 'levels' key, and an 'actions' key for each 'resource'.
        foreach($resourcesAndActions as $resource => $actionsAndLevels) {
            if (!isset($actionsAndLevels['levels']) || !isset($actionsAndLevels['actions'])) {
                throw new ApplicationException("Invalide levels and actions map - must have a 'levels' and an 'actions' key");
            }
            // Shouldn't have anything other than the two keys
            if (count($actionsAndLevels) > 2) {
                throw new ApplicationException("Invalid levels and actions map - should only have a 'levels' and an 'actions' key");
            }
            // make sure that we don't go more than 2 levels deep
            if (Parser::arrayDepth($actionsAndLevels) > 2) {
                throw new ApplicationException("Levels and actions map should be no more than 2 levels deep");
            }
            // Ensure that we have the 'any' action as a valid action for any resource
            if (!isset($actionsAndLevels['actions'][self::PERM_ACTION_ANY])) {
                array_push($resourcesAndActions[$resource]['actions'], self::PERM_ACTION_ANY);
            }
            
            // If we have an empty set of 'levels' for any given resource, assume all levels,
            // and populate it as such.
            if (empty($actionsAndLevels['levels'])) {
                $resourcesAndActions[$resource]['levels'] = [
                    AuthZ::PERM_LEVEL_ORGANIZATION,
                    AuthZ::PERM_LEVEL_CLIENT,
                    AuthZ::PERM_LEVEL_GROUP,
                    AuthZ::PERM_LEVEL_SELF,
                ];
            }
            // Validate that we're only using allowed levels
            else {
                foreach ($actionsAndLevels['levels'] as $level) {
                    if (!in_array($level, [
                        AuthZ::PERM_LEVEL_ORGANIZATION,
                        AuthZ::PERM_LEVEL_CLIENT,
                        AuthZ::PERM_LEVEL_GROUP,
                        AuthZ::PERM_LEVEL_SELF,
                    ])) {
                        throw new ApplicationException("Invalid level [{$level}]. Must be one of organization, client, group, or self");
                    }
                }
            }
        }
    }
    
    
    
    public function isValidResource($resource): bool {
        return isset($this->getResourcesAndActions()[$resource]);
    }
    
    public function isValidResourceAction($resource, $action): bool {
        if ($this->isValidResource($resource)) {
            return in_array($action, $this->getResourcesAndActions()[$resource]['actions']);
        }
        return false;
    }
    
    public function isValidResourceActionLevel($resource, $action, $level): bool {
        // Valid resource and action?
        if ($this->isValidResourceAction($resource, $action)) {
            // Valid resource and level?
            if ($this->isValidResourceLevel($resource, $level)) {
                return true;
            }
        }
        return false;
    }
    
    public function isValidResourceLevel($resource, $level): bool {
        if ($this->isValidResource($resource)) {
            return in_array($level, $this->getResourcesAndActions()[$resource]['levels']);
        }
        return false;
    }
    
}