<?php
namespace IleafCtg\AuthMgmt\Core\Auth;

use IleafCtg\AuthMgmt\Core\Exceptions\LoginException;
use IleafCtg\AuthMgmt\Modules\UserMgmt\Entities\User;

interface iAuth {
    
    function login(User $user) : bool;
    
    function logout() : bool;
    
    function isLoggedIn() : bool;
    
    function currentUser() : User;
    
}