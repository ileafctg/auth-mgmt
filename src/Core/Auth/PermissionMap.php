<?php
namespace IleafCtg\AuthMgmt\Core\Auth;

use IleafCtg\AuthMgmt\Core\AuthZ;
use IleafCtg\AuthMgmt\Core\Exceptions\ApplicationException;
use IleafCtg\AuthMgmt\Modules\UserMgmt\Entities\User;
use IleafCtg\AuthMgmt\Utils\Parser;

class PermissionMap {
    
    /** @var array */
    private $permissionMapArray;
    
    /** @var iAuthResourcesAndActions  */
    private $resourceActionLevelMap;
    
    /** @var User */
    private $user;
    
    /**
     * @param iAuthResourcesAndActions $resourcesAndActions    The resources and actions that are valid for this permission set.
     * @param User                     $user                   The User object for which this permissionMap applies.
     * @param array|null               $permissionMapArray     The permission map array (resource/action/permit|deny).
     *
     * @throws ApplicationException
     */
    public function __construct(iAuthResourcesAndActions $resourcesAndActions, User $user, array $permissionMapArray = null) {
        $this->resourceActionLevelMap = $resourcesAndActions;
        if ($permissionMapArray) {
            $this->hydrateFromArray($permissionMapArray);
        }
        
        $this->user = clone($user);
    }
    
    
    
    /**
     * Return the userId associaged with this permission map
     * @return int
     */
    public function getPermissionUserId(): int {
        return $this->user->id;
    }
    
    /**
     * @return iAuthResourcesAndActions
     */
    public function getValidResourcesAndActions(): iAuthResourcesAndActions {
        return $this->resourceActionLevelMap;
    }
    
    
    /**
     * @throws ApplicationException
     */
    public function hydrateFromArray(array $permissionMap) {
        // Example permissions array
        // ^^^^^^^^^^^^^^^^^^^^^^^^^
        //
        // 'users' => [
        //      'any' => [
        //          'client' => 'deny',
        //          'group' => [
        //              5 => 'allow',
        //              7 => 'allow',
        //      ],
        //      'create' => [
        //          'group' => [
        //              5 => 'deny'
        //          ]
        //      ],
        //  ],
        //  'timesheets' => [
        //      'any' => [
        //          'self' => 'allow',
        //      ],
        //  ],
        //     
        
        // All of these should be found true for this to be a valid array we're processing
        $containsResources = false;
        $containsActions = false;
        $containsLevels = false;
        $containsPermitOrDeny = false;
        
        foreach($permissionMap as $resource => $actions) {
            foreach ($actions as $action => $levels) {
                foreach ($levels as $level => $permitOrDeny) {
                    // If $level is 'group', then we have another array to process.
                    if ($level === AuthZ::PERM_LEVEL_GROUP) {
                        foreach ($permitOrDeny as $groupId => $permitDenyWithGroup) {
                            $this->setAccess($resource, $action, $level, $permitDenyWithGroup, $groupId);
                        }
                    }
                    else {
                        $this->setAccess($resource, $action, $level, $permitOrDeny);
                        
                    }
                    if (in_array($permitOrDeny, [AuthZ::PERM_PERM_PERMIT, AuthZ::PERM_PERM_DENY])) {
                        $containsPermitOrDeny = true;
                    }
                    $containsLevels = true;
                }
                $containsActions = true;
            }
            $containsResources = true;
        }
        
        if (!$containsResources || !$containsActions || !$containsLevels || !$containsPermitOrDeny) {
            throw new ApplicationException("Invalid resources or actions or levels in your permission map");
        }
    }
    
    
    
    /**
     * Checks whether the given details match a 'permit' or 'deny' in the perm map
     * 
     * @param      $resource
     * @param      $action
     * @param      $level
     * @param null $groupId
     *
     * @return bool
     */
    public function isPermitted($resource, $action, $level, $groupId = null): bool {
    
        /**
         * Higher level permissions cascade down to lower levels. However, any explicit settings at a lower level override
         * what may have been set at a higher level.
         */
        // NOTE: if both an 'any' action and some other action are defined at the same level, the more specific
        //       action (i.e. not 'any' action) will take precedence.  This allows us to say, for example, 
        //       permit 'any' action, except when it's a 'write' action.
        // 
        // If no permit or deny found along the way, false is returned by default.
    
        // Start with an assumption of not permitted
        $permitOrDenyDirective = AuthZ::PERM_PERM_DENY;
        
        if (isset($this->permissionMapArray[$resource])) {
            // Start from Organization and work our way down through the levels.
            // This allows us to broadly permit or deny action at a given level, but then override at a more specific
            // level.  For example, someone may be denied the ability to edit users at the organizational or client level,
            // but be permitted to edit them at the group level.
            $levels = [
                AuthZ::PERM_LEVEL_ORGANIZATION,
                AuthZ::PERM_LEVEL_CLIENT,
                AuthZ::PERM_LEVEL_GROUP,
                AuthZ::PERM_LEVEL_SELF,
            ];
        
            foreach ($levels as $checkLevel) {
                // We'll prefer the specific action, if it's present, otherwise we'll fall back to 'any' as our action
                // during checks.
                $actionEvaluated = $action;
                if (!isset($this->permissionMapArray[$resource][$action][$checkLevel])) {
                    $actionEvaluated = AuthZ::PERM_ACTION_ANY;
                }
                
                if (isset($this->permissionMapArray[$resource][$actionEvaluated][$checkLevel])) {
                    if ($checkLevel === AuthZ::PERM_LEVEL_GROUP) {
                        if (isset($this->permissionMapArray[$resource][$actionEvaluated][$checkLevel][$groupId])) {
                            $permitOrDenyDirective = $this->permissionMapArray[$resource][$actionEvaluated][$checkLevel][$groupId];
                        }
                    }
                    else {
                        $permitOrDenyDirective = $this->permissionMapArray[$resource][$actionEvaluated][$checkLevel];
                    }
                }
                
                // Don't go any deeper than the target $level
                // I.e. if they specified ->forOrganization(), the intent is to return the permission ** at the level specified **,
                //      regardless of whether it may be a different outcome at a lower level.
                if ($checkLevel === $level) {
                    break;
                }
            }
        }
        
        return $this->evaluatePermitDeny($permitOrDenyDirective);
    }
    
    
    
    
    /**
     * Takes a value ('permit' or 'deny') and returns either true or false.
     * @param string $value
     *
     * @return bool
     * @throws ApplicationException
     */
    private function evaluatePermitDeny(string $value): bool {
        $value = strtolower($value);
        if ($value === AuthZ::PERM_PERM_PERMIT) return true;
        if ($value === AuthZ::PERM_PERM_DENY) return false;
        
        throw new ApplicationException("Invalid value {$value}, must be either " . AuthZ::PERM_PERM_PERMIT . " or " . AuthZ::PERM_PERM_DENY);
    }
    
    
    
    /**
     * @param string   $resource
     * @param string   $action
     * @param string   $level
     * @param string   $permit
     * @param int|null $groupId
     *
     * @throws ApplicationException
     */
    private function setAccess(string $resource, string $action, string $level, string $permit, int $groupId = null) {
        // Validate resource, action, level
        if (!$this->resourceActionLevelMap->isValidResourceActionLevel($resource, $action, $level)) {
            throw new ApplicationException("Invalid resource/action/level {$resource}/{$action}/{$level}. Must be defined in your resource/action/level map");
        }
        // As long as evaluation succeeds, then $permit is a valid value
        $this->evaluatePermitDeny($permit);
        
        if ($level == 'group') {
            // Set it up with a group id.
            $this->permissionMapArray[$resource][$action][$level][$groupId] = $permit;
        }
        else {
            $this->permissionMapArray[$resource][$action][$level] = $permit;
        }
    }
}