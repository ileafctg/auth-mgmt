<?php

namespace IleafCtg\AuthMgmt\Core\Auth;

use IleafCtg\AuthMgmt\Modules\GroupMgmt\Entities\Group;
use IleafCtg\AuthMgmt\Modules\UserMgmt\Entities\User;

interface iAuthZ {
    
    function can(string $action, string $resource): bool;
    
    function cannot(string $action, string $resource): bool;
    
    function canAny(array $actions, string $resource): bool;
    
    function cannotAny(array $actions, string $resource): bool;
    
    function forOrganization(): self;
    
    function forClient(): self;
    
    function forGroup($group): self;
    
    function forUser(User $user = null): self;
    
    function authorizeOrThrow(iPolicy $policy): iAuthorizationPolicyResponse;
    
    function authorize(iPolicy $policy): iAuthorizationPolicyResponse;
}