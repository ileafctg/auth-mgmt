<?php

namespace IleafCtg\AuthMgmt\Core\Auth;

interface iAuthResourcesAndActions {
    public function getResourcesAndActions(): array;
    
    public function isValidResource($resource): bool;
    
    public function isValidResourceAction($resource, $action): bool;
    
    public function isValidResourceLevel($resource, $level): bool; 
}