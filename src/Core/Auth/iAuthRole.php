<?php

namespace IleafCtg\AuthMgmt\Core\Auth;

use IleafCtg\AuthMgmt\Modules\UserMgmt\Entities\User;

interface iAuthRole {
    public function getPermissionMap(User $user): PermissionMap;
}