<?php

namespace IleafCtg\AuthMgmt\Core\BaseServiceSupplemental;

use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\iEntity;
use IleafCtg\AuthMgmt\Core\BaseRepoSupplemental\iFilter;

interface iService {
    
    public function save(iEntity $entity);
    
    
    
    /**
     * @param int $id
     *
     * @return mixed|iEntity
     */
    public function findById(int $id);
    
    public function fetch(iFilter $filter);
    
}