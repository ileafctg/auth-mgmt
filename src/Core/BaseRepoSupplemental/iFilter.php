<?php

namespace IleafCtg\AuthMgmt\Core\BaseRepoSupplemental;

use IleafCtg\AuthMgmt\Core\iSingleton;

/**
 * This is a filter that can be used to translate Service filters into an array of filters that can be used 
 * by the Repository layer.
 */
interface iFilter {
    
    public function getFilters(): array;
    
    public function getOrderBy(): string;
    
    public function setPageNumber(int $pageNumber);
    public function getPageNumber(): int;
    
    public function setLimit(int $limit);
    public function getLimit(): int;
}