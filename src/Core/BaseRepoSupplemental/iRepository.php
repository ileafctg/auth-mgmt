<?php

namespace IleafCtg\AuthMgmt\Core\BaseRepoSupplemental;

use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\iEntity;

/**
 * Interface that each Repository class should implement.
 *
 */
interface iRepository {
    
    /**
     * Save the given entity. The save could be an update or a create, depending on whether the entity has its id property set or not.
     *
     * @param iEntity $entity
     *
     * @return array
     */
    public function persist(iEntity $entity): array;
    
    
    
    /**
     * Find the Entity that has the given id.
     * 
     * @param int $id
     *
     * @return array
     */
    public function findById(int $id): array;
    
    
    
    /**
     * Fetches an array of records that match the given filter.  Results include pagination details and request
     * is subject to page max, etc.
     * 
     * @param iFilter $filter  The $filter object allows you to specify query filters and limit and page number.
     * 
     */
    public function fetch(iFilter $filter): array;
}