<?php

namespace IleafCtg\AuthMgmt\Core;

use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\EntityCollection;
use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\iEntity;
use IleafCtg\AuthMgmt\Core\BaseRepoSupplemental\iFilter;
use IleafCtg\AuthMgmt\Core\BaseRepoSupplemental\iRepository;
use IleafCtg\AuthMgmt\Core\BaseServiceSupplemental\iService;

abstract class BaseService implements iService {
    protected static iService $instance;
    
    /** @var iEntity|string */
    protected $entityClass;
    
    /** @var iRepository  */
    protected $repository;
    
    
    
    /**
     * @param string $entityClass
     * @param iRepository $repository
     */
    public function __construct($entityClass, $repository) {
        $this->entityClass = $entityClass;
        $this->repository = $repository;
        
        // Register authorization policy mappings
    }
    
    
    
    /**
     * Saves the given $entity, persisting it to the repository layer.
     * @param iEntity $entity
     *
     * @return iEntity|mixed
     */
    public function save(iEntity $entity) {
        // FIXME - check for and evaluate authorization policy, if it exists.
        $dto = $this->repository->persist($entity);
        return $this->createEntity($dto);
    }
    
    
    
    /**
     * Hydrates an entity object out of the provided $record
     * @param null $record
     *
     * @return iEntity|mixed
     */
    public function createEntity($record = null) {
        $entityClass = $this->entityClass;
        return $entityClass::hydrateFromArray($record);
    }
    
    
    
    /**
     * Finds the entity object associated with the given id.
     * @param int $id
     *
     * @return iEntity|mixed
     */
    public function findById(int $id) {
        $dto = $this->repository->findById($id);
        return $this->createEntity($dto);
    }
    
    
    
    /**
     * Fetches a collection of entities based on the given $filter
     * @param iFilter $filter
     *
     * @return EntityCollection
     */
    public function fetch(iFilter $filter): EntityCollection {
        $out = $this->repository->fetch($filter);
        
        $collection = new EntityCollection();
        $collection->more = $out['more'];
        foreach ($out['results'] as $result) {
            $collection->results[$result['id']] = $this->createEntity($result);
        }
        
        return $collection;
    }
}