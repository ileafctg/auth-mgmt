<?php

namespace IleafCtg\AuthMgmt\Utils;

use IleafCtg\AuthMgmt\Core\Exceptions\ApplicationException;

class Parser {
    /**
     * Takes a value and returns either true or false. Returns $default
     * if $value is empty.
     * Example:
     *       $bool = sanitizeTF($value)
     *
     * @param mixed $value      Bool or String representing true or false (i.e. 'yes', 'no', 'y', 'n', '1', '0', 'true', 'false', 't', 'f', 'off', 'on')
     * @param bool  $default    Optional. true if not specified.
     *
     * @return bool|mixed
     */
    public static function sanitizeTF($value="", $default=true) {
        if ($value === true) return true;
        if ($value === false) return false;
        if ($value === "") return $default;
        $value = strtolower($value);
        if ($value == 'no' or $value == 'n' or $value == 'off' or $value == 'false' or $value == 'f' or $value == '0')
            return false;
        else if ($value == 'yes' or $value == 'y' or $value == 'on' or $value == 'true' or $value == 't' or $value == '1')
            return true;
        else
            return $default;
    }
    
    
    
    /**
     * Takes a value and returns either 1 or 0. Returns $default
     * if $value is empty.
     * Example:
     *       $bool = sanitizeBoolOneOrZero($value)
     *
     * @param mixed $value
     * @param int  $default    Optional. 1 if not specified.
     *
     * @return int
     */
    public static function sanitizeBoolOneOrZero($value = '', int $default = 1): int {
        $output = static::sanitizeTF($value, $default === 1);
        return ($output ? 1 : 0);
    }
    
    
    
    /**
     * Parses an array to determine how many levels deep it goes.
     * It uses print_r() to efficiently parse the array into a string that 
     * can be plumbed for depth.  Inspired by https://stackoverflow.com/a/263621/339532
     * 
     * NOTE: This has not been checked for how well it could handle a very large array.  Should work
     * but the larger it is the more overhead as far as mem/cpu would be involved.
     * 
     * @param $array
     *
     * @return float  The number of levels contained in the array.
     * @throws ApplicationException
     */
    public static function arrayDepth($array) {
        if (!is_array($array)) {
            throw new ApplicationException("Must be an array that's passed to this method");
        }
        
        $actualDepth = 1;
    
        $arrayString = print_r($array, true);
        $lines = explode("\n", $arrayString);
    
        foreach ($lines as $line) {
            $indentationLevel = (strlen($line) - strlen(ltrim($line))) / 4;
        
            if ($indentationLevel > $actualDepth) {
                $actualDepth = $indentationLevel;
            }
        }
    
        return ceil(($actualDepth - 1) / 2) + 1;
    }
    
    /**
     * @param string $dottedString
     *
     * @return array
     * @throws ApplicationException
     */
    public static function dottedStringToFlatArray(string $dottedString):array {
        // Throw an exception if this is not a string
        if (!is_string($dottedString)) {
            throw new ApplicationException("Expected string, got something else");
        }
        
        $flatArray = explode(".", $dottedString);
        return $flatArray;
    }
    
    /**
     * @param string|array $dottedString   Dotted-notation string like "auth.user.name" to convert into an array like ['auth']['user']['name']
     * @param null|mixed   $value          Value to assign to the lowermost (leaf) node of the resulting dottedStringToArray array.
     *
     * @return array
     * @throws ApplicationException
     */
    public static function dottedStringToNestedArrayWithOptionalValue(string $dottedString, $value = null):array {
        // Throw an exception if this is not a string
        if (!is_string($dottedString)) {
            throw new ApplicationException("Expected string, got something else");
        }
        
        // Convert it into an array, split on dots in the string.  I.e. "auth.authz.user" becomes ['auth']['authz']['user']
        $flatArray = explode(".", $dottedString);
        $nestedArray = [];
        $current = &$nestedArray;
        foreach($flatArray as $element) {
            $current[$element] = [];
            $current = &$current[$element];
        }
        
        // If we have a value, assign it.
        if (isset($value)) {
            $current = $value;
        }
        return $nestedArray;
    }
    
    /**
     * @param array $sourceArray
     * @param array $keys
     *
     * @return mixed|null
     */
    public static function getNestedValue(array &$sourceArray, array $keys) {
        $key = array_shift($keys);
        
        if (!isset($sourceArray[$key])) {
            return null;
        }
        if (is_array($sourceArray[$key])) {
            return self::getNestedValue($sourceArray[$key], $keys);
        }
        return $sourceArray[$key];
    }
    
}