<?php

namespace IleafCtg\AuthMgmt\Modules\UserMgmt;

use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\iEntity;
use IleafCtg\AuthMgmt\Core\BaseRepoSupplemental\iRepository;
use IleafCtg\AuthMgmt\Core\BaseService;
use IleafCtg\AuthMgmt\Modules\UserMgmt\Entities\User;

class UserService extends BaseService {
    
    /**
     * @param iRepository $repository
     */
    public function __construct($repository) {
        parent::__construct(User::class, $repository);
    }
    
    
    
    /**
     * @param User $entity
     *
     * @return User
     */
    public function save(iEntity $entity): User {
        return parent::save($entity);
    }
    
    
    
}
