<?php

namespace IleafCtg\AuthMgmt\Modules\UserMgmt\Internal;

use Aura\Sql\ExtendedPdoInterface;
use IleafCtg\AuthMgmt\Core\BaseDataMapperSupplemental\iDataMapper;
use IleafCtg\AuthMgmt\Core\BaseRepository;
use IleafCtg\AuthMgmt\Modules\UserMgmt\Entities\UserDataMapper;
use IleafCtg\PdoSimple\PdoSimple;

class UserServiceRepository extends BaseRepository {
    const REPO_TABLE = 'mt_users';
    
    protected PdoSimple $pdoSimple;
    protected iDataMapper $dataMapper;
    
    public function __construct(ExtendedPdoInterface $pdo) {
        if (!empty($pdo)) {
            $this->pdoSimple = new PdoSimple($pdo);
        }
        $this->dataMapper = UserDataMapper::instance();
    }
}