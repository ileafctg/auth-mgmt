<?php

namespace IleafCtg\AuthMgmt\Modules\UserMgmt\Internal;

use DateTime;
use IleafCtg\AuthMgmt\Core\BaseFilter;
use IleafCtg\AuthMgmt\Modules\UserMgmt\Entities\UserDataMapper;

/**
 *
 */
class UserServiceFilter extends BaseFilter {
    
    public DateTime $accessedSince;
    
    public function getFilters(): array {
        $filtersArray = [];
        
        if (isset($this->ids)) {
            // It's okay for this to be an array
            $filtersArray['id'] = $this->ids;
        }
        if (isset($this->deleted)) {
            $filtersArray['deleted'] = ($this->deleted ? 1 : 0);
        }
        if (isset($this->modifiedSince)) {
            $filtersArray['mtime'] = ">= " . $this->modifiedSince->format('Y-m-d H:i:s');
        }
        if (isset($this->accessedSince)) {
            $filtersArray['atime'] = ">= " . $this->accessedSince->format('Y-m-d H:i:s');
        }
        
        return $filtersArray;
    }
    
    public function getOrderBy(): string {
        $dataMapper = UserDataMapper::instance();
        return $dataMapper->getEntityToRepoFieldName('id');
    }
}