<?php
namespace IleafCtg\AuthMgmt\Modules\UserMgmt\Entities;

use DateTime;
use DateTimeZone;
use IleafCtg\AuthMgmt\Core\BaseEntity;
use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\PropInfo;
use IleafCtg\AuthMgmt\Core\Exceptions\ApplicationException;

/**
 * @property-read int $id
 * @property-read int $clientId
 * @property-read int $groupId
 * @property string $firstName
 * @property string $lastName
 * @property bool $deleted
 * @property bool $enabled
 * @property-read DateTime $ctime
 * @property-read DateTime $mtime
 * @property-read DateTime $atime
 */
class User extends BaseEntity {
    
    /** @var string */
    protected $name;
    protected $deleted;
    protected $clientId;
    protected DateTime $atime;
    
    
    public function initProps() {
        parent::initProps();
        $this->addProp('firstName', PropInfo::STRING, PropInfo::READ_WRITE);
        $this->addProp('lastName', PropInfo::STRING, PropInfo::READ_WRITE);
        $this->addProp('deleted', PropInfo::BOOL, PropInfo::READ_WRITE);
        $this->addProp('enabled', PropInfo::BOOL, PropInfo::READ_WRITE);
        $this->addProp('groupId', PropInfo::INT, PropInfo::READ_WRITE);
        $this->addProp('clientId', PropInfo::INT, PropInfo::READ_ONLY);
        $this->addProp('atime', PropInfo::DATETIME, PropInfo::READ_ONLY);
    }
    
    
    /**
     * Set the clientId for a new user (cannot change once it's set to something).
     *
     * @param int $clientId
     *
     * @throws ApplicationException
     */
    public function setClientId(int $clientId) {
        if (isset($this->clientId)) {
            throw new ApplicationException("You cannot change the clientId of a user, you may only set it when creating a new user.");
        }
        $this->clientId = $clientId;
    }
    
    
    
    /**
     * Set the groupId for a new user.
     * @param int $groupId
     */
    public function setGroupId(int $groupId) {
        $this->groupId = $groupId;
    }
    
    
    
    /**
     * Set the atime (accessed time) for the user.
     * @param DateTime|null $atime
     */
    public function setAtime(DateTime $atime = null) {
        // If no $atime was passed in, assume 'now'
        if (is_null($atime)) {
            $atime = new DateTime();
        }
        
        // Always record value in UTC, regardless of what caller passed DateTime is in.
        $atime->setTimezone(new DateTimeZone("UTC"));
        
        $this->atime = $atime;
    }
    
    
}