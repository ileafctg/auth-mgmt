<?php

namespace IleafCtg\AuthMgmt\Modules\UserMgmt\Entities;

use DateTime;
use DateTimeZone;
use IleafCtg\AuthMgmt\Core\BaseDataMapper;
use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\iEntity;
use IleafCtg\AuthMgmt\Core\Exceptions\ApplicationException;
use IleafCtg\AuthMgmt\Utils\Parser;

class UserDataMapper extends BaseDataMapper {
    
    protected array $entityToRepoDtoFieldMap = [
        'id' => 'id',
        'clientId' => 'client_id',
        'groupId' => 'group_id',
        'firstName' => 'first_name',
        'lastName' => 'last_name',
        'deleted' => 'deleted',
        'enabled' => 'enabled',
        'mtime' => 'mtime',
        'ctime' => 'ctime',
        'atime' => 'atime',
    ];
    
    protected array $requiredRepoDtoFields = [
        'first_name',
        'client_id',
    ];
    
    protected array $requiredEntityDtoFields = [
        'id',
        'clientId',
        'firstName',
        'deleted',
        'enabled',
        'ctime',
        'mtime',
        'atime',
    ];
    
    
    
    /**
     * @param User $entity
     *
     * @return array
     * @throws ApplicationException
     */
    public function entityToRepoDto(iEntity $entity): array {
        $dto = parent::entityToRepoDto($entity);
        
        $dto['deleted'] = (isset($dto['deleted']) ? Parser::sanitizeBoolOneOrZero($dto['deleted']) : 0);
        $dto['enabled'] = (isset($dto['enabled']) ? Parser::sanitizeBoolOneOrZero($dto['enabled']) : 1);
        $dto['mtime'] = (new DateTime())->setTimezone(new DateTimeZone('UTC'))->format('Y-m-d H:i:s');
        // atime gets set outside of here, we just need to make sure it's the right format for our Repo layer.
        if (isset($dto['atime'])) {
            $dto['atime'] = $entity->atime->format('Y-m-d H:i:s');
        }
        
        if (!isset($dto['id'])) {
            // If getting created, we'll set the ctime as well
            $dto['ctime'] = $dto['mtime'];
    
            // If atime is not set, and this entity is getting created, let's set the atime
            // to some value in the past, so that we know that this record has never been accessed
            // until something explicitly sets the atime.  Otherwise, the atime gets set
            // to CURRENT_TIMESTAMP in the DB.
            if (!isset($dto['atime'])) {
                $dto['atime'] = '1901-01-01 00:00:00';
            }
            
            // If group_id is not set, default to zero
            if (!isset($dto['group_id'])) {
                $dto['group_id'] = 0;
            }
        }
        // Otherwise, the ctime should never be adjusted.
        else {
            unset($dto['ctime']);
        }
        
        return $dto;
    }
    
    
}