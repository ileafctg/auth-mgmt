<?php

namespace IleafCtg\AuthMgmt\Modules\SsoMgmt\Entities;

use DateTime;
use IleafCtg\AuthMgmt\Core\BaseEntity;
use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\PropInfo;


/**
 * @property-read int $id
 * @property-read DateTime $ctime
 * @property-read DateTime $mtime
 * @property string $ssoProvider
 * @property string $ssoIdentifier
 * @property string $email
 * @property bool $emailVerified
 * @property string $name
 * @property string $givenName
 * @property string $familyName
 * @property string $locale
 * @property string $picture
 *                             
 */
class Sso extends BaseEntity {
    protected $ssoProvider;
    protected $ssoIdentifier;
    protected $email;
    protected $emailVerified;
    protected $name;
    protected $givenName;
    protected $familyName;
    
    public function initProps() {
        parent::initProps();
        $this->addProp('ssoProvider', PropInfo::STRING, PropInfo::READ_WRITE);
        $this->addProp('ssoIdentifier', PropInfo::STRING, PropInfo::READ_WRITE);
        $this->addProp('email', PropInfo::STRING, PropInfo::READ_WRITE);
        $this->addProp('emailVerified', PropInfo::BOOL, PropInfo::READ_WRITE);
        $this->addProp('name', PropInfo::STRING, PropInfo::READ_WRITE);
        $this->addProp('givenName', PropInfo::STRING, PropInfo::READ_WRITE);
        $this->addProp('familyName', PropInfo::STRING, PropInfo::READ_WRITE);
        $this->addProp('locale', PropInfo::STRING, PropInfo::READ_WRITE);
        $this->addProp('picture', PropInfo::STRING, PropInfo::READ_WRITE);
    }
    
    
}