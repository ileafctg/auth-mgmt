<?php

namespace IleafCtg\AuthMgmt\Modules\SsoMgmt\Entities;

use DateTime;
use DateTimeZone;
use IleafCtg\AuthMgmt\Core\BaseDataMapper;
use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\iEntity;
use IleafCtg\AuthMgmt\Core\Exceptions\ApplicationException;
use IleafCtg\AuthMgmt\Utils\Parser;

class SsoDataMapper extends BaseDataMapper {
    
    protected array $entityToRepoDtoFieldMap = [
        'id' => 'id',
        'ssoProvider' => 'sso_provider',
        'ssoIdentifier' => 'sso_identifier',
        'email' => 'email',
        'emailVerified' => 'email_verified',
        'name' => 'name',
        'givenName' => 'given_name',
        'familyName' => 'family_name',
        'locale' => 'locale',
        'picture' => 'picture_uri',
        'mtime' => 'mtime',
        'ctime' => 'ctime',
    ];
    
    protected array $requiredRepoDtoFields = [
        'sso_provider',
        'sso_identifier',
    ];
    
    protected array $requiredEntityDtoFields = [
        'id',
        'ssoProvider',
        'ssoIdentifier',
        'ctime',
        'mtime',
    ];
    
    
    
    /**
     * @param Sso $entity
     *
     * @return array
     * @throws ApplicationException
     */
    public function entityToRepoDto(iEntity $entity): array {
        $dto = parent::entityToRepoDto($entity);
        
        $dto['mtime'] = (new DateTime())->setTimezone(new DateTimeZone('UTC'))->format('Y-m-d H:i:s');
        
        if (!isset($dto['id'])) {
            // If getting created, we'll set the ctime as well
            $dto['ctime'] = $dto['mtime'];
        }
        // Otherwise, the ctime should never be adjusted.
        else {
            unset($dto['ctime']);
        }
        
        return $dto;
    }
    
    
}