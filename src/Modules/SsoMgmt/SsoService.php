<?php

namespace IleafCtg\AuthMgmt\Modules\SsoMgmt;

use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\iEntity;
use IleafCtg\AuthMgmt\Core\BaseRepoSupplemental\iRepository;
use IleafCtg\AuthMgmt\Core\BaseService;
use IleafCtg\AuthMgmt\Core\Exceptions\ApplicationException;
use IleafCtg\AuthMgmt\Modules\ClientMgmt\Entities\Client;
use IleafCtg\AuthMgmt\Modules\SsoMgmt\Entities\Sso;
use IleafCtg\AuthMgmt\Modules\UserMgmt\Entities\User;
use IleafCtg\AuthMgmt\Modules\UserMgmt\UserService;
use IleafCtg\AuthMgmtTest\App;

class SsoService extends BaseService {
    
    /** @var UserService */
    private $userService;
    
    /**
     * @param iRepository $repository
     */
    public function __construct($repository, UserService $userService) {
        parent::__construct(Sso::class, $repository);
        $this->userService = $userService;
    }
    
    
    
    /**
     * @override
     */
    public function save(iEntity $sso) {
        // If a matching SSO already exists based on provider and identifier, just update it.
        /** @var Sso $sso */
        $existingSso = $this->findBySsoIdentifier($sso->ssoProvider, $sso->ssoIdentifier);
        if ($existingSso) {
            $existingSso->email = $sso->email;
            $existingSso->name = $sso->name;
            $existingSso->givenName = $sso->givenName;
            $existingSso->familyName = $sso->familyName;
            $existingSso = parent::save($existingSso);
    
            return $existingSso;
        }
        // otherwise create a record for it.
        else {
            return parent::save($sso);
        }
    }
    
    
    
    /**
     * @param string $ssoProvider
     * @param string $ssoIdentifier
     *
     * @return Sso|null
     */
    public function findBySsoIdentifier(string $ssoProvider, string $ssoIdentifier) {
        $dto = $this->repository->findBySsoIdentifier($ssoProvider, $ssoIdentifier);
        if ($dto) {
            return $this->createEntity($dto);
        }
        return null;
    }
    
    
    public function linkSsoToUser(Sso $sso, User $user) {
        // Make sure each are persisted to our DB first (must have an id)
        if (!$sso->id || !$user->id) {
            throw new ApplicationException("You must persist your SSO and User objects to the repository before trying to link them.");
        }
        
        $linkRecordId = $this->repository->linkSsoToUser($sso, $user);
        
        if ($linkRecordId) {
            return true;
        }
        return false;
    }
    
    
    
    /**
     * @param Sso         $sso
     * @param Client|null $client
     *
     * @return User[]
     */
    public function fetchUsersViaSso(Sso $sso, Client $client = null) {
        $dtos = $this->repository->findUsersBySsoIdentifier($sso->ssoProvider, $sso->ssoIdentifier, $client);
        $return = [];
        foreach ($dtos as $dto) {
            $return[] = User::hydrateFromArray($dto);
        }
        
        return $return;
    }
    
    
    
    /**
     * @param User $user
     *
     * @return Sso[]
     */
    public function fetchSsoEntitiesForUser(User $user) {
        $dtos = $this->repository->findSsoRecordsForUser($user);
        $return = [];
        foreach ($dtos as $dto) {
            $return[] = $this->createEntity($dto);
        }
        
        return $return;
    }
    
    public function setUpNewUserViaSSOSignUp(Sso $sso, Client $client): User {
        
        // See if we already have an entry for this SSO from previous.
        $existingSso = $this->findBySsoIdentifier($sso->ssoProvider, $sso->ssoIdentifier);
        
        // Persist the SSO record
        if ($existingSso) {
            // Update anything that may have changed since last time we touched this record
            $existingSso->email = $sso->email;
            $existingSso->name = $sso->name;
            $existingSso->givenName = $sso->givenName;
            $existingSso->familyName = $sso->familyName;
            $existingSso->emailVerified = $sso->emailVerified;
            $existingSso->locale = $sso->locale;
            $existingSso->picture = $sso->picture;
            
            $existingSso = $this->save($existingSso);
            
            $sso = $existingSso;
        }
        else {
            $sso = $this->save($sso);
        }
        
        // Do we happen to already have a user on the given client with this SSO?  If so, just return it.
        $users = $this->fetchUsersViaSso($sso, $client);
        if ($users) {
            if (count($users > 1)) {
                throw new ApplicationException("Looks like there is more than one user for the given client already linked to the given SSO. This should never happen - investigate.");
            }
            $user = $users[0];
        }
        else {
            // Create a new Client/User within the single org, tied to the SSO
            $user = new User();
            $user->firstName = $sso->givenName;
            $user->lastName = $sso->familyName;
            $user->setClientId($client->id);
            $user = $this->userService->save($user);
        }
        
        // Link the two together in the users-SSO table
        if ($this->linkSsoToUser($sso, $user)) {
            return $user;   
        }
        throw new ApplicationException("Problem linking sso credentials to user");
        
    }
    
}