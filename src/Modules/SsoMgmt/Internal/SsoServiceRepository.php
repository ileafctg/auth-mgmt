<?php

namespace IleafCtg\AuthMgmt\Modules\SsoMgmt\Internal;

use Aura\Sql\ExtendedPdoInterface;
use IleafCtg\AuthMgmt\Core\BaseDataMapperSupplemental\iDataMapper;
use IleafCtg\AuthMgmt\Core\BaseRepository;
use IleafCtg\AuthMgmt\Modules\ClientMgmt\Entities\Client;
use IleafCtg\AuthMgmt\Modules\SsoMgmt\Entities\Sso;
use IleafCtg\AuthMgmt\Modules\SsoMgmt\Entities\SsoDataMapper;
use IleafCtg\AuthMgmt\Modules\UserMgmt\Entities\User;
use IleafCtg\AuthMgmt\Modules\UserMgmt\Entities\UserDataMapper;
use IleafCtg\PdoSimple\PdoSimple;

class SsoServiceRepository extends BaseRepository {
    const REPO_TABLE = 'mt_sso_identity_external';
    const SSO_USER_LINK_TABLE = 'mt_sso_identity_links';
    
    protected PdoSimple $pdoSimple;
    protected iDataMapper $dataMapper;
    protected UserDataMapper $userDataMapper;
    
    public function __construct(ExtendedPdoInterface $pdo) {
        if (!empty($pdo)) {
            $this->pdoSimple = new PdoSimple($pdo);
        }
        $this->dataMapper = SsoDataMapper::instance();
        $this->userDataMapper = UserDataMapper::instance();
    }
    
    public function findBySsoIdentifier(string $ssoProvider, string $ssoIdentifier): array {
        $record = $this->pdoSimple->dbGetRecord(static::REPO_TABLE, ['sso_provider' => $ssoProvider, 'sso_identifier' => $ssoIdentifier]);
        if ($record) {
            $dto = $this->dataMapper->repoToEntityDto($record);
            return $dto;
        }
        return [];
    }
    
    public function findUsersBySsoIdentifier(string $ssoProvider, string $ssoIdentifier, Client $client = null): array {
        // TODO: May be necessary to filter by organization id at some point
    
        // Where clause array
        $where = [
            'sso_type' => 'external',
            'sso_provider' => $ssoProvider,
            'sso_identifier' => $ssoIdentifier,
        ];
        
        // If Client provided, filter by client_id
        $where_client = '';
        if ($client) {
            $where_client = "AND users.client_id = :client_id";
            $where['client_id'] = $client->id;
        }
        $query = " 
            select users.*
            
            from mt_sso_identity_links links
             join mt_users users on (links.user_id = users.id)
             join mt_sso_identity_external sso on (links.sso_id = sso.id)
             join mt_clients on (mt_clients.id = users.client_id)
             join mt_organizations on (mt_clients.organization_id = mt_organizations.id)
            where links.sso_type = :sso_type
             and sso_provider = :sso_provider
             and sso_identifier = :sso_identifier
             and users.enabled = 1
             and users.deleted = 0
             and mt_clients.deleted = 0
             and mt_organizations.deleted = 0
            {$where_client}
            ORDER BY users.id
        ";
    
        $records = $this->pdoSimple->getPdoInstance()->fetchAssoc($query, $where);
        
        $results = [];
        foreach ($records as $record) {
            $results[] = $this->userDataMapper->repoToEntityDto($record);
        }
        
        // TODO: I doubt we'll ever need to paginate these results, but leaving a to-do here just in case.
        return $results;
        
    }
    
    public function findSsoRecordsForUser(User $user): array {
        $query = " 
            select sso.*
            
            from mt_sso_identity_links links
             join mt_users users on (links.user_id = users.id)
             join mt_sso_identity_external sso on (links.sso_id = sso.id)
             join mt_clients on (mt_clients.id = users.client_id)
             join mt_organizations on (mt_clients.organization_id = mt_organizations.id)
            where links.sso_type = :sso_type
             and users.id = :user_id
             and users.enabled = 1
             and users.deleted = 0
             and mt_clients.deleted = 0
             and mt_organizations.deleted = 0
            ORDER BY users.id
        ";
    
        $where = [
            'sso_type' => 'external',
            'user_id' => $user->id,
        ];
    
        $records = $this->pdoSimple->getPdoInstance()->fetchAssoc($query, $where);
    
        $results = [];
        foreach ($records as $record) {
            $results[] = $this->dataMapper->repoToEntityDto($record);
        }
        
        return $results;
    }
    
    public function linkSsoToUser(Sso $ssoEntity, User $userEntity) {
        // See if there's already a link
        $record = $this->pdoSimple->dbGetRecord(static::SSO_USER_LINK_TABLE, ['user_id' => $userEntity->id, 'sso_type' => 'external', 'sso_id' => $ssoEntity->id]);
        
        $linkedId = 0;
        if ($record) {
            $linkedId = $record['id'];
        }
        else {
            $data = [
                'user_id' => $userEntity->id,
                'sso_id' => $ssoEntity->id,
                'sso_type' => 'external',
            ];
            $this->pdoSimple->dbInsertRecord(static::SSO_USER_LINK_TABLE, $data);
            $linkedId = $this->pdoSimple->getLastInsertId();
        }
        
        return $linkedId;
    }
    
}