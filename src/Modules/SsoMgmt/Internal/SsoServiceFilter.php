<?php

namespace IleafCtg\AuthMgmt\Modules\SsoMgmt\Internal;

use DateTime;
use IleafCtg\AuthMgmt\Core\BaseFilter;
use IleafCtg\AuthMgmt\Core\Exceptions\ApplicationException;
use IleafCtg\AuthMgmt\Modules\UserMgmt\Entities\UserDataMapper;

/**
 *
 */
class SsoServiceFilter extends BaseFilter {
    
    public string $ssoIdentifier;
    public string $ssoProvider;
    
    public function getFilters(): array {
        $filtersArray = [];
        
        if (isset($this->ids)) {
            // It's okay for this to be an array
            $filtersArray['id'] = $this->ids;
        }
        if (isset($this->ssoIdentifier)) {
            if (!isset($this->ssoProvider)) {
                throw new ApplicationException("Must include an ssoProvider if filtering on ssoIdentifier");
            }
            $filtersArray['sso_identifier'] = $this->ssoIdentifier;
        }
        if (isset($this->ssoProvider)) {
            if (!isset($this->ssoIdentifier)) {
                throw new ApplicationException("Must include an ssoIdentifier if filtering on ssoProvider");
            }
            $filtersArray['sso_provider'] = $this->ssoProvider;
        }
        return $filtersArray;
    }
    
    public function getOrderBy(): string {
        $dataMapper = UserDataMapper::instance();
        return $dataMapper->getEntityToRepoFieldName('id');
    }
}