<?php

namespace IleafCtg\AuthMgmt\Modules\GroupMgmt\Entities;

use DateTime;
use DateTimeZone;
use IleafCtg\AuthMgmt\Core\BaseDataMapper;
use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\iEntity;
use IleafCtg\AuthMgmt\Utils\Parser;

class GroupDataMapper extends BaseDataMapper {
    
    protected array $entityToRepoDtoFieldMap = [
        'id' => 'id',
        'clientId' => 'client_id',
        'name' => 'name',
        'deleted' => 'deleted',
        'mtime' => 'mtime',
        'ctime' => 'ctime',
    ];
    
    protected array $requiredRepoDtoFields = [
        'name',
        'client_id',
    ];
    
    protected array $requiredEntityDtoFields = [
        'id',
        'clientId',
        'name',
        'deleted',
        'ctime',
        'mtime',
    ];
    
    
    
    public function entityToRepoDto(iEntity $entity): array {
        $dto = parent::entityToRepoDto($entity);
        
        $dto['deleted'] = (isset($dto['deleted']) ? Parser::sanitizeBoolOneOrZero($dto['deleted']) : 0);
        $dto['mtime'] = (new DateTime())->setTimezone(new DateTimeZone('UTC'))->format('Y-m-d H:i:s');
        
        if (!isset($dto['id'])) {
            $dto['ctime'] = $dto['mtime'];
        }
        else {
            unset($dto['ctime']);
        }
        
        return $dto;
    }
    
    
    
}