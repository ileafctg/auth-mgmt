<?php
namespace IleafCtg\AuthMgmt\Modules\GroupMgmt\Entities;

use DateTime;
use IleafCtg\AuthMgmt\Core\BaseEntity;
use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\PropInfo;
use IleafCtg\AuthMgmt\Core\Exceptions\ApplicationException;

/**
 * @property-read int $id
 * @property-read int $clientId
 * @property string $name
 * @property bool $deleted
 * @property-read DateTime $ctime
 * @property-read DateTime $mtime
 */
class Group extends BaseEntity {
    
    /** @var string */
    protected $name;
    protected $deleted;
    protected $clientId;
    protected DateTime $atime;
    
    
    public function initProps() {
        parent::initProps();
        $this->addProp('name', PropInfo::STRING, PropInfo::READ_WRITE);
        $this->addProp('deleted', PropInfo::BOOL, PropInfo::READ_WRITE);
        $this->addProp('clientId', PropInfo::INT, PropInfo::READ_ONLY);
    }
    
    
    /**
     * Set the clientId for a new group (cannot change once it's set to something).
     *
     * @param int $clientId
     *
     * @throws ApplicationException
     */
    public function setClientId(int $clientId) {
        if (isset($this->clientId)) {
            throw new ApplicationException("You cannot change the clientId of a group, you may only set it when creating a new group.");
        }
        $this->clientId = $clientId;
    }
    
}