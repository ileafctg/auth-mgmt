<?php

namespace IleafCtg\AuthMgmt\Modules\GroupMgmt;

use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\iEntity;
use IleafCtg\AuthMgmt\Core\BaseRepoSupplemental\iRepository;
use IleafCtg\AuthMgmt\Core\BaseService;
use IleafCtg\AuthMgmt\Modules\GroupMgmt\Entities\Group;

class GroupService extends BaseService {
    /**
     * @param iRepository $repository
     */
    public function __construct($repository) {
        parent::__construct(Group::class, $repository);
    }
    
    
    
    /**
     * @param Group $entity
     *
     * @return Group
     */
    public function save(iEntity $entity): Group {
        return parent::save($entity);
    }
    
}
