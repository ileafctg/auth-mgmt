<?php

namespace IleafCtg\AuthMgmt\Modules\GroupMgmt\Internal;

use Aura\Sql\ExtendedPdoInterface;
use IleafCtg\AuthMgmt\Core\BaseDataMapperSupplemental\iDataMapper;
use IleafCtg\AuthMgmt\Core\BaseRepository;
use IleafCtg\AuthMgmt\Modules\GroupMgmt\Entities\GroupDataMapper;
use IleafCtg\PdoSimple\PdoSimple;

class GroupServiceRepository extends BaseRepository {
    const REPO_TABLE = 'mt_groups';
    
    protected PdoSimple $pdoSimple;
    protected iDataMapper $dataMapper;
    
    public function __construct(ExtendedPdoInterface $pdo) {
        if (!empty($pdo)) {
            $this->pdoSimple = new PdoSimple($pdo);
        }
        $this->dataMapper = GroupDataMapper::instance();
    }
}