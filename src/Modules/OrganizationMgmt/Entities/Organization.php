<?php
namespace IleafCtg\AuthMgmt\Modules\OrganizationMgmt\Entities;

use DateTime;
use IleafCtg\AuthMgmt\Core\BaseEntity;
use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\PropInfo;

/**
 * @property-read int $id
 * @property string $name
 * @property bool $deleted
 * @property-read DateTime $ctime
 * @property-read DateTime $mtime
 */
class Organization extends BaseEntity {
    
    /** @var string */
    protected $name;
    protected $deleted;
    
    public function initProps() {
        parent::initProps();
        $this->addProp('name', PropInfo::STRING, PropInfo::READ_WRITE);
        $this->addProp('deleted', PropInfo::BOOL, PropInfo::READ_WRITE);
    }
    
}