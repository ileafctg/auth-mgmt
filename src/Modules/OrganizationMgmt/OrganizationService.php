<?php

namespace IleafCtg\AuthMgmt\Modules\OrganizationMgmt;

use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\iEntity;
use IleafCtg\AuthMgmt\Core\BaseRepoSupplemental\iRepository;
use IleafCtg\AuthMgmt\Core\BaseService;
use IleafCtg\AuthMgmt\Modules\OrganizationMgmt\Entities\Organization;

class OrganizationService extends BaseService {
    
    /**
     * @param iRepository $repository
     */
    public function __construct($repository) {
        parent::__construct(Organization::class, $repository);
    }
    
    public function save(iEntity $entity) {
        return parent::save($entity);
    }
    
}
