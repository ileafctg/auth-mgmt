<?php
namespace IleafCtg\AuthMgmt\Modules\OrganizationMgmt\Internal;

use Aura\Sql\ExtendedPdoInterface;
use IleafCtg\AuthMgmt\Core\BaseDataMapperSupplemental\iDataMapper;
use IleafCtg\AuthMgmt\Core\BaseRepository;
use IleafCtg\AuthMgmt\Modules\OrganizationMgmt\Entities\OrganizationDataMapper;
use IleafCtg\PdoSimple\PdoSimple;

class OrganizationManagementServiceRepository extends BaseRepository {
    
    const REPO_TABLE = 'mt_organizations';
    
    protected PdoSimple $pdoSimple;
    protected iDataMapper $dataMapper;
    
    public function __construct(ExtendedPdoInterface $pdo) {
        if (!empty($pdo)) {
            $this->pdoSimple = new PdoSimple($pdo);
        }
        $this->dataMapper = OrganizationDataMapper::instance();
    }
    
    
    
}