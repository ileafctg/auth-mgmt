<?php
namespace IleafCtg\AuthMgmt\Modules\ClientMgmt\Entities;

use DateTime;
use IleafCtg\AuthMgmt\Core\BaseEntity;
use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\PropInfo;
use IleafCtg\AuthMgmt\Core\Exceptions\ApplicationException;

/**
 * @property-read int $id
 * @property-read int $organizationId
 * @property string $name
 * @property bool $deleted
 * @property-read DateTime $ctime
 * @property-read DateTime $mtime
 */
class Client extends BaseEntity {
    
    /** @var string */
    protected $name;
    protected $deleted;
    protected $organizationId;
    
    
    public function initProps() {
        parent::initProps();
        $this->addProp('name', PropInfo::STRING, PropInfo::READ_WRITE);
        $this->addProp('deleted', PropInfo::BOOL, PropInfo::READ_WRITE);
        $this->addProp('organizationId', PropInfo::INT, PropInfo::READ_ONLY);
    }
    
    
    /**
     * Set the organizationId for a new client (cannot change once it's set to something).
     * 
     * @param int $organizationId
     *
     * @throws ApplicationException
     */
    public function setOrganizationId(int $organizationId) {
        if (isset($this->organizationId)) {
            throw new ApplicationException("You cannot change the organizationId of a client, you may only set it when creating a new client.");
        }
        $this->organizationId = $organizationId;
    }
}