<?php

namespace IleafCtg\AuthMgmt\Modules\ClientMgmt\Entities;

use DateTime;
use DateTimeZone;
use IleafCtg\AuthMgmt\Core\BaseDataMapper;
use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\iEntity;
use IleafCtg\AuthMgmt\Utils\Parser;

class ClientDataMapper extends BaseDataMapper {
    
    protected array $entityToRepoDtoFieldMap = [
        'id' => 'id',
        'organizationId' => 'organization_id',
        'name' => 'name',
        'deleted' => 'deleted',
        'mtime' => 'mtime',
        'ctime' => 'ctime',
    ];
    
    protected array $requiredRepoDtoFields = [
        'name',
        'organization_id',
    ];
    
    protected array $requiredEntityDtoFields = [
        'id',
        'organizationId',
        'name',
        'deleted',
        'ctime',
        'mtime',
    ];
    
    
    
    public function entityToRepoDto(iEntity $entity): array {
        $dto = parent::entityToRepoDto($entity);
    
        $dto['deleted'] = (isset($dto['deleted']) ? Parser::sanitizeBoolOneOrZero($dto['deleted']) : 0);
        $dto['mtime'] = (new DateTime())->setTimezone(new DateTimeZone('UTC'))->format('Y-m-d H:i:s');
        
        if (!isset($dto['id'])) {
            $dto['ctime'] = $dto['mtime'];
        }
        else {
            unset($dto['ctime']);
        }
        
        return $dto;
    }
    
    
    
}