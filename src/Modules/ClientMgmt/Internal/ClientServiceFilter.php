<?php

namespace IleafCtg\AuthMgmt\Modules\ClientMgmt\Internal;

use IleafCtg\AuthMgmt\Core\BaseFilter;
use IleafCtg\AuthMgmt\Modules\ClientMgmt\Entities\ClientDataMapper;

/**
 * 
 */
class ClientServiceFilter extends BaseFilter {
    
    public function getFilters(): array {
        $filtersArray = [];
        
        if (isset($this->ids)) {
            // It's okay for this to be an array
            $filtersArray['id'] = $this->ids;
        }
        if (isset($this->deleted)) {
            $filtersArray['deleted'] = ($this->deleted ? 1 : 0);
        }
        if (isset($this->modifiedSince)) {
            $filtersArray['mtime'] = ">= " . $this->modifiedSince->format('Y-m-d H:i:s');
        }
        
        return $filtersArray;
    }
    
    public function getOrderBy(): string {
        $dataMapper = ClientDataMapper::instance();
        return $dataMapper->getEntityToRepoFieldName('id');
    }
}