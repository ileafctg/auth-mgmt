<?php

namespace IleafCtg\AuthMgmt\Modules\ClientMgmt\Internal;

use Aura\Sql\ExtendedPdoInterface;
use IleafCtg\AuthMgmt\Core\BaseDataMapperSupplemental\iDataMapper;
use IleafCtg\AuthMgmt\Core\BaseRepository;
use IleafCtg\AuthMgmt\Modules\ClientMgmt\Entities\ClientDataMapper;
use IleafCtg\PdoSimple\PdoSimple;

class ClientServiceRepository extends BaseRepository {
    const REPO_TABLE = 'mt_clients';
    
    protected PdoSimple $pdoSimple;
    protected iDataMapper $dataMapper;
    
    public function __construct(ExtendedPdoInterface $pdo) {
        if (!empty($pdo)) {
            $this->pdoSimple = new PdoSimple($pdo);
        }
        $this->dataMapper = ClientDataMapper::instance();
    }
}