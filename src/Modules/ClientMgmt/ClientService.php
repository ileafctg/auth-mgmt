<?php

namespace IleafCtg\AuthMgmt\Modules\ClientMgmt;

use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\iEntity;
use IleafCtg\AuthMgmt\Core\BaseRepoSupplemental\iRepository;
use IleafCtg\AuthMgmt\Core\BaseService;
use IleafCtg\AuthMgmt\Modules\ClientMgmt\Entities\Client;

class ClientService extends BaseService {
    
    
    /**
     * @param iRepository $repository
     */
    public function __construct($repository) {
        parent::__construct(Client::class, $repository);
    }
    
    
    
    /**
     * @param Client $entity
     *
     * @return Client
     */
    public function save(iEntity $entity): Client {
        return parent::save($entity);
    }
}
