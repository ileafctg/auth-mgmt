# To-do list

### OrganizationMgmt
- [x] createOrg
- [x] updateOrg

### ClientMgmt
- [x] createClient
- [x] updateClient

### GroupMgmt
- [x] createGroup
- [x] updateGroup
- [-] addUserToGroup(user_id)
  - Will only add this if/when it's needed

### UserMgmt
- [x] createUser
- [x] updateUser
- [x] setGroup(group_id)  ## assign to zero to remove from group
- [ ] _setPermission(level, action, resource, allow|deny)
  - [ ] setGroupLevelPermission(group_id, action, resource, allow|deny)
  - [ ] removeGroupLevelPermission(group_id, action, resource)
  - [ ] setUserLevelPermission(user_id, action, resource, allow|deny)
  - [ ] removeUserLevelPermission(user_id, action, resource)
  - [ ] setClientLevelPermission(action, resource, allow|deny)
  - [ ] removeClientLevelPermission(action, resource)
- [ ] getEffectivePermissions()
- [ ] isAuthorized(action, resource)
- [ ] assignToRole(roleId)

### RoleMgmt
- [ ] createRole
- [ ] updateRole
- [ ] assignToUser(userId)
- [ ] _setPermission(level, action, resource, allow|deny)
    - [ ] setGroupLevelPermission(group_id, action, resource, allow|deny)
    - [ ] removeGroupLevelPermission(group_id, action, resource)
    - [ ] setUserLevelPermission(user_id, action, resource, allow|deny)
    - [ ] removeUserLevelPermission(user_id, action, resource)
    - [ ] setClientLevelPermission(action, resource, allow|deny)
    - [ ] removeClientLevelPermission(action, resource)



### Authentication
- [ ] private authenticate ??
- [ ] loginViaWeb
- [ ] fakeLogin


### CustomFields
- rules
  - Only one value per field per user
  - field entry types:
    - multivalue
    - freeform
  - field validation types
    - date
    - datetime
    - alphanumeric
    - bool
    - monetary
    - regex
- [ ] createField
- [ ] updateField
- [ ] createItem
- [ ] updateItem
- [ ] setFieldValue(userId, fieldId, value)
- [ ] removeFieldValue(userId, fieldId)