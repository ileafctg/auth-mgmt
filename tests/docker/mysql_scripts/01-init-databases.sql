# grant rights to root and project users on all databases.
GRANT ALL ON *.* TO 'root'@'%';
GRANT ALL ON *.* TO 'project'@'%';

