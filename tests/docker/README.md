# Setting up docker for unit tests

We've built out this framework to operate against a MySQL database.  To run all of the unit and integration tests, you'll need to have a MySQL instance up and running that the tests can connect and run against.

Assuming you have Docker installed, from this directory, run:
- `docker-compose build` (only need to run this once, unless the docker-compose.yml file changes)
- `docker-compose up -d` (to start up the MySQL container)

When not needed, you can stop the MySQL container with:
- `docker-compose down -v`
