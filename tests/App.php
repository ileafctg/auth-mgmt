<?php

namespace IleafCtg\AuthMgmtTest;
use DI\ContainerBuilder;

class App {
    private static $instances;
    private static $container;
    
    private function __construct() {
        $builder = new ContainerBuilder();
        $builder->addDefinitions(__DIR__ . '/diconfig.php');
        static::$container = $builder->build();
    }
    
    /**
     * Method for returning a singleton.
     *
     * @return App
     */
    public static function instance(): App {
        $class = get_called_class();
        
        if (!isset(static::$instances[$class])) {
            // Instantiate the singleton
            static::$instances[$class] = new static();
        }
        return static::$instances[$class];
    }
    
    public function get($className) {
        return static::$container->get($className);
    }
    
}