<?php

use IleafCtg\AuthMgmt\Core\Auth\iSession;
use IleafCtg\AuthMgmt\Core\MemorySession;
use IleafCtg\AuthMgmt\Modules\UserMgmt\Internal\UserServiceRepository;
use IleafCtg\AuthMgmt\Modules\UserMgmt\UserService;
use IleafCtg\AuthMgmtTest\Helpers\Pdo\MysqlPdo;
use IleafCtg\AuthMgmtTest\suites\main\TestAuthN;
use Psr\Container\ContainerInterface;


return [
    //// Bind an interface to an implementation
    //ArticleRepository::class => create(InMemoryArticleRepository::class),
    //
    //// Configure Twig
    //Environment::class => function () {
    //    $loader = new FilesystemLoader(__DIR__ . '/../src/SuperBlog/Views');
    //    return new Environment($loader);
    //},
    
    iSession::class => function(ContainerInterface $container) {
        return new MemorySession();
    },
    
    TestAuthN::class => function(ContainerInterface $container) {
        $session = $container->get(iSession::class);
        return TestAuthN::instance($session);
    },
    
    UserService::class => function(ContainerInterface $container) {
        $pdo = new MysqlPdo(getenv('PHOENIX_MYSQL_DATABASE'));
        return new UserService(new UserServiceRepository($pdo));
    }
];
