<?php

namespace IleafCtg\AuthMgmtTest\suites\main;
use IleafCtg\AuthMgmt\Core\MemorySession;
use PHPUnit\Framework\TestCase;

/**
 *
 * Tests related to Authentication/Authorization.
 *
 */
final class MemorySessionTest extends TestCase {
    
    
    
    public function testSessionStart() {
        $session = new MemorySession();
        $session->start();
        
        self::assertNotEmpty($session->getId());
        self::assertTrue($session->isStarted());
    }
    
    public function testSessionDestroy() {
        $session = new MemorySession();
        $session->start();
        
        self::assertNotEmpty($session->getId());
        self::assertTrue($session->isStarted());
        
        $oldId = $session->getId();
        
        // Set something in session
        $session->set("some.thing", "blarg");
        self::assertEquals("blarg", $session->get("some.thing"));
        self::assertNotEmpty($session->all());
        
        $session->destroy();
        $newId = $session->getId();
        self::assertEmpty($session->all());
        self::assertNotEquals($oldId, $newId);
    }
    
    public function testSessionSetGet() {
        $session = new MemorySession();
        $session->start();
        
        $key1 = "some.random.key";
        $value1 = "testString";
        $array1 = [
            'some' => [
                'random' => [
                    'key' => $value1
                ]
            ]
        ];
        
        // Set a key and make sure it all looks as expected
        $session->set($key1, $value1);
        self::assertEquals($value1, $session->get($key1));
        $all = $session->all();
        self::assertEquals($all, $array1);
        self::assertTrue($session->has($key1));
        self::assertFalse($session->has("non.existent.key"));
        
        // Verify that retrieval of a subkey works
        $subArray = $session->get("some.random");
        self::assertEquals($array1['some']['random'], $subArray);
        
        // Verify clearing the session works
        $session->clear();
        self::assertEmpty($session->all());
        
        // Verify that setting a key to an empty array empties everything under it.
        $session->set($key1, $value1);
        $session->set("some", []);
        self::assertEquals([], $session->get("some"));
        
        // Make sure it all gets set back as expected when we overwrite and do the original assignment again.
        $session->set($key1, $value1);
        self::assertEquals($value1, $session->get($key1));
        $all = $session->all();
        self::assertEquals($all, $array1);
        
        // Remove a value from the session
        $session->remove($key1);
        self::assertEmpty($session->get($key1));
        
    }
    
    
    
}