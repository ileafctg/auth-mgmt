<?php

namespace IleafCtg\AuthMgmtTest\suites\main;

use Aura\Sql\ExtendedPdoInterface;
use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\iEntity;
use IleafCtg\AuthMgmt\Modules\OrganizationMgmt\Entities\Organization;
use IleafCtg\AuthMgmt\Modules\OrganizationMgmt\Internal\OrganizationManagementServiceRepository;
use IleafCtg\AuthMgmt\Modules\OrganizationMgmt\Internal\OrganizationServiceFilter;
use IleafCtg\AuthMgmt\Modules\OrganizationMgmt\OrganizationService;
use IleafCtg\AuthMgmtTest\suites\main\testing_migrations\phoenix\AuthMgmtInit;
use PHPUnit\Framework\TestCase;

/**
 *
 * Tests related to the OrganizationService.
 *
 */
final class OrganizationServiceTest extends TestCase
{
    
    protected static ExtendedPdoInterface $pdo;
    
    protected static $initialized = false;
    
    
    
    protected function setUp(): void
    {
        // Clear out and initialize our DB schema for our tests (once)
        if (!static::$initialized) {
            static::$pdo = AuthMgmtInit::initialize();
            static::$initialized = true;
        }
        
    }
    
    
    public function test_OrgInstantiation() {
        $org = new Organization();
        $org->name = 'My Org';
        
        $this->assertNotNull($org->name, "Expected name not to be null for org");
    }
    
    public function test_OrgSave() {
        $org = new Organization();
        $org->name = 'My Org';
    
        $orgService = new OrganizationService(new OrganizationManagementServiceRepository(static::$pdo));
    
        /** @var Organization $savedEntity */
        $savedEntity = $orgService->save($org);
        
        $this->assertInstanceOf(iEntity::class, $savedEntity);
        $this->assertEquals($org->name, $savedEntity->name);
        $this->assertEquals($org->deleted, $savedEntity->deleted);
        
        sleep(1); // to test that the mtime is different than ctime
        
        // Now make and edit and make sure it sticks.
        $org = clone($savedEntity);
        $org->name = 'My Updated Org';
        $org->deleted = true;
    
        $savedEntity = $orgService->save($org);
    
        $this->assertInstanceOf(iEntity::class, $savedEntity);
        $this->assertEquals($org->name, $savedEntity->name);
        $this->assertEquals($org->deleted, $savedEntity->deleted);
        $this->assertNotEquals($savedEntity->ctime, $savedEntity->mtime);
    }
    
    public function test_FindOrg() {
        $org = new Organization();
        $org->name = 'My Org';
    
        $orgService = new OrganizationService(new OrganizationManagementServiceRepository(static::$pdo));
    
        /** @var Organization $savedEntity */
        $savedEntity = $orgService->save($org);
    
        $this->assertInstanceOf(iEntity::class, $savedEntity);
        
        $entity = $orgService->findById($savedEntity->id);
        $this->assertInstanceOf(iEntity::class, $entity);
        
    }
    
    public function test_FetchOrgs() {
        $org1 = new Organization();
        $org1->name = 'My Org 1';
        
        $org2 = new Organization();
        $org2->name = 'My Org 2';
    
        $org3 = new Organization();
        $org3->name = 'My Org 3';
    
        $orgService = new OrganizationService(new OrganizationManagementServiceRepository(static::$pdo));
        
        $savedOrg1 = $orgService->save($org1);
        $savedOrg2 = $orgService->save($org2);
        $savedOrg3 = $orgService->save($org3);
        
        $orgFilter = new OrganizationServiceFilter();
        $orgFilter->ids = [$savedOrg1->id, $savedOrg2->id, $savedOrg3->id];
        $orgCollection = $orgService->fetch($orgFilter);
        
        $this->assertEquals($orgCollection->results[$savedOrg1->id]->name, $savedOrg1->name);
        $this->assertEquals($orgCollection->results[$savedOrg2->id]->name, $savedOrg2->name);
        $this->assertEquals($orgCollection->results[$savedOrg3->id]->name, $savedOrg3->name);
        
        // Now test some pagination and limits
        $orgFilter->setLimit(1);
        $orgCollection = $orgService->fetch($orgFilter);
        $this->assertEquals(1, count($orgCollection->results));
        $this->assertEquals(true, $orgCollection->more);
        $this->assertEquals($orgCollection->results[$savedOrg1->id]->name, $savedOrg1->name);
    
        $orgFilter->setPageNumber(2);
        $orgFilter->setLimit(2);
        $orgCollection = $orgService->fetch($orgFilter);
        $this->assertEquals(1, count($orgCollection->results));
        $this->assertEquals(false, $orgCollection->more);
        $this->assertEquals($orgCollection->results[$savedOrg3->id]->name, $savedOrg3->name);
    
        $orgFilter->setPageNumber(3);
        $orgFilter->setLimit(2);
        $orgCollection = $orgService->fetch($orgFilter);
        $this->assertEquals(0, count($orgCollection->results));
        $this->assertEquals(false, $orgCollection->more);
    
    
    }
    
    
    
    
}


