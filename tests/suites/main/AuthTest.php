<?php

namespace IleafCtg\AuthMgmtTest\suites\main;

use Aura\Sql\ExtendedPdoInterface;
use IleafCtg\AuthMgmt\Core\Auth\BaseAuthN;
use IleafCtg\AuthMgmt\Core\Auth\iAuthRole;
use IleafCtg\AuthMgmt\Core\Auth\iSession;
use IleafCtg\AuthMgmt\Core\Auth\PermissionMap;
use IleafCtg\AuthMgmt\Core\AuthZ;
use IleafCtg\AuthMgmt\Core\Exceptions\ApplicationException;
use IleafCtg\AuthMgmt\Modules\ClientMgmt\ClientService;
use IleafCtg\AuthMgmt\Modules\ClientMgmt\Entities\Client;
use IleafCtg\AuthMgmt\Modules\ClientMgmt\Internal\ClientServiceRepository;
use IleafCtg\AuthMgmt\Modules\GroupMgmt\Entities\Group;
use IleafCtg\AuthMgmt\Modules\GroupMgmt\GroupService;
use IleafCtg\AuthMgmt\Modules\GroupMgmt\Internal\GroupServiceRepository;
use IleafCtg\AuthMgmt\Modules\OrganizationMgmt\Entities\Organization;
use IleafCtg\AuthMgmt\Modules\OrganizationMgmt\Internal\OrganizationManagementServiceRepository;
use IleafCtg\AuthMgmt\Modules\OrganizationMgmt\OrganizationService;
use IleafCtg\AuthMgmt\Modules\UserMgmt\Entities\User;
use IleafCtg\AuthMgmt\Modules\UserMgmt\Internal\UserServiceRepository;
use IleafCtg\AuthMgmt\Modules\UserMgmt\UserService;
use IleafCtg\AuthMgmtTest\App;
use IleafCtg\AuthMgmtTest\Helpers\AuthN\TestAuthResourcesAndActions;
use IleafCtg\AuthMgmtTest\suites\main\testing_migrations\phoenix\AuthMgmtInit;
use PHPUnit\Framework\TestCase;
use ReflectionException;

/**
 *
 * Tests related to Authentication/Authorization.
 *
 */
final class AuthTest extends TestCase
{
    
    protected static ExtendedPdoInterface $pdo;
    
    protected static $initialized = false;
    
    protected Organization $org;
    protected Client $client;
    protected Group $group1;
    protected User $user1;
    protected User $user2;
    protected iSession $session;
    
    const THROW_EXCEPTION = 'throw_exception';
    
    const USER1_SSO_CLAIMED_ID = "abcdefg12345678";
    public static $user1Id = 0;
    public static $user2Id = 0;
    
    
    protected function setUp(): void
    {
        self::init();
    
    }
    
    
    
    /**
     * Get the PDO object associated with this test class.
     * @return ExtendedPdoInterface
     */
    public static function getPdo() {
        if (self::$initialized) {
            return self::$pdo;
        }
        throw new ApplicationException("Shouldn't call this method unless " . self::class . " has been initialized");
    }
    
    protected function init(): void {
        // Clear out and initialize our DB schema for our tests (once)
        if (!static::$initialized) {
            static::$pdo = AuthMgmtInit::initialize();
            static::$initialized = true;
        }
    
        $org = new Organization();
        $org->name = 'My Org';
        $this->org = (new OrganizationService(new OrganizationManagementServiceRepository(static::$pdo)))->save($org);
    
        $client = new Client();
        $client->name = 'My Client';
        $client->setOrganizationId($this->org->id);
        $clientService = new ClientService(new ClientServiceRepository(static::$pdo));
        $this->client = $clientService->save($client);
    
        $group = new Group();
        $group->name = 'myGroup';
        $group->setClientId($this->client->id);
        $groupService = new GroupService(new GroupServiceRepository(static::$pdo));
        $this->group1 = $groupService->save($group);
    
        $user1 = new User();
        $user1->firstName = "My User 1";
        $user1->setClientId($this->client->id);
        $userService = new UserService(new UserServiceRepository(static::$pdo));
        $this->user1 = $userService->save($user1);
        static::$user1Id = $this->user1->id;
    
        $user2 = new User();
        $user2->firstName = "My User 2";
        $user2->setClientId($this->client->id);
        $user2->setGroupId($this->group1->id);
        $userService = new UserService(new UserServiceRepository(static::$pdo));
        $this->user2 = $userService->save($user2);
        static::$user2Id = $this->user2->id;
    }
    
    
    
    /**
     * We'll empty the private 'instances' property of the TestAuthN class,
     * so it'll force a new instance to be created on next call of instance(),
     * which will cause it to look for and utilize what's in $_SESSION to repopulate
     * logged in info (if present).
     * @throws ReflectionException
     */
    private function resetAuthContextInstanceToForceSessionLoad() {
        // Make sure that newing up AuthContext loads it from session
        $testAuth = App::instance()->get(TestAuthN::class);
        $authContext = $testAuth;
        $class = new \ReflectionClass($authContext);
        $instanceProperty = $class->getProperty("instances");
        $instanceProperty->setAccessible(true);
        $instanceProperty->setValue(null);
    }
    
    
    
    /**
     * Test that we can successfully login via impersonating a user.
     * @throws ReflectionException
     */
    public function test_UserImpersonation() {
        // fake login our user (impersonate them)
        /** @var TestAuthN $testAuth */
        $testAuth = App::instance()->get(TestAuthN::class);
        $testAuth->impersonateUser($this->user1);
        
        // Details should agree between the user I'm impersonating and the loggedInUser
        static::assertEquals($this->user1->id, $testAuth->currentUser()->id);
        
        // Force session load for our AuthContext class. This simulates going away and loading up the session afresh.
        $this->resetAuthContextInstanceToForceSessionLoad();
        
        // Make sure that we have a logged in user after session reload, and that it's the user we expect.
        static::assertEquals($this->user1->id, $testAuth->currentUser()->id);
    }
    
    
    
    /**
     * Test that we can login as user1, then impersonate as user2, then return back to our login as user1.
     */
    public function testLoginThenImpersonation() {
        /** @var TestAuthN $testAuth */
        $testAuth = App::instance()->get(TestAuthN::class);
        $testAuth->login($this->user1);
        static::assertEquals($this->user1->id, $testAuth->currentUser()->id);
        
        // Now impersonate user2
        $testAuth->impersonateUser($this->user2);
        
        static::assertEquals($this->user2->id, $testAuth->currentUser()->id);
        static::assertEquals($this->user2->id, $testAuth->getSession()->get("auth.authN")->currentUser()->id);
        // Backup user should be user1
        static::assertEquals($this->user1->id, $testAuth->getSession()->get("auth_backup.authN")->currentUser()->id);
    
        // Force session load for our AuthContext class. This simulates going away and loading up the session afresh.
        // We should still be impersonating the other user.
        $this->resetAuthContextInstanceToForceSessionLoad();
        static::assertEquals($this->user2->id, $testAuth->currentUser()->id);
        static::assertEquals($this->user2->id, $testAuth->getSession()->get("auth.authN")->currentUser()->id);
        // Backup user should be user1
        static::assertEquals($this->user1->id, $testAuth->getSession()->get("auth_backup.authN")->currentUser()->id);
    
        // Now end the impersonation, and verify we're back to user1
        $testAuth->endImpersonation();
        static::assertEquals($this->user1->id, $testAuth->currentUser()->id);
        static::assertEquals($this->user1->id, $testAuth->getSession()->get("auth.authN")->currentUser()->id);
        static::assertEmpty($testAuth->getSession()->get("auth_backup"));
    }
    
    
    /**
     * Test that authorization for a given user works after they've been successfully logged in.
     */
    public function test_UserAuthorization() {
        // Log in our user (via impersonation)
        
        /** @var TestAuthN $testAuth */
        $testAuth = App::instance()->get(TestAuthN::class);
        $testAuth->impersonateUser($this->user1);
    
        
        // Make sure their related permissions are observed correctly.
        static::assertFalse($testAuth->getAuthZ()->forClient()->can(AuthZ::PERM_ACTION_ANY, 'users'));
        static::assertTrue($testAuth->getAuthZ()->forUser()->can(AuthZ::PERM_ACTION_ANY, 'timesheets'));
        static::assertFalse($testAuth->getAuthZ()->forOrganization()->can(AuthZ::PERM_ACTION_ANY, 'timesheets'));
    }
    
    
    
    /**
     * Test standard login via SSO ClaimedId lookup
     * @throws \IleafCtg\AuthMgmt\Core\Exceptions\LoginException
     */
    public function test_UserAuthenticationViaSSOClaimLookup() {
        // Bootstrapping AuthN consists of:
        // - Establish the allowed actions and resources
        // - Establish the User and their corresponding permissions (role)
        // - Log them in with SSO lookup
    
        /** @var TestAuthN $authN */
        $authN = App::instance()->get(TestAuthN::class);
        $authN->login($this->user1);
    
        // $authN->currentUser() and TestAuthN::instance()->currentUser() should be the same at this point,
        // and should be $this->user1
        static::assertEquals($this->user1->id, App::instance()->get(TestAuthN::class)->currentUser()->id);
        static::assertEquals($this->user1->id, $authN->currentUser()->id);
        static::assertTrue($authN->isLoggedIn());
        static::assertTrue(App::instance()->get(TestAuthN::class)->isLoggedIn());
        static::assertEquals($this->user1->id, $authN->getSession()->get("auth.authN")->currentUser()->id);
        
        $authN->logout();
        
        static::assertFalse($authN->isLoggedIn());
        static::assertFalse(App::instance()->get(TestAuthN::class)->isLoggedIn());
        static::assertEmpty($authN->getSession()->get("auth"));
        
    }
    
}





class TestAuthN extends BaseAuthN {
    
    /**
     * @inheritDoc
     */
    protected function getPermissionMap(User $user): PermissionMap {
        // We'll use the same role for our two test users
        if ($user->id === AuthTest::$user1Id || $user->id === AuthTest::$user2Id) {
            return (new UserTestRole())->getPermissionMap($user);
        }
        else {
            throw new ApplicationException("TODO - set up Role for other user id {$user->id}");
        }
    }
}

class UserTestRole implements iAuthRole {
    
    // Resource
    //  action
    //      level
    //          permit|deny
    private $permMapArray = [
        'users' => [
            'any' => [
                'client' => 'deny',
                'group' => [
                    5 => 'permit',
                    7 => 'permit',
                ],
            ],
            'create' => [
                'group' => [
                    5 => 'deny'
                ]
            ],
        ],
        'timesheets' => [
            'any' => [
                'self' => 'permit',
            ],
        ],
    ];
    
    public function getPermissionMap($user): PermissionMap {
        $permMap = new PermissionMap(new TestAuthResourcesAndActions(), $user);
        $permMap->hydrateFromArray($this->permMapArray);
        return $permMap;
    }
}

