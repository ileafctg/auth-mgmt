<?php

namespace IleafCtg\AuthMgmtTest\suites\main;


use Aura\Sql\ExtendedPdoInterface;
use Eloquent\Phony\Phpunit\Phony;
use Firebase\JWT\JWT;
use Google_Client;
use Google_Service_Oauth2;
use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\iEntity;
use IleafCtg\AuthMgmt\Core\Exceptions\ApplicationException;
use IleafCtg\AuthMgmt\Modules\ClientMgmt\ClientService;
use IleafCtg\AuthMgmt\Modules\ClientMgmt\Entities\Client;
use IleafCtg\AuthMgmt\Modules\ClientMgmt\Internal\ClientServiceRepository;
use IleafCtg\AuthMgmt\Modules\GroupMgmt\Entities\Group;
use IleafCtg\AuthMgmt\Modules\GroupMgmt\GroupService;
use IleafCtg\AuthMgmt\Modules\GroupMgmt\Internal\GroupServiceRepository;
use IleafCtg\AuthMgmt\Modules\OrganizationMgmt\Entities\Organization;
use IleafCtg\AuthMgmt\Modules\OrganizationMgmt\Internal\OrganizationManagementServiceRepository;
use IleafCtg\AuthMgmt\Modules\OrganizationMgmt\OrganizationService;
use IleafCtg\AuthMgmt\Modules\SsoMgmt\Entities\Sso;
use IleafCtg\AuthMgmt\Modules\SsoMgmt\Internal\SsoServiceRepository;
use IleafCtg\AuthMgmt\Modules\SsoMgmt\SsoProviders;
use IleafCtg\AuthMgmt\Modules\SsoMgmt\SsoService;
use IleafCtg\AuthMgmt\Modules\UserMgmt\Entities\User;
use IleafCtg\AuthMgmt\Modules\UserMgmt\Internal\UserServiceRepository;
use IleafCtg\AuthMgmt\Modules\UserMgmt\UserService;
use IleafCtg\AuthMgmtTest\suites\main\testing_migrations\phoenix\AuthMgmtInit;
use League\OAuth2\Client\Provider\Google as GoogleProvider;
use League\OAuth2\Client\Token\AccessToken;
use PHPUnit\Framework\TestCase;

class SsoServiceTest extends TestCase {
    protected static ExtendedPdoInterface $pdo;
    
    protected static $initialized = false;
    
    protected Organization $org;
    protected Client $client;
    protected Group $group1;
    protected User $user1;
    protected User $user2;
    
    const THROW_EXCEPTION = 'throw_exception';
    
    const USER1_SSO_CLAIMED_ID = "abcdefg12345678";
    public static $user1Id = 0;
    public static $user2Id = 0;
    
    
    protected function setUp(): void {
        self::init();
    
    }
    
    /**
     * Get the PDO object associated with this test class.
     * @return ExtendedPdoInterface
     */
    public static function getPdo() {
        if (self::$initialized) {
            return self::$pdo;
        }
        throw new ApplicationException("Shouldn't call this method unless " . self::class . " has been initialized");
    }
    
    protected function init(): void {
        // Clear out and initialize our DB schema for our tests (once)
        if (!static::$initialized) {
            static::$pdo = AuthMgmtInit::initialize();
            static::$initialized = true;
        }
        
        $org = new Organization();
        $org->name = 'My Org';
        $this->org = (new OrganizationService(new OrganizationManagementServiceRepository(static::$pdo)))->save($org);
        
        $client = new Client();
        $client->name = 'My Client';
        $client->setOrganizationId($this->org->id);
        $clientService = new ClientService(new ClientServiceRepository(static::$pdo));
        $this->client = $clientService->save($client);
        
        $group = new Group();
        $group->name = 'myGroup';
        $group->setClientId($this->client->id);
        $groupService = new GroupService(new GroupServiceRepository(static::$pdo));
        $this->group1 = $groupService->save($group);
        
        $user1 = new User();
        $user1->firstName = "My User 1";
        $user1->setClientId($this->client->id);
        $userService = new UserService(new UserServiceRepository(static::$pdo));
        $this->user1 = $userService->save($user1);
        static::$user1Id = $this->user1->id;
        
        $user2 = new User();
        $user2->firstName = "My User 2";
        $user2->setClientId($this->client->id);
        $user2->setGroupId($this->group1->id);
        $userService = new UserService(new UserServiceRepository(static::$pdo));
        $this->user2 = $userService->save($user2);
        static::$user2Id = $this->user2->id;
        
    }
    
    public function test_SsoInstantiation() {
        $sso = new Sso();
        $sso->name = 'Jon';
        $sso->ssoProvider = SsoProviders::GOOGLE;
        $sso->ssoIdentifier = static::USER1_SSO_CLAIMED_ID;
        $sso->email = 'jonjonz@example.com';
        $sso->familyName = 'Jonz';
        $sso->givenName = 'Jon';
        
        $this->assertNotNull($sso->name);
        $this->assertNotNull($sso->ssoProvider);
        $this->assertNotNull($sso->ssoIdentifier);
        $this->assertNotNull($sso->email);
        $this->assertNotNull($sso->familyName);
        $this->assertNotNull($sso->givenName);
        
    }
    
    public function test_SsoSave() {
        $sso = new Sso();
        $sso->name = 'Jon';
        $sso->ssoProvider = SsoProviders::GOOGLE;
        $sso->ssoIdentifier = static::USER1_SSO_CLAIMED_ID;
        $sso->email = 'jonjonz@example.com';
        $sso->emailVerified = true;
        $sso->familyName = 'Jonz';
        $sso->givenName = 'Jon';
        $sso->locale = 'en-us';
        $sso->picture = 'https://somewhere.com';
        
        $ssoService = new SsoService(new SsoServiceRepository(static::$pdo), new UserService(new UserServiceRepository(static::$pdo)));
        
        $savedEntity = $ssoService->save($sso);
        
        $this->assertInstanceOf(iEntity::class, $savedEntity);
        $this->assertEquals($sso->name, $savedEntity->name);
        $this->assertEquals($sso->ssoProvider, $savedEntity->ssoProvider);
        $this->assertEquals($sso->ssoIdentifier, $savedEntity->ssoIdentifier);
        $this->assertEquals($sso->email, $savedEntity->email);
        $this->assertEquals($sso->familyName, $savedEntity->familyName);
        $this->assertEquals($sso->givenName, $savedEntity->givenName);
        
        $sso = clone($savedEntity);
        $sso->name = 'updated firstname';
        $sso->familyName = 'updated familyname';
        $sso->givenName = 'updated givenName';
        $sso->email = 'updated email';
        $savedEntity2 = $ssoService->save($sso);
        $this->assertEquals($sso->name, $savedEntity2->name);
        $this->assertEquals($sso->ssoProvider, $savedEntity2->ssoProvider);
        $this->assertEquals($sso->ssoIdentifier, $savedEntity2->ssoIdentifier);
        $this->assertEquals($sso->email, $savedEntity2->email);
        $this->assertEquals($sso->familyName, $savedEntity2->familyName);
        $this->assertEquals($sso->givenName, $savedEntity2->givenName);
        $this->assertEquals($savedEntity->id, $savedEntity2->id);
    }
    
    public function test_FindSsoById() {
        $sso = new Sso();
        $sso->name = 'Jon';
        $sso->ssoProvider = SsoProviders::GOOGLE;
        $sso->ssoIdentifier = static::USER1_SSO_CLAIMED_ID;
        $sso->email = 'jonjonz@example.com';
        $sso->familyName = 'Jonz';
        $sso->givenName = 'Jon';
    
        $ssoService = new SsoService(new SsoServiceRepository(static::$pdo), new UserService(new UserServiceRepository(static::$pdo)));
    
        $savedEntity = $ssoService->save($sso);
        
        $entity = $ssoService->findById($savedEntity->id);
        $this->assertInstanceOf(iEntity::class, $entity);
        $this->assertEquals($savedEntity->id, $entity->id);
    }
    
    public function test_FindSsoBySsoIdentifier() {
        $sso = new Sso();
        $sso->name = 'Jon';
        $sso->ssoProvider = SsoProviders::GOOGLE;
        $sso->ssoIdentifier = static::USER1_SSO_CLAIMED_ID;
        $sso->email = 'jonjonz@example.com';
        $sso->familyName = 'Jonz';
        $sso->givenName = 'Jon';
    
        $ssoService = new SsoService(new SsoServiceRepository(static::$pdo), new UserService(new UserServiceRepository(static::$pdo)));
    
        /** @var Sso $savedEntity */
        $savedEntity = $ssoService->save($sso);
    
        $entity = $ssoService->findBySsoIdentifier($savedEntity->ssoProvider, $savedEntity->ssoIdentifier);
        $this->assertInstanceOf(iEntity::class, $entity);
        $this->assertEquals($savedEntity->id, $entity->id);
    }
    
    public function test_SsoSaveFailWithoutRequiredFields() {
        $sso = new Sso();
        $sso->name = 'Jon';
        //$sso->ssoProvider = SsoProviders::GOOGLE;
        $sso->ssoIdentifier = static::USER1_SSO_CLAIMED_ID;
        $sso->email = 'jonjonz@example.com';
        $sso->familyName = 'Jonz';
        $sso->givenName = 'Jon';
    
        $ssoService = new SsoService(new SsoServiceRepository(static::$pdo), new UserService(new UserServiceRepository(static::$pdo)));
    
        /** @var Sso $savedEntity */
        $this->expectException(\TypeError::class);
        $savedEntity = $ssoService->save($sso);
    }
    
    public function test_linkUserToSso() {
        $sso = new Sso();
        $sso->name = 'Jon';
        $sso->ssoProvider = SsoProviders::GOOGLE;
        $sso->ssoIdentifier = static::USER1_SSO_CLAIMED_ID;
        $sso->email = 'jonjonz@example.com';
        $sso->familyName = 'Jonz';
        $sso->givenName = 'Jon';
    
        $ssoService = new SsoService(new SsoServiceRepository(static::$pdo), new UserService(new UserServiceRepository(static::$pdo)));
    
        /** @var Sso $savedEntity */
        $savedEntity = $ssoService->save($sso);
        
        $result = $ssoService->linkSsoToUser($savedEntity, $this->user1);
        $this->assertTrue($result);
    }
    
    
    public function test_lookupUsersViaSso() {
        $sso = new Sso();
        $sso->name = 'Jon';
        $sso->ssoProvider = SsoProviders::GOOGLE;
        $sso->ssoIdentifier = static::USER1_SSO_CLAIMED_ID;
        $sso->email = 'jonjonz@example.com';
        $sso->familyName = 'Jonz';
        $sso->givenName = 'Jon';
    
        $ssoService = new SsoService(new SsoServiceRepository(static::$pdo), new UserService(new UserServiceRepository(static::$pdo)));
    
        /** @var Sso $savedEntity */
        $savedEntity = $ssoService->save($sso);
    
        $result = $ssoService->linkSsoToUser($savedEntity, $this->user1);
        $this->assertTrue($result);
        
        $linkedUsers = $ssoService->fetchUsersViaSso($sso);
        
        $this->assertGreaterThanOrEqual(1, $linkedUsers);
        $foundExpectedUserInResults = false;
        foreach($linkedUsers as $linkedUser) {
            if ($linkedUser == $this->user1) {
                $foundExpectedUserInResults = true;
            }
        }
        $this->assertTrue($foundExpectedUserInResults);
        
    }
    
    private function mockGoogleUserInfoApiResponse() {
        $data = [
            "email" => "jon.jonz@example.com",
            "familyName" => "Jonz",
            "gender" => null,
            "givenName" => "Jon",
            "hd" => null,
            "id" => static::USER1_SSO_CLAIMED_ID,
            "link" => null,
            "locale" => "en",
            "name" => "Jon Jonz",
            "picture" => "https:\/\/lh3.googleusercontent.com\/a\/AEdF***G1d=s96-c",
            "verifiedEmail" => true
        ];
        //return json_encode($data, JSON_PRETTY_PRINT);
        return $data;
    }
    
    private function mockAuthCodeExchangeForTokenResponse(): array {
        $id_token_decoded = [
            "iss" => "https:\/\/accounts.google.com",
            "azp" => "8827098***.apps.googleusercontent.com",
            "aud" => "8827098***.apps.googleusercontent.com",
            "sub" => static::USER1_SSO_CLAIMED_ID,
            "email" => "jon.jonz@example.com",
            "email_verified" => true,
            "at_hash" => "yf***Ef5nWdPA",
            "name" => "Jon Jonz",
            "picture" => "https:\/\/lh3.googleusercontent.com\/a\/****bVIj_XvG1d=s96-c",
            "given_name" => "Jon",
            "family_name" => "Jonz",
            "locale" => "en",
            "iat" => time(),
            "exp" => time() + 300,
        ];
        
        // Encode as a JWT
        
        $secret = base64_decode(strtr('whatever', '-_', '+/'));
        
        $id_token = JWT::encode($id_token_decoded, $secret, 'HS256');
        
        //$id_token = base64_encode(json_encode(str_replace('/', '_', str_replace('+','-',implode('.', $id_token_decoded)))));
        
        return [
            'access_token' => 'mock_access_token',
            'expires_in' => 3600,
            'refresh_token' => 'mock_refresh_token',
            'scope' => 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile openid',
            'token_type' => "Bearer",
            'id_token' => $id_token,
            'created' => time(),
        ];
        
        /*
         * 
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## Response that comes when user consents to scope during OAuth process - the authorization_code
        https://accounts.google.com/o/oauth2/v2/auth
        ?response_type=code
        &access_type=offline
        &client_id=882709***.apps.googleusercontent.com
        &redirect_uri=http%3A%2F%2F127.0.0.1%3A8888%2Fauth%2Fsui
        &state
        &scope=email%20profile%20openid
        &approval_prompt=auto
        
        HTTP/1.1
        Host: developers.google.com
        
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## Exchange authorization_code for a token
        POST /token HTTP/1.1
        Host: oauth2.googleapis.com
        Content-length: 261
        content-type: application/x-www-form-urlencoded
        user-agent: google-oauth-playground
        
        code=4%2F0AWgavdc***Fvb9jXmUQ
        &redirect_uri=http%3A%2F%2F127.0.0.1%3A8888%2Fauth%2Fsui
        &client_id=4********92.apps.googleusercontent.com
        &client_secret=************
        &scope=openid email profile
        &grant_type=authorization_code
        
        
        
        #### Response:
        HTTP/1.1 200 OK
        Content-type: application/json; charset=utf-8
        {
          "access_token": "ya29.a0AX9GBdXtNofpF", 
          "id_token": "eyJhbGciOiJSkrhm7m2HrkxL_d3Q90YF0gNkse04EI7h9Q",  ## decoding this gives you everything that can be obtained from a userinfo API request
          "expires_in": 3599, 
          "token_type": "Bearer", 
          "scope": "https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile openid", 
          "refresh_token": "Epy6BnQ_J31-jtK",
          "created": 1672182422
        }
        
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## Decoded JWT from id_token
        {
            "iss": "https:\/\/accounts.google.com",
            "azp": "8827098***.apps.googleusercontent.com",
            "aud": "8827098***.apps.googleusercontent.com",
            "sub": "1107***5",
            "email": "jon.jonz@example.com",
            "email_verified": true,
            "at_hash": "yf***Ef5nWdPA",
            "name": "Jon Jonz",
            "picture": "https:\/\/lh3.googleusercontent.com\/a\/****bVIj_XvG1d=s96-c",
            "given_name": "Jon",
            "family_name": "Jonz",
            "locale": "en",
            "iat": 1672182423,
            "exp": 1672186023
        }
        
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## Get person info
        GET /oauth2/v2/userinfo HTTP/1.1
        Host: www.googleapis.com
        Content-length: 0
        Authorization: Bearer ya29.a0AX9GBdXtNofpF
        
        #### Response
        HTTP/1.1 200 OK
        Content-type: application/json; charset=UTF-8
        
        {
            "email": "jon.jonz@example.com",
            "familyName": "Jonz",
            "gender": null,
            "givenName": "Jon",
            "hd": null,
            "id": "1107***5",
            "link": null,
            "locale": "en",
            "name": "Jon Jonz",
            "picture": "https:\/\/lh3.googleusercontent.com\/a\/AEdF***G1d=s96-c",
            "verifiedEmail": true
        }
        
        
         */
        
        
    }
    
    public function test_userinfoQueryAfterOauth() {
        // Since we're decoding the JWT from the token, there's no need for us to obtain user info via the userinfo endpoint.
        // But leaving this here as reference, if needed.
        $googleAppCredentialsJson = file_get_contents(__DIR__ . '/config/googleAppCredentials.json');
        $googleAppCredentials = json_decode($googleAppCredentialsJson, true);
        $client = new Google_Client();
        $client->setClientId($googleAppCredentials['web']['client_id']);
        $client->setClientSecret($googleAppCredentials['web']['client_secret']);
        $client->setRedirectUri($googleAppCredentials['web']['redirect_uris'][0]);
        $client->addScope('email');
        $client->addScope('profile');
        $client->addScope('openid');
        $client->setAccessType('offline');
    
        //$token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
        $token = $this->mockAuthCodeExchangeForTokenResponse();
        $client->setAccessToken($token['access_token']);
    
        // get profile info 
        //$google_oauth = new Google_Service_Oauth2($client);
        $userInfoApiResponse = $this->mockGoogleUserInfoApiResponse();
        $provider = Phony::partialMock(Google_Service_Oauth2::class);
        // FIXME - userinfo doesn't exist error occurs.  Need to figure out how to mock when the 'userinfo' is dynamically invoked by class.
        //$provider->userinfo->get->returns($userInfoApiResponse);
        //
        ///** @var Google_Service_Oauth2 $google_oauth */
        //$google_oauth = $provider->get();
        //$google_account_info = $google_oauth->userinfo->get();
        //
        //$this->assertNotEmpty($google_account_info->getId());
    }
    
    public function test_setUpNewUserViaExistingSsoSignUp() {
        
        // Normally, during 3-legged OAuth, the user will have visited Google's authorization url, signed in, and 
        // consented.  This would result in them getting redirected back to your app's auth page, and the request
        // would contain an authorization_code (in $_GET['code']).  This code can, in turn, be exchanged for
        // an access token.  We'll shortcut all of that and just mock an access token.  We trust that all of the other
        // pieces of the flow described above are tested sufficiently in the package maintainer's code that we're using.
        $authCodeTokenExchangeResponse = $this->mockAuthCodeExchangeForTokenResponse();
        
        $jwt = json_decode(base64_decode(str_replace('_', '/', str_replace('-','+',explode('.', $authCodeTokenExchangeResponse['id_token'])[1]))));
        
        $sso = new Sso();
        $sso->name = $jwt->name;
        $sso->givenName = $jwt->given_name;
        $sso->familyName = $jwt->family_name;
        $sso->locale = $jwt->locale;
        $sso->email = $jwt->email;
        $sso->emailVerified = $jwt->email_verified;
        $sso->picture = $jwt->picture;
        $sso->ssoProvider = SsoProviders::GOOGLE;
        $sso->ssoIdentifier = $jwt->sub;
    
        $ssoService = new SsoService(new SsoServiceRepository(static::$pdo), new UserService(new UserServiceRepository(static::$pdo)));
        
        // Save the sso, then sleep 1 sec, to simulate an existing sso entity before linking
        $ssoService->save($sso);
        sleep(1);
    
        /** @var Sso $savedEntity */
        $savedUser = $ssoService->setUpNewUserViaSSOSignUp($sso, $this->client);
        $this->assertNotNull($savedUser->id);
        $this->assertEquals($savedUser->firstName, $sso->givenName);
        $this->assertEquals($savedUser->lastName, $sso->familyName);
        
        // Find the linked SSO of the user
        $linkedSsos = $ssoService->fetchSsoEntitiesForUser($savedUser);
        $found = false;
        foreach ($linkedSsos as $linkedSso) {
            if ($linkedSso->ssoIdentifier == $sso->ssoIdentifier && $linkedSso->ssoProvider == $sso->ssoProvider) {
                $found = true;
                break;
            }
        }
        $this->assertTrue($found);
        
        $linkedUser = $ssoService->fetchUsersViaSso($sso, $this->client)[0];
        $this->assertEquals($linkedUser->id, $savedUser->id);
        
        // Due to it being an existing SSO, the mtime and ctime should no longer match
        $ssoRefreshed = $ssoService->findBySsoIdentifier($sso->ssoProvider, $sso->ssoIdentifier);
        $this->assertNotEquals($ssoRefreshed->ctime, $ssoRefreshed->mtime);
        
    }
    
    public function test_setUpNewUserViaNewSsoSignUp() {
        // reset database
        static::$initialized = false;
        self::init();
        
        // Normally, during 3-legged OAuth, the user will have visited Google's authorization url, signed in, and 
        // consented.  This would result in them getting redirected back to your app's auth page, and the request
        // would contain an authorization_code (in $_GET['code']).  This code can, in turn, be exchanged for
        // an access token.  We'll shortcut all of that and just mock an access token.  We trust that all of the other
        // pieces of the flow described above are tested sufficiently in the package maintainer's code that we're using.
        $authCodeTokenExchangeResponse = $this->mockAuthCodeExchangeForTokenResponse();
    
        $jwt = json_decode(base64_decode(str_replace('_', '/', str_replace('-','+',explode('.', $authCodeTokenExchangeResponse['id_token'])[1]))));
    
        $sso = new Sso();
        $sso->name = $jwt->name;
        $sso->givenName = $jwt->given_name;
        $sso->familyName = $jwt->family_name;
        $sso->locale = $jwt->locale;
        $sso->email = $jwt->email;
        $sso->emailVerified = $jwt->email_verified;
        $sso->picture = $jwt->picture;
        $sso->ssoProvider = SsoProviders::GOOGLE;
        $sso->ssoIdentifier = $jwt->sub;
    
        $ssoService = new SsoService(new SsoServiceRepository(static::$pdo), new UserService(new UserServiceRepository(static::$pdo)));
    
        /** @var Sso $savedEntity */
        $savedUser = $ssoService->setUpNewUserViaSSOSignUp($sso, $this->client);
        $this->assertNotNull($savedUser->id);
        $this->assertEquals($savedUser->firstName, $sso->givenName);
        $this->assertEquals($savedUser->lastName, $sso->familyName);
    
        // Find the linked SSO of the user
        $linkedSsos = $ssoService->fetchSsoEntitiesForUser($savedUser);
        $found = false;
        foreach ($linkedSsos as $linkedSso) {
            if ($linkedSso->ssoIdentifier == $sso->ssoIdentifier && $linkedSso->ssoProvider == $sso->ssoProvider) {
                $found = true;
                break;
            }
        }
        $this->assertTrue($found);
    
        $linkedUser = $ssoService->fetchUsersViaSso($sso, $this->client)[0];
        $this->assertEquals($linkedUser->id, $savedUser->id);
    
        // Due to it being a new SSO, the mtime and ctime should match
        $ssoRefreshed = $ssoService->findBySsoIdentifier($sso->ssoProvider, $sso->ssoIdentifier);
        $this->assertEquals($ssoRefreshed->ctime, $ssoRefreshed->mtime);
    
    }
    
    
}