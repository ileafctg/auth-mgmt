<?php

namespace IleafCtg\AuthMgmtTest\suites\main;

use Aura\Sql\ExtendedPdoInterface;
use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\iEntity;
use IleafCtg\AuthMgmt\Core\Exceptions\ApplicationException;
use IleafCtg\AuthMgmt\Modules\ClientMgmt\ClientService;
use IleafCtg\AuthMgmt\Modules\ClientMgmt\Entities\Client;
use IleafCtg\AuthMgmt\Modules\ClientMgmt\Internal\ClientServiceRepository;
use IleafCtg\AuthMgmt\Modules\GroupMgmt\Entities\Group;
use IleafCtg\AuthMgmt\Modules\GroupMgmt\GroupService;
use IleafCtg\AuthMgmt\Modules\GroupMgmt\Internal\GroupServiceFilter;
use IleafCtg\AuthMgmt\Modules\GroupMgmt\Internal\GroupServiceRepository;
use IleafCtg\AuthMgmt\Modules\OrganizationMgmt\Entities\Organization;
use IleafCtg\AuthMgmt\Modules\OrganizationMgmt\Internal\OrganizationManagementServiceRepository;
use IleafCtg\AuthMgmt\Modules\OrganizationMgmt\OrganizationService;
use IleafCtg\AuthMgmtTest\suites\main\testing_migrations\phoenix\AuthMgmtInit;
use PHPUnit\Framework\TestCase;

/**
 *
 * Tests related to the OrganizationService.
 *
 */
final class GroupServiceTest extends TestCase
{
    
    protected static ExtendedPdoInterface $pdo;
    
    protected static $initialized = false;
    
    protected Organization $org;
    protected Client        $client;
    
    
    
    protected function setUp(): void
    {
        // Clear out and initialize our DB schema for our tests (once)
        if (!static::$initialized) {
            static::$pdo = AuthMgmtInit::initialize();
            static::$initialized = true;
        }
        
        $org = new Organization();
        $org->name = 'My Org';
        $this->org = (new OrganizationService(new OrganizationManagementServiceRepository(static::$pdo)))->save($org);
        
        $client = new Client();
        $client->name = 'My Client';
        $client->setOrganizationId($this->org->id);
        $clientService = new ClientService(new ClientServiceRepository(static::$pdo));
        $this->client = $clientService->save($client);
        
    }
    
    
    public function test_GroupInstantiation() {
        $group = new Group();
        $group->name = 'My Group';
        $group->setClientId($this->client->id);
        
        $this->assertNotNull($group->name, "Expected name not to be null for group");
        $this->assertEquals($this->client->id, $group->clientId);
    }
    
    public function test_GroupSaveFailWithoutClientId() {
        $group = new Group();
        // By omitting the clientId, the save should fail.
        //$group->setClientId($this->client->id);
        $group->name = "My Group";
    
        $groupService = new GroupService(new GroupServiceRepository(static::$pdo));
    
        $this->expectException(ApplicationException::class);
        $groupService->save($group);
        
    }
    
    public function test_GroupSave() {
        
        
        $group = new Group();
        $group->setClientId($this->client->id);
        $group->name = "My Group";
    
        $groupService = new GroupService(new GroupServiceRepository(static::$pdo));
        $savedEntity = $groupService->save($group);
    
        $this->assertInstanceOf(iEntity::class, $savedEntity);
        $this->assertEquals($group->name, $savedEntity->name);
        $this->assertEquals($group->deleted, $savedEntity->deleted);
        $this->assertEquals($group->clientId, $savedEntity->clientId);
    
        sleep(1); // to test that the mtime is different than ctime
    
        $group = clone($savedEntity);
        $group->name = "My updated group";
        $group->deleted = true;
        $savedEntity = $groupService->save($group);
    
        $this->assertInstanceOf(iEntity::class, $savedEntity);
        $this->assertEquals($group->name, $savedEntity->name);
        $this->assertEquals($group->deleted, $savedEntity->deleted);
        $this->assertEquals($group->clientId, $savedEntity->clientId);
        $this->assertNotEquals($savedEntity->ctime, $savedEntity->mtime);
        
    }
    
    public function test_FindGroupById() {
        $group = new Group();
        $group->name = 'My Group';
        $group->setClientId($this->client->id);
    
        $groupService = new GroupService(new GroupServiceRepository(static::$pdo));
        $savedEntity = $groupService->save($group);
    
        $this->assertInstanceOf(iEntity::class, $savedEntity);
    
        $entity = $groupService->findById($savedEntity->id);
        $this->assertInstanceOf(iEntity::class, $entity);
        
    }
    
    public function test_FetchGroups() {
        $group1 = new Group();
        $group1->name = "My Group 1";
        $group1->setClientId($this->org->id);
    
        $group2 = new Group();
        $group2->name = "My Group 2";
        $group2->setClientId($this->org->id);
    
        $group3 = new Group();
        $group3->name = "My Group 3";
        $group3->setClientId($this->org->id);
    
        $groupService = new GroupService(new GroupServiceRepository(static::$pdo));
        $savedGroup1 = $groupService->save($group1);
        $savedGroup2 = $groupService->save($group2);
        $savedGroup3 = $groupService->save($group3);
    
        $groupFilter = new GroupServiceFilter();
        $groupFilter->ids = [$savedGroup1->id, $savedGroup2->id, $savedGroup3->id];
        $groupCollection = $groupService->fetch($groupFilter);
    
    
    
        $this->assertEquals($groupCollection->results[$savedGroup1->id]->name, $savedGroup1->name);
        $this->assertEquals($groupCollection->results[$savedGroup2->id]->name, $savedGroup2->name);
        $this->assertEquals($groupCollection->results[$savedGroup3->id]->name, $savedGroup3->name);
    
        // Now test some pagination and limits
        $groupFilter->setLimit(1);
        $groupCollection = $groupService->fetch($groupFilter);
        $this->assertEquals(1, count($groupCollection->results));
        $this->assertEquals(true, $groupCollection->more);
        $this->assertEquals($groupCollection->results[$savedGroup1->id]->name, $savedGroup1->name);
    
        $groupFilter->setPageNumber(2);
        $groupFilter->setLimit(2);
        $groupCollection = $groupService->fetch($groupFilter);
        $this->assertEquals(1, count($groupCollection->results));
        $this->assertEquals(false, $groupCollection->more);
        $this->assertEquals($groupCollection->results[$savedGroup3->id]->name, $savedGroup3->name);
    
        $groupFilter->setPageNumber(3);
        $groupFilter->setLimit(2);
        $groupCollection = $groupService->fetch($groupFilter);
        $this->assertEquals(0, count($groupCollection->results));
        $this->assertEquals(false, $groupCollection->more);
    }
    
}


