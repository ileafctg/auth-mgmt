<?php

namespace IleafCtg\AuthMgmtTest\suites\main;

use Aura\Sql\ExtendedPdoInterface;
use IleafCtg\AuthMgmt\Core\Auth\PermissionMap;
use IleafCtg\AuthMgmt\Core\AuthZ;
use IleafCtg\AuthMgmt\Core\Exceptions\ApplicationException;
use IleafCtg\AuthMgmt\Modules\ClientMgmt\ClientService;
use IleafCtg\AuthMgmt\Modules\ClientMgmt\Entities\Client;
use IleafCtg\AuthMgmt\Modules\ClientMgmt\Internal\ClientServiceRepository;
use IleafCtg\AuthMgmt\Modules\GroupMgmt\Entities\Group;
use IleafCtg\AuthMgmt\Modules\GroupMgmt\GroupService;
use IleafCtg\AuthMgmt\Modules\GroupMgmt\Internal\GroupServiceRepository;
use IleafCtg\AuthMgmt\Modules\OrganizationMgmt\Entities\Organization;
use IleafCtg\AuthMgmt\Modules\OrganizationMgmt\Internal\OrganizationManagementServiceRepository;
use IleafCtg\AuthMgmt\Modules\OrganizationMgmt\OrganizationService;
use IleafCtg\AuthMgmt\Modules\UserMgmt\Entities\User;
use IleafCtg\AuthMgmt\Modules\UserMgmt\Internal\UserServiceRepository;
use IleafCtg\AuthMgmt\Modules\UserMgmt\UserService;
use IleafCtg\AuthMgmtTest\Helpers\AuthN\TestAuthResourcesAndActions;
use IleafCtg\AuthMgmtTest\suites\main\testing_migrations\phoenix\AuthMgmtInit;
use PHPUnit\Framework\TestCase;

/**
 *
 * Tests related to AuthZ.
 *
 */
final class AuthZTest extends TestCase
{
    
    protected static ExtendedPdoInterface $pdo;
    
    protected static $initialized = false;
    
    protected Organization $org;
    protected Client $client;
    protected Group $group1;
    protected User $user1;
    protected User $user2;
    protected User $userLoggedIn;
    
    const THROW_EXCEPTION = 'throw_exception';
    
    
    protected function setUp(): void
    {
        self::init();
    
    }
    
    protected function init(): void {
        // Clear out and initialize our DB schema for our tests (once)
        if (!static::$initialized) {
            static::$pdo = AuthMgmtInit::initialize();
            static::$initialized = true;
        }
    
        $org = new Organization();
        $org->name = 'My Org';
        $this->org = (new OrganizationService(new OrganizationManagementServiceRepository(static::$pdo)))->save($org);
    
        $client = new Client();
        $client->name = 'My Client';
        $client->setOrganizationId($this->org->id);
        $clientService = new ClientService(new ClientServiceRepository(static::$pdo));
        $this->client = $clientService->save($client);
    
        $group = new Group();
        $group->name = 'myGroup';
        $group->setClientId($this->client->id);
        $groupService = new GroupService(new GroupServiceRepository(static::$pdo));
        $this->group1 = $groupService->save($group);
    
        $user1 = new User();
        $user1->firstName = "My User 1";
        $user1->setClientId($this->client->id);
        $userService = new UserService(new UserServiceRepository(static::$pdo));
        $this->user1 = $userService->save($user1);
    
        $user2 = new User();
        $user2->firstName = "My User 2";
        $user2->setClientId($this->client->id);
        $user2->setGroupId($this->group1->id);
        $this->user2 = $userService->save($user2);
        
        $userLoggedIn = new User();
        $userLoggedIn->firstName = "Me";
        $userLoggedIn->setClientId($this->client->id);
        $userLoggedIn->setGroupId(0); // No Group
        $this->userLoggedIn = $userService->save($userLoggedIn);
    }
    
    
    
    /**
     * Test the scenarios involving can() on behalf of another user
     *
     * @param AuthZ $authZ
     * @param array $input
     * @param       $expectedResult
     *
     * @dataProvider forUser_DataProvider
     * @throws ApplicationException
     */
    public function test_permCheckOtherUser(AuthZ $authZ, array $input, $expectedResult) {
        $this->scenarioTester($authZ, $input, $expectedResult);
    }
    /**
     * dataProvider for test_permCheckOtherUser()
     * @return array[]
     */
    public function forUser_DataProvider() {
        // dataProviders run before setup(), so we'll call init() here in order to reference group and user ids in our result set
        self::init();
        
        $validResourcesAndActions = new TestAuthResourcesAndActions([
            ## Resources
            'widgets' => [
                'actions' => [
                    TestAuthResourcesAndActions::PERM_ACTION_MANAGE,
                ],
                // Since 'levels' is empty, assumption is that defined actions are allowed at any level.
                'levels' => []
            ],
        ]);
    
        $permissionMap = new PermissionMap($validResourcesAndActions, $this->userLoggedIn, [
            'widgets' => [
                'manage' => [
                    'organization' => 'deny',
                    'client'       => 'deny',
                    'group'        => [
                        $this->group1->id => 'permit',
                    ],
                    'self'         => 'permit',
                ]
            ],
        ]);
    
        $authZ = new AuthZ($permissionMap);
    
        return [
            // deny - org/any/widgets
            [ $authZ, ['level' => AuthZ::PERM_LEVEL_ORGANIZATION, 'action' => TestAuthResourcesAndActions::PERM_ACTION_ANY, 'resource' => 'widgets'], false ],
            // deny - org/manage/widgets
            [ $authZ, ['level' => AuthZ::PERM_LEVEL_ORGANIZATION, 'action' => TestAuthResourcesAndActions::PERM_ACTION_MANAGE, 'resource' => 'widgets'], false ],
            // deny - client/manage/widgets
            [ $authZ, ['level' => AuthZ::PERM_LEVEL_CLIENT, 'action' => TestAuthResourcesAndActions::PERM_ACTION_MANAGE, 'resource' => 'widgets'], false ],
            // permit - self/manage/widgets
            [ $authZ, ['level' => AuthZ::PERM_LEVEL_SELF, 'action' => TestAuthResourcesAndActions::PERM_ACTION_MANAGE, 'resource' => 'widgets'], true ],
            // permit - group/manage/widgets for group1->id
            [ $authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_MANAGE, 'resource' => 'widgets', 'group_id' => $this->group1->id], true ],
            // permit - self/manage/widgets for user2->id ; Should work because we have groupId 1 permit, and user2 is a member of groupId 1.
            [ $authZ, ['level' => AuthZ::PERM_LEVEL_SELF, 'action' => TestAuthResourcesAndActions::PERM_ACTION_MANAGE, 'resource' => 'widgets', 'user_id' => $this->user2->id], true ],
            // deny - self/manage/widgets for user1->id ; Should not work because we have groupId 1 permit, but not groupId 0, and user1 is a member of groupId 0.
            [ $authZ, ['level' => AuthZ::PERM_LEVEL_SELF, 'action' => TestAuthResourcesAndActions::PERM_ACTION_MANAGE, 'resource' => 'widgets', 'user_id' => $this->user1->id], false ],
            // permit - self/manage/widgets for self via specifying forUser(<ourUserId>); Should not work because we stipulate deny for self
            [ $authZ, ['level' => AuthZ::PERM_LEVEL_SELF, 'action' => TestAuthResourcesAndActions::PERM_ACTION_MANAGE, 'resource' => 'widgets', 'user_id' => $this->userLoggedIn->id], true ],
        ];
    }
    
    
    
    /**
     * Earlier test to check all of our possible combinations. Not necessarily complete, but keeping test because more tests are good.
     * @dataProvider baseFunctionality_DataProvider
     */
    public function test_baseFunctionality(AuthZ $authZ, array $input, $expectedResult) {
        $this->scenarioTester($authZ, $input, $expectedResult);
    }
    /**
     * dataProvider for test_baseFunctionality()
     * 
     * @return array[]
     */
    public function baseFunctionality_DataProvider() {
        // dataProviders run before setup(), so we'll call init() here in order to reference group and user ids in our result set
        self::init();
        
        /*
         * We start with an AuthResourcesAndActions object, wherein we define each resource and the valid levels and actions that may be performed against the resource.
         * 
         * Then, we have a permissionMap object that takes in the AuthResourcesAndActions object, and defines the permissions a given user may have against a set of resources, where we define whether a given action/resource/level combo is permitted or denied for the user.  
         * 
         * Then we create an AuthZ object based on the PermissionMap object.
         * 
         * Then we can call the can(), etc. methods on the AuthZ object.  The can() method will first ensure that we have a valid action/resource/level combo according to the AuthResourcesAndActions object.  Then, it compares the action/resource/level against the user's permissionMap.
         * 
         * Higher level permissions cascade down to lower levels.  However, any explicit settings at a lower level override what may have been set at a higher level.
         * 
         * When evaluating permission, we look at the target level and any level *above* it when evaluating the permission (but not *below* it).  For example, if they specified ->forClient(), we will check the 'client' and 'organization' levels, but no deeper.  The intent is to return the permission ** at the level specified ** regardless of whether it may be a different outcome at a lower level.
         * 
         * If both the 'any' action and some more specific action (e.g. 'edit') are defined at the same level of a user's permissionMap, the more specific action (e.g. 'edit' rather than 'any') will take precedence.
         * 
         */
    
        $validResourcesAndActions = new TestAuthResourcesAndActions([
            ## Resources
            'users' => [
                'actions' => [
                    TestAuthResourcesAndActions::PERM_ACTION_MANAGE,
                    TestAuthResourcesAndActions::PERM_ACTION_CREATE,
                    // NOTE: 'any' (AuthZ::PERM_ACTION_ANY) is a wildcard action which will return true regardless of which other defined action is specified,
                    // if the user has the 'any' action permitted in their permissionMap.
                ],
                'levels' => [
                    // If empty, we'll assume the defined actions can be performed for this resource
                    // at every level.  Otherwise, honor what's defined here.
                    AuthZ::PERM_LEVEL_ORGANIZATION,
                    AuthZ::PERM_LEVEL_CLIENT,
                    AuthZ::PERM_LEVEL_GROUP,
                ]
            ],
            'widgets' => [
                'actions' => [
                    TestAuthResourcesAndActions::PERM_ACTION_CREATE,
                    TestAuthResourcesAndActions::PERM_ACTION_READ,
                    TestAuthResourcesAndActions::PERM_ACTION_UPDATE,
                    TestAuthResourcesAndActions::PERM_ACTION_DELETE,
                ],
                // Since 'levels' is empty, assumption is that defined actions are allowed at any level.
                'levels' => []
            ],
        ]);
    
        $permissionMap = new PermissionMap($validResourcesAndActions, $this->userLoggedIn, [
            'users'   => [
                'any'    => [
                    'organization' => 'deny',
                    'client'       => 'permit',
                    'group'        => [
                        5 => 'permit',
                        7 => 'permit',
                    ],
                ],
                'manage' => [
                    'group' => [
                        5 => 'deny',
                        7 => 'permit',
                    ]
                ],
            ],
            'widgets' => [
                'any'    => [
                    'organization' => 'permit',
                ],
                'delete' => [
                    'client' => 'deny',
                    'self'   => 'permit',
                ]
            ],
        ]);
    
        $authZ = new AuthZ($permissionMap);
        
        return [
            // deny - org/manage/users
            [ $authZ, ['level' => AuthZ::PERM_LEVEL_ORGANIZATION, 'action' => TestAuthResourcesAndActions::PERM_ACTION_MANAGE, 'resource' => 'users'], false ],
            // permit - client/manage/users
            [ $authZ, ['level' => AuthZ::PERM_LEVEL_CLIENT, 'action' => TestAuthResourcesAndActions::PERM_ACTION_MANAGE, 'resource' => 'users'], true ],
            [ $authZ, ['level' => null /* defaults to client */, 'action' => TestAuthResourcesAndActions::PERM_ACTION_MANAGE, 'resource' => 'users'], true ],
            // permit - group:7/manage/users
            [ $authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_MANAGE, 'resource' => 'users', 'group_id' => 7], true ],
            // deny - group:5/manage/users
            [ $authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_MANAGE, 'resource' => 'users', 'group_id' => 5], false ],
            // permit - group:5/create/users
            [ $authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_CREATE, 'resource' => 'users', 'group_id' => 5], true ],
            // permit - group:5/any/users
            [ $authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_ANY, 'resource' => 'users', 'group_id' => 5], true ],
            // Invalid - undefined level
            [ $authZ, ['level' => AuthZ::PERM_LEVEL_SELF, 'action' => TestAuthResourcesAndActions::PERM_ACTION_MANAGE, 'resource' => 'users'], self::THROW_EXCEPTION ],
            // Invalid - undefined action
            [ $authZ, ['level' => AuthZ::PERM_LEVEL_CLIENT, 'action' => 'someAction', 'resource' => 'users'], self::THROW_EXCEPTION ],
            // Invalid - undefined resource
            [ $authZ, ['level' => AuthZ::PERM_LEVEL_CLIENT, 'action' => TestAuthResourcesAndActions::PERM_ACTION_MANAGE, 'resource' => 'whatever'], self::THROW_EXCEPTION ],
            
            
            // permit - org/read/widgets
            [ $authZ, ['level' => AuthZ::PERM_LEVEL_ORGANIZATION, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => 'widgets'], true ],
            // permit - client/read/widgets
            [ $authZ, ['level' => AuthZ::PERM_LEVEL_CLIENT, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => 'widgets'], true ],
            // permit - group:*/read/widgets
            [ $authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => 'widgets', 'group_id' => 1], true ],
            // group_id shouldn't matter in this case, we have org-wide perms
            [ $authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => 'widgets', 'group_id' => 99], true ],
            // permit - self/read/widgets
            [ $authZ, ['level' => AuthZ::PERM_LEVEL_SELF, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => 'widgets'], true ],
            // permit - null(client)/read/widgets
            [ $authZ, ['level' => null, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => 'widgets'], true ],
            // permit - null(client)/any/widgets
            [ $authZ, ['level' => null, 'action' => TestAuthResourcesAndActions::PERM_ACTION_ANY, 'resource' => 'widgets'], true ],
            // Invalid - undefined action
            [ $authZ, ['level' => null, 'action' => 'someaction', 'resource' => 'widgets'], self::THROW_EXCEPTION ],
            
            // permit - org/delete/widgets ; Permitted because org-permit is set for 'any'
            [ $authZ, ['level' => AuthZ::PERM_LEVEL_ORGANIZATION, 'action' => TestAuthResourcesAndActions::PERM_ACTION_DELETE, 'resource' => 'widgets'], true ],
            // deny - client/delete/widgets ; Denied b/c client-deny is set for 'delete'
            [ $authZ, ['level' => AuthZ::PERM_LEVEL_CLIENT, 'action' => TestAuthResourcesAndActions::PERM_ACTION_DELETE, 'resource' => 'widgets'], false ],
            // deny - group/delete/widgets ; Denied b/c client-deny is set for 'delete', and nothing explicit is set for group, so we inherit from level above (client)
            [ $authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_DELETE, 'resource' => 'widgets', 'group_id' => 1], false ],
            // permit - self/delete/widgets ; Permitted b/c self-permit is set for 'delete'
            [ $authZ, ['level' => AuthZ::PERM_LEVEL_SELF, 'action' => TestAuthResourcesAndActions::PERM_ACTION_DELETE, 'resource' => 'widgets'], true ],
              
        ];
    }
    
    
    
    /**
     * Test for invalid level, resource, or action       
     * @dataProvider invalidActionResource_DataProvider
     */
    public function test_invalidActionResource(AuthZ $authZ, array $input, $expectedResult) {
        $this->scenarioTester($authZ, $input, $expectedResult);
    }
    public function invalidActionResource_DataProvider() {
        // dataProviders run before setup(), so we'll call init() here in order to reference group and user ids in our result set
        self::init();
        
        $validResourcesAndActions = new TestAuthResourcesAndActions([
            ## Resources
            'widgets' => [
                'actions' => [
                    TestAuthResourcesAndActions::PERM_ACTION_MANAGE,
                    TestAuthResourcesAndActions::PERM_ACTION_CREATE,
                    TestAuthResourcesAndActions::PERM_ACTION_UPDATE,
                    TestAuthResourcesAndActions::PERM_ACTION_DELETE,
                    TestAuthResourcesAndActions::PERM_ACTION_READ,
                ],
                'levels'  => [
                    // If empty, we'll assume the defined actions can be performed for this resource
                    // at every level.  Otherwise, honor what's defined here.
                ]
            ]
        ]);
    
        $permissionMap = new PermissionMap($validResourcesAndActions, $this->userLoggedIn, [
            'widgets' => [
                'any' => [
                    'organization' => 'permit',
                ]
            ],
        ]);
    
        $authZ = new AuthZ($permissionMap);
    
        // fail if invalid resource specified
        // fail if invalid action specified
    
        return [
            [$authZ, ['level' => AuthZ::PERM_LEVEL_ORGANIZATION, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => 'invalidWidget'], self::THROW_EXCEPTION],
            [$authZ, ['level' => AuthZ::PERM_LEVEL_ORGANIZATION, 'action' => 'invalidAction', 'resource' => 'widgets'], self::THROW_EXCEPTION],
        ];
    }
    
    
    
    /**
     * Test that specifying an invalid level results in an exception
     */
    public function test_invalidLevel() {
        
        // fail if invalid level specified
        //static::expectException(ApplicationException::class);
        try {
            $validResourcesAndActions = new TestAuthResourcesAndActions([
                ## Resources
                'widgets' => [
                    'actions' => [
                        TestAuthResourcesAndActions::PERM_ACTION_MANAGE,
                        TestAuthResourcesAndActions::PERM_ACTION_CREATE,
                        TestAuthResourcesAndActions::PERM_ACTION_UPDATE,
                        TestAuthResourcesAndActions::PERM_ACTION_DELETE,
                        TestAuthResourcesAndActions::PERM_ACTION_READ,
                    ],
                    'levels'  => [
                        // Specifying an invalid level here will cause an exception to be thrown
                        'invalidLevel',
                    ]
                ]
            ]);
        }
        catch (ApplicationException $e) {
            static::assertTrue(true);
            return;
        }
        static::fail("Failed to throw ApplicationException as expected");
    }
    
    
    
    /**
     * Test permit or deny at various levels based on various versions of a permission map.
     */
    public function test_permitOrDenyAtVariousLevels() {
        $validResourcesAndActions = new TestAuthResourcesAndActions([
            ## Resources
            'widgets' => [
                'actions' => [
                    TestAuthResourcesAndActions::PERM_ACTION_READ,
                ],
                'levels' => [
                    // If empty, we'll assume the defined actions can be performed for this resource
                    // at every level.  Otherwise, honor what's defined here.
                ]
            ]
        ]);
    
        $resource = 'widgets';
        
        // Permit at org level, should cascade down to all
        $permissionMapPermit = new PermissionMap($validResourcesAndActions, $this->userLoggedIn, [
            $resource => [
                'any' => [
                    'organization' => 'permit',
                ]
            ],
        ]);
    
        $authZ = new AuthZ($permissionMapPermit);
    
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_ORGANIZATION, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource], true);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_CLIENT, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource], true);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource, 'group_id' => 0], true);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_SELF, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource], true);
    
    
        // Denied at org levevl, should cascade down to all
        $permissionMapDeny = new PermissionMap($validResourcesAndActions, $this->userLoggedIn, [
            $resource => [
                'any' => [
                    'organization' => 'deny',
                ]
            ],
        ]);
    
        $authZ = new AuthZ($permissionMapDeny);
        
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_ORGANIZATION, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource], false);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_CLIENT, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource], false);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource, 'group_id' => 0], false);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_SELF, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource], false);
    
    
        // Mixed at different levels - honored at each level.
        $permissionMapMixed = new PermissionMap($validResourcesAndActions, $this->userLoggedIn, [
            $resource => [
                'any' => [
                    'organization' => 'deny',
                    'client'       => 'permit',
                    'group'        => [
                        0 => 'deny',
                        3 => 'permit',
                    ],
                    'self'         => 'deny'
                ]
            ],
        ]);
    
        $authZ = new AuthZ($permissionMapMixed);
    
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_ORGANIZATION, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource], false);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_CLIENT, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource], true);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource, 'group_id' => 0], false);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource, 'group_id' => 3], true);
        // For groupId 5, we don't explicitly have it called out in our permission map, so the higher level client perm applies.
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource, 'group_id' => 5], true);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_SELF, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource], false);
    
    
        // Denied at org, omitting client and group, permit at self
        $permissionMapMixed2 = new PermissionMap($validResourcesAndActions, $this->userLoggedIn, [
            $resource => [
                'any' => [
                    'organization' => 'deny',
                    'self'         => 'permit'
                ]
            ],
        ]);
    
        $authZ = new AuthZ($permissionMapMixed2);
        
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_ORGANIZATION, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource], false);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_CLIENT, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource], false);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource, 'group_id' => 0], false);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource, 'group_id' => 3], false);
        // For groupId 5, we don't explicitly have it called out in our permission map, so the higher level org perm applies.
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource, 'group_id' => 5], false);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_SELF, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource], true);
    
    
        // Permitted at org, omitting client and group, deny at self
        $permissionMapMixed3 = new PermissionMap($validResourcesAndActions, $this->userLoggedIn, [
            $resource => [
                'any' => [
                    'organization' => 'permit',
                    'self'         => 'deny'
                ]
            ],
        ]);
    
        $authZ = new AuthZ($permissionMapMixed3);
    
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_ORGANIZATION, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource], true);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_CLIENT, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource], true);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource, 'group_id' => 0], true);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource, 'group_id' => 3], true);
        // For groupId 5, we don't explicitly have it called out in our permission map, so the higher level org perm applies.
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource, 'group_id' => 5], true);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_SELF, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource], false);
    
    
    
        // Omitting at org and client (should default to deny), and permitted at group and self
        $permissionMapMixed4 = new PermissionMap($validResourcesAndActions, $this->userLoggedIn, [
            $resource => [
                'any' => [
                    'group' => [
                        5 => 'permit',
                    ],
                    'self'  => 'permit'
                ]
            ],
        ]);
    
        $authZ = new AuthZ($permissionMapMixed4);
    
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_ORGANIZATION, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource], false);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_CLIENT, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource], false);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource, 'group_id' => 0], false);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource, 'group_id' => 3], false);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource, 'group_id' => 5], true);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_SELF, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource], true);
    }
    
    /**
     * Test permit or deny at various levels based on various versions of a permission map with multiple actions.
     */
    public function test_ActionsAtVariousLevelsAndPermitDeny() {
        $validResourcesAndActions = new TestAuthResourcesAndActions([
            ## Resources
            'widgets' => [
                'actions' => [
                    TestAuthResourcesAndActions::PERM_ACTION_MANAGE,
                    TestAuthResourcesAndActions::PERM_ACTION_CREATE,
                    TestAuthResourcesAndActions::PERM_ACTION_UPDATE,
                    TestAuthResourcesAndActions::PERM_ACTION_DELETE,
                    TestAuthResourcesAndActions::PERM_ACTION_READ,
                ],
                'levels'  => [
                    // If empty, we'll assume the defined actions can be performed for this resource
                    // at every level.  Otherwise, honor what's defined here.
                ]
            ]
        ]);
    
        $resource = 'widgets';
    
        // Permit at org level, should cascade down to all, except where explicitly overridden, and those should then cascade down.
        $permissionMap = new PermissionMap($validResourcesAndActions, $this->userLoggedIn, [
            $resource => [
                'any'                                           => [
                    'organization' => 'permit',
                ],
                TestAuthResourcesAndActions::PERM_ACTION_MANAGE => ['client' => 'permit'],
                TestAuthResourcesAndActions::PERM_ACTION_DELETE => ['client' => 'deny'],
            ],
        ]);
    
        $authZ = new AuthZ($permissionMap);
    
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_ORGANIZATION, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource], true);
        
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_CLIENT, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource], true);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_CLIENT, 'action' => TestAuthResourcesAndActions::PERM_ACTION_MANAGE, 'resource' => $resource], true);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_CLIENT, 'action' => TestAuthResourcesAndActions::PERM_ACTION_DELETE, 'resource' => $resource], false);
    
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource, 'group_id' => 0], true);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_MANAGE, 'resource' => $resource, 'group_id' => 0], true);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_DELETE, 'resource' => $resource, 'group_id' => 0], false);
    
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_SELF, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource], true);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_SELF, 'action' => TestAuthResourcesAndActions::PERM_ACTION_MANAGE, 'resource' => $resource], true);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_SELF, 'action' => TestAuthResourcesAndActions::PERM_ACTION_DELETE, 'resource' => $resource], false);
    
    
    
        // Any omitted perms should default to deny
        $permissionMap2 = new PermissionMap($validResourcesAndActions, $this->userLoggedIn, [
            $resource => [
                TestAuthResourcesAndActions::PERM_ACTION_MANAGE => ['client' => 'permit'],
                TestAuthResourcesAndActions::PERM_ACTION_DELETE => ['client' => 'deny'],
            ],
        ]);
    
        $authZ = new AuthZ($permissionMap2);
    
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_ORGANIZATION, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource], false);
    
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_CLIENT, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource], false);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_CLIENT, 'action' => TestAuthResourcesAndActions::PERM_ACTION_MANAGE, 'resource' => $resource], true);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_CLIENT, 'action' => TestAuthResourcesAndActions::PERM_ACTION_DELETE, 'resource' => $resource], false);
    
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource, 'group_id' => 0], false);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_MANAGE, 'resource' => $resource, 'group_id' => 0], true);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_GROUP, 'action' => TestAuthResourcesAndActions::PERM_ACTION_DELETE, 'resource' => $resource, 'group_id' => 0], false);
    
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_SELF, 'action' => TestAuthResourcesAndActions::PERM_ACTION_READ, 'resource' => $resource], false);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_SELF, 'action' => TestAuthResourcesAndActions::PERM_ACTION_MANAGE, 'resource' => $resource], true);
        $this->scenarioTester($authZ, ['level' => AuthZ::PERM_LEVEL_SELF, 'action' => TestAuthResourcesAndActions::PERM_ACTION_DELETE, 'resource' => $resource], false);
    
    }
    
    
    
    /**
     * Ensures we have the right level set based on inputs for out AuthZ object.
     * 
     * @param AuthZ    $authZ
     * @param string|null   $level
     * @param int|null $groupId
     * @param int|null $userId
     *
     * @throws ApplicationException
     */
    private function helperAuthLevelSetter(AuthZ $authZ, string $level = null, int $groupId = null, int $userId = null) {
        if ($level === AuthZ::PERM_LEVEL_CLIENT) {
            $authZ->forClient();
        }
        else if ($level === AuthZ::PERM_LEVEL_GROUP) {
            $authZ->forGroup($groupId);
        }
        else if ($level === AuthZ::PERM_LEVEL_ORGANIZATION) {
            $authZ->forOrganization();
        }
        else if ($level === AuthZ::PERM_LEVEL_SELF) {
            $user = null;
            if (isset($userId) && is_int($userId)) {
                $userService = new UserService(new UserServiceRepository(static::$pdo));
                $user = $userService->findById($userId);
            }
            $authZ->forUser($user);
        }
    }
    
    /**
     * Helper method to run through scenarios for auth check testing.
     * 
     * @param AuthZ $authZ
     * @param array $input
     * @param       $expectedResult
     *
     * @throws ApplicationException
     */
    private function scenarioTester(AuthZ $authZ, array $input, $expectedResult, $method = 'can') {
        isset($input['level']) ?: $input['level'] = null;
        isset($input['group_id']) ?: $input['group_id'] = null;
        isset($input['user_id']) ?: $input['user_id'] = null;
        $this->helperAuthLevelSetter($authZ, $input['level'], $input['group_id'], $input['user_id']);
        
        if ($expectedResult === self::THROW_EXCEPTION) {
            static::expectException(ApplicationException::class);
            $authZ->$method($input['action'], $input['resource']);
        }
        else {
            static::assertEquals($expectedResult, $authZ->$method($input['action'], $input['resource']), "Didn't get expected result of {$expectedResult} for level:{$input['level']}, action:{$input['action']}, resource:{$input['resource']}");
    
                
            $this->helperAuthLevelSetter($authZ, $input['level'], $input['group_id'], $input['user_id']);
            $method = 'cannot';
            $expectedResult = !$expectedResult;
            static::assertEquals($expectedResult, $authZ->$method($input['action'], $input['resource']), "Didn't get expected result of {$expectedResult} for level:{$input['level']}, action:{$input['action']}, resource:{$input['resource']}");
    
            
            $this->helperAuthLevelSetter($authZ, $input['level'], $input['group_id'], $input['user_id']);
            // Test the canAny method
            $method = 'canAny';
            // flip expected result back to what was originally passed in.
            $expectedResult = !$expectedResult;
            // Turn our action into an array of 1 action
            $actions = [$input['action']];
            static::assertEquals($expectedResult, $authZ->$method($actions, $input['resource']), "Didn't get expected result of {$expectedResult} for level:{$input['level']}, action:{$input['action']}, resource:{$input['resource']}");
    
            
            $this->helperAuthLevelSetter($authZ, $input['level'], $input['group_id'], $input['user_id']);
            // Test the cannotAny method
            $method = 'cannotAny';
            // flip expected result to opposite
            $expectedResult = !$expectedResult;
            // Turn our action into an array of 1 action
            $actions = [$input['action']];
            static::assertEquals($expectedResult, $authZ->$method($actions, $input['resource']), "Didn't get expected result of {$expectedResult} for level:{$input['level']}, action:{$input['action']}, resource:{$input['resource']}");
            
        }
        
    }
    
}



