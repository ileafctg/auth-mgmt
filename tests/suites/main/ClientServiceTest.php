<?php

namespace IleafCtg\AuthMgmtTest\suites\main;

use Aura\Sql\ExtendedPdoInterface;
use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\iEntity;
use IleafCtg\AuthMgmt\Core\Exceptions\ApplicationException;
use IleafCtg\AuthMgmt\Modules\ClientMgmt\ClientService;
use IleafCtg\AuthMgmt\Modules\ClientMgmt\Entities\Client;
use IleafCtg\AuthMgmt\Modules\ClientMgmt\Internal\ClientServiceFilter;
use IleafCtg\AuthMgmt\Modules\ClientMgmt\Internal\ClientServiceRepository;
use IleafCtg\AuthMgmt\Modules\OrganizationMgmt\Entities\Organization;
use IleafCtg\AuthMgmt\Modules\OrganizationMgmt\Internal\OrganizationManagementServiceRepository;
use IleafCtg\AuthMgmt\Modules\OrganizationMgmt\Internal\OrganizationServiceFilter;
use IleafCtg\AuthMgmt\Modules\OrganizationMgmt\OrganizationService;
use IleafCtg\AuthMgmtTest\suites\main\testing_migrations\phoenix\AuthMgmtInit;
use PHPUnit\Framework\TestCase;

/**
 *
 * Tests related to the OrganizationService.
 *
 */
final class ClientServiceTest extends TestCase
{
    
    protected static ExtendedPdoInterface $pdo;
    
    protected static $initialized = false;
    
    protected Organization $org;
    
    
    
    protected function setUp(): void
    {
        // Clear out and initialize our DB schema for our tests (once)
        if (!static::$initialized) {
            static::$pdo = AuthMgmtInit::initialize();
            static::$initialized = true;
        }
    
        $org = new Organization();
        $org->name = 'My Org';
        $this->org = (new OrganizationService(new OrganizationManagementServiceRepository(static::$pdo)))->save($org);
    
    
    }
    
    
    public function test_ClientInstantiation() {
        $client = new Client();
        $client->name = 'My Client';
        $client->setOrganizationId($this->org->id);
        
        $this->assertNotNull($client->name, "Expected name not to be null for client");
        $this->assertEquals($this->org->id, $client->organizationId);
    }
    
    
    
    /**
     * Test that omitting the organizationId for a new client causes the save() to fail.
     */
    public function test_ClientSaveFailWithoutOrgId() {
        $client = new Client();
        // By omitting the orgId, the save should fail.
        //$client->setOrganizationId($this->org->id);
        $client->name = "My Client";
    
        $clientService = new ClientService(new ClientServiceRepository(static::$pdo));
    
        $this->expectException(ApplicationException::class);
        $clientService->save($client);
    
    }
    
    
    public function test_ClientSave() {
        $client = new Client();
        $client->setOrganizationId($this->org->id);
        $client->name = "My Client";
        
        $clientService = new ClientService(new ClientServiceRepository(static::$pdo));
        $savedEntity = $clientService->save($client);
    
        $this->assertInstanceOf(iEntity::class, $savedEntity);
        $this->assertEquals($client->name, $savedEntity->name);
        $this->assertEquals($client->deleted, $savedEntity->deleted);
        $this->assertEquals($client->organizationId, $savedEntity->organizationId);
        
        sleep(1); // to test that the mtime is different than ctime
        
        $client = clone($savedEntity);
        $client->name = "My updated client";
        $client->deleted = true;
        $savedEntity = $clientService->save($client);
        
        $this->assertInstanceOf(iEntity::class, $savedEntity);
        $this->assertEquals($client->name, $savedEntity->name);
        $this->assertEquals($client->deleted, $savedEntity->deleted);
        $this->assertEquals($client->organizationId, $savedEntity->organizationId);
        $this->assertNotEquals($savedEntity->ctime, $savedEntity->mtime);
    }
    
    public function test_FindClient() {
        $client = new Client();
        $client->name = 'My Client';
        $client->setOrganizationId($this->org->id);
    
        $clientService = new ClientService(new ClientServiceRepository(static::$pdo));
        $savedEntity = $clientService->save($client);
        
        $this->assertInstanceOf(iEntity::class, $savedEntity);
        
        $entity = $clientService->findById($savedEntity->id);
        $this->assertInstanceOf(iEntity::class, $entity);
                
    }
    
    public function test_FetchClients() {
        $client1 = new Client();
        $client1->name = "My Client 1";
        $client1->setOrganizationId($this->org->id);
        
        $client2 = new Client();
        $client2->name = "My Client 2";
        $client2->setOrganizationId($this->org->id);
        
        $client3 = new Client();
        $client3->name = "My Client 3";
        $client3->setOrganizationId($this->org->id);
    
        $clientService = new ClientService(new ClientServiceRepository(static::$pdo));
        $savedClient1 = $clientService->save($client1);
        $savedClient2 = $clientService->save($client2);
        $savedClient3 = $clientService->save($client3);
       
        $clientFilter = new ClientServiceFilter();
        $clientFilter->ids = [$savedClient1->id, $savedClient2->id, $savedClient3->id];
        $clientCollection = $clientService->fetch($clientFilter);
        
        
        
        $this->assertEquals($clientCollection->results[$savedClient1->id]->name, $savedClient1->name);
        $this->assertEquals($clientCollection->results[$savedClient2->id]->name, $savedClient2->name);
        $this->assertEquals($clientCollection->results[$savedClient3->id]->name, $savedClient3->name);
        
        // Now test some pagination and limits
        $clientFilter->setLimit(1);
        $clientCollection = $clientService->fetch($clientFilter);
        $this->assertEquals(1, count($clientCollection->results));
        $this->assertEquals(true, $clientCollection->more);
        $this->assertEquals($clientCollection->results[$savedClient1->id]->name, $savedClient1->name);
        
        $clientFilter->setPageNumber(2);
        $clientFilter->setLimit(2);
        $clientCollection = $clientService->fetch($clientFilter);
        $this->assertEquals(1, count($clientCollection->results));
        $this->assertEquals(false, $clientCollection->more);
        $this->assertEquals($clientCollection->results[$savedClient3->id]->name, $savedClient3->name);
        
        $clientFilter->setPageNumber(3);
        $clientFilter->setLimit(2);
        $clientCollection = $clientService->fetch($clientFilter);
        $this->assertEquals(0, count($clientCollection->results));
        $this->assertEquals(false, $clientCollection->more);
        
        
    }
    
    
    
    
}


