<?php

namespace IleafCtg\AuthMgmtTest\suites\main;

use Aura\Sql\ExtendedPdoInterface;
use DateTime;
use IleafCtg\AuthMgmt\Core\BaseEntitySupplemental\iEntity;
use IleafCtg\AuthMgmt\Core\Exceptions\ApplicationException;
use IleafCtg\AuthMgmt\Modules\ClientMgmt\ClientService;
use IleafCtg\AuthMgmt\Modules\ClientMgmt\Entities\Client;
use IleafCtg\AuthMgmt\Modules\ClientMgmt\Internal\ClientServiceRepository;
use IleafCtg\AuthMgmt\Modules\GroupMgmt\Entities\Group;
use IleafCtg\AuthMgmt\Modules\GroupMgmt\GroupService;
use IleafCtg\AuthMgmt\Modules\GroupMgmt\Internal\GroupServiceRepository;
use IleafCtg\AuthMgmt\Modules\OrganizationMgmt\Entities\Organization;
use IleafCtg\AuthMgmt\Modules\OrganizationMgmt\Internal\OrganizationManagementServiceRepository;
use IleafCtg\AuthMgmt\Modules\OrganizationMgmt\OrganizationService;
use IleafCtg\AuthMgmt\Modules\UserMgmt\Entities\User;
use IleafCtg\AuthMgmt\Modules\UserMgmt\Internal\UserServiceFilter;
use IleafCtg\AuthMgmt\Modules\UserMgmt\Internal\UserServiceRepository;
use IleafCtg\AuthMgmt\Modules\UserMgmt\UserService;
use IleafCtg\AuthMgmtTest\suites\main\testing_migrations\phoenix\AuthMgmtInit;
use PHPUnit\Framework\TestCase;

/**
 *
 * Tests related to the OrganizationService.
 *
 */
final class UserServiceTest extends TestCase
{
    
    protected static ExtendedPdoInterface $pdo;
    
    protected static $initialized = false;
    
    protected Organization $org;
    protected Client $client;
    
    
    
    protected function setUp(): void
    {
        // Clear out and initialize our DB schema for our tests (once)
        if (!static::$initialized) {
            static::$pdo = AuthMgmtInit::initialize();
            static::$initialized = true;
        }
    
        $org = new Organization();
        $org->name = 'My Org';
        $this->org = (new OrganizationService(new OrganizationManagementServiceRepository(static::$pdo)))->save($org);
    
        $client = new Client();
        $client->name = 'My Client';
        $client->setOrganizationId($this->org->id);
        $clientService = new ClientService(new ClientServiceRepository(static::$pdo));
        $this->client = $clientService->save($client);
    
    }
    
    
    public function test_UserInstantiation() {
        $user = new User();
        $user->firstName = 'Jon';
        $user->lastName = 'Jonz';
        $user->setClientId($this->client->id);
        $atime = new DateTime();
        $user->setAtime($atime);
        
        $this->assertNotNull($user->firstName, "Expected first name not to be null for user");
        $this->assertNotNull($user->firstName, "Expected last name not to be null for user");
        $this->assertEquals($this->client->id, $user->clientId);
        $this->assertEquals($user->atime->getTimestamp(), $atime->getTimestamp());
    }
    
    
    
    public function test_UserSaveFailWithoutClientId() {
        $user = new User();
        $user->firstName = 'Jon';
        $user->lastName = 'Jonz';
        //$user->setClientId($this->client->id);
        $atime = new DateTime();
        $user->setAtime($atime);
        
        $userService = new UserService(new UserServiceRepository(static::$pdo));
    
        $this->expectException(ApplicationException::class);
        $userService->save($user);
    }
    
    public function test_UserSave() {
        $user = new User();
        $user->firstName = 'Jon';
        $user->lastName = 'Jonz';
        $user->setClientId($this->client->id);
        $user->setGroupId(0);
        $atime = new DateTime();
        $user->setAtime($atime);
    
        $userService = new UserService(new UserServiceRepository(static::$pdo));
    
        $savedEntity = $userService->save($user);
    
        $this->assertInstanceOf(iEntity::class, $savedEntity);
        $this->assertEquals($user->firstName, $savedEntity->firstName);
        $this->assertEquals($user->deleted, $savedEntity->deleted);
        $this->assertEquals($user->clientId, $savedEntity->clientId);
    
        sleep(1); // to test that the mtime is different than ctime
    
        $user = clone($savedEntity);
        $user->firstName = "My updated firstName";
        $user->lastName = "My updated lastName";
        $user->deleted = true;
        $user->setAtime();
        $savedEntity2 = $userService->save($user);
    
        $this->assertInstanceOf(iEntity::class, $savedEntity2);
        $this->assertEquals($user->firstName, $savedEntity2->firstName);
        $this->assertEquals($user->lastName, $savedEntity2->lastName);
        $this->assertEquals($user->deleted, $savedEntity2->deleted);
        $this->assertEquals($user->clientId, $savedEntity2->clientId);
        $this->assertNotEquals($savedEntity2->ctime, $savedEntity2->mtime);
        $this->assertNotEquals($savedEntity2->atime, $savedEntity->atime);
    }
    
    public function test_AssignGroupToUser() {
        $group = new Group();
        $group->setClientId($this->client->id);
        $group->name = "My Group";
    
        $groupService = new GroupService(new GroupServiceRepository(static::$pdo));
        $savedGroup = $groupService->save($group);
        
        $user = new User();
        $user->firstName = 'Jon';
        $user->lastName = 'Jonz';
        $user->setClientId($this->client->id);
        $user->setGroupId($savedGroup->id);
        $atime = new DateTime();
        $user->setAtime($atime);
    
        $userService = new UserService(new UserServiceRepository(static::$pdo));
    
        $savedUser = $userService->save($user);
    
        $this->assertInstanceOf(iEntity::class, $savedUser);
        $this->assertEquals($user->firstName, $savedUser->firstName);
        $this->assertEquals($user->lastName, $savedUser->lastName);
        $this->assertEquals($user->deleted, $savedUser->deleted);
        $this->assertEquals($user->clientId, $savedUser->clientId);
        $this->assertEquals($user->groupId, $savedUser->groupId);
        
        $savedUser->setGroupId(0);
        $savedUser2 = $userService->save($savedUser);
    
        $this->assertInstanceOf(iEntity::class, $savedUser2);
        $this->assertEquals($savedUser->firstName, $savedUser2->firstName);
        $this->assertEquals($savedUser->lastName, $savedUser2->lastName);
        $this->assertEquals($savedUser->deleted, $savedUser2->deleted);
        $this->assertEquals($savedUser->clientId, $savedUser2->clientId);
        $this->assertEquals($savedUser->groupId, $savedUser2->groupId);
        
        
    }
    
    public function test_FindUserById() {
        $user = new User();
        $user->firstName = 'Jon';
        $user->lastName = 'Jonz';
        $user->setClientId($this->client->id);
        $user->setGroupId(0);
        $atime = new DateTime();
        $user->setAtime($atime);
    
        $userService = new UserService(new UserServiceRepository(static::$pdo));
    
        $savedEntity = $userService->save($user);
    
        $entity = $userService->findById($savedEntity->id);
        $this->assertInstanceOf(iEntity::class, $entity);
    }
    
    public function test_FetchUsers() {
        $user1 = new User();
        $user1->firstName = "My User 1";
        $user1->setClientId($this->client->id);
    
        $user2 = new User();
        $user2->firstName = "My User 2";
        $user2->setClientId($this->client->id);
    
        $user3 = new User();
        $user3->firstName = "My User 3";
        $user3->setClientId($this->client->id);
    
        $userService = new UserService(new UserServiceRepository(static::$pdo));
        $savedUser1 = $userService->save($user1);
        $savedUser2 = $userService->save($user2);
        $savedUser3 = $userService->save($user3);
    
        $userFilter = new UserServiceFilter();
        $userFilter->ids = [$savedUser1->id, $savedUser2->id, $savedUser3->id];
        $userCollection = $userService->fetch($userFilter);
    
    
    
        $this->assertEquals($userCollection->results[$savedUser1->id]->firstName, $savedUser1->firstName);
        $this->assertEquals($userCollection->results[$savedUser2->id]->firstName, $savedUser2->firstName);
        $this->assertEquals($userCollection->results[$savedUser3->id]->firstName, $savedUser3->firstName);
    
        // Now test some pagination and limits
        $userFilter->setLimit(1);
        $userCollection = $userService->fetch($userFilter);
        $this->assertEquals(1, count($userCollection->results));
        $this->assertEquals(true, $userCollection->more);
        $this->assertEquals($userCollection->results[$savedUser1->id]->firstName, $savedUser1->firstName);
    
        $userFilter->setPageNumber(2);
        $userFilter->setLimit(2);
        $userCollection = $userService->fetch($userFilter);
        $this->assertEquals(1, count($userCollection->results));
        $this->assertEquals(false, $userCollection->more);
        $this->assertEquals($userCollection->results[$savedUser3->id]->firstName, $savedUser3->firstName);
    
        $userFilter->setPageNumber(3);
        $userFilter->setLimit(2);
        $userCollection = $userService->fetch($userFilter);
        $this->assertEquals(0, count($userCollection->results));
        $this->assertEquals(false, $userCollection->more);
    }
    
}


