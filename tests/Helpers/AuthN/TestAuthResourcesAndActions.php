<?php

namespace IleafCtg\AuthMgmtTest\Helpers\AuthN;

use IleafCtg\AuthMgmt\Core\Auth\BaseAuthResourcesAndActions;
use IleafCtg\AuthMgmt\Core\AuthZ;

/**
 * For use in our testing
 * The Auth resources and actions for testing.
 * We provide a set of default actions/resources here - but they can be overridden in the construction.
 */
class TestAuthResourcesAndActions extends BaseAuthResourcesAndActions {
    
    // Our default Resources and actions for our TestAuth
    protected $defaultResourcesAndActions = [
        ## Resources
        'users' => [
            'actions' => [
                self::PERM_ACTION_MANAGE,
                self::PERM_ACTION_CREATE,
                self::PERM_ACTION_READ,
                self::PERM_ACTION_UPDATE,
                self::PERM_ACTION_DELETE,
                
                // NOTE: 'any' (AuthZ::PERM_ACTION_ANY) is a wildcard action which will return true regardless of which other defined action is specified,
                // if the user has the 'any' action permitted.
            ],
            'levels' => [
                // If empty, we'll assume you can perform actions for this resource
                // at every level.  Otherwise honor what's defined here.
                AuthZ::PERM_LEVEL_CLIENT,
                AuthZ::PERM_LEVEL_GROUP,
            ]
        ],
        'timesheets' => [
            'actions' => [
                self::PERM_ACTION_MANAGE,
                self::PERM_ACTION_CREATE,
                self::PERM_ACTION_READ,
                self::PERM_ACTION_UPDATE,
                self::PERM_ACTION_DELETE,
            ],
            'levels' => [
                // Empty means you can perform at any level.
            ]
        ]
    ];
    
}