<?php

namespace IleafCtg\AuthMgmtTest\Helpers\Adapter;

interface CleanupInterface
{
    public function cleanupDatabase();
}
