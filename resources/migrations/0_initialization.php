<?php

use Phoenix\Migration\AbstractMigration;

class Initialization extends AbstractMigration
{
    protected function up(): void
    {
        if (!$this->tableExists('mt_clients')) {
            $this->table('mt_clients', 'id')
                ->setCharset('utf8mb4')
                ->setCollation('utf8mb4_general_ci')
                ->addColumn('id', 'integer', ['autoincrement' => true, 'signed' => false])
                ->addColumn('organization_id', 'integer', ['signed' => false])
                ->addColumn('name', 'string', ['default' => '', 'length' => 250])
                ->addColumn('deleted', 'boolean', ['default' => false])
                ->addColumn('ctime', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
                ->addColumn('mtime', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
                ->create();
        }

        if (!$this->tableExists('mt_fields')) {
            $this->table('mt_fields', 'id')
                ->setCharset('utf8mb4')
                ->setCollation('utf8mb4_general_ci')
                ->addColumn('id', 'integer', ['autoincrement' => true, 'signed' => false])
                ->addColumn('name', 'string', ['default' => '', 'length' => 50])
                ->addColumn('applies_to', 'enum', ['default' => 'users', 'length' => 0, 'values' => ['users']])
                ->addColumn('regex_rule', 'string', ['default' => '\'\''])
                ->create();
        }

        if (!$this->tableExists('mt_field_values')) {
            $this->table('mt_field_values', 'id')
                ->setCharset('utf8mb4')
                ->setCollation('utf8mb4_general_ci')
                ->addColumn('id', 'integer', ['autoincrement' => true, 'signed' => false])
                ->addColumn('field_id', 'integer')
                ->addColumn('value', 'string', ['default' => '', 'length' => 50])
                ->create();
        }

        if (!$this->tableExists('mt_field_values_users')) {
            $this->table('mt_field_values_users', 'id')
                ->setCharset('utf8mb4')
                ->setCollation('utf8mb4_general_ci')
                ->addColumn('id', 'integer', ['autoincrement' => true, 'signed' => false])
                ->addColumn('field_value_id', 'integer')
                ->addColumn('user_id', 'integer')
                ->create();
        }

        if (!$this->tableExists('mt_groups')) {
            $this->table('mt_groups', 'id')
                ->setCharset('utf8mb4')
                ->setCollation('utf8mb4_general_ci')
                ->addColumn('id', 'integer', ['autoincrement' => true, 'signed' => false])
                ->addColumn('client_id', 'integer', ['signed' => false])
                ->addColumn('name', 'string', ['default' => '', 'length' => 50])
                ->addColumn('deleted', 'boolean', ['default' => false])
                ->addColumn('ctime', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
                ->addColumn('mtime', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
                ->create();
        }

        if (!$this->tableExists('mt_organizations')) {
            $this->table('mt_organizations', 'id')
                ->setCharset('utf8mb4')
                ->setCollation('utf8mb4_general_ci')
                ->addColumn('id', 'integer', ['autoincrement' => true, 'signed' => false])
                ->addColumn('name', 'string', ['default' => '', 'length' => 250])
                ->addColumn('deleted', 'boolean', ['default' => false])
                ->addColumn('ctime', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
                ->addColumn('mtime', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
                ->create();
        }

        if (!$this->tableExists('mt_permissions_actions')) {
            $this->table('mt_permissions_actions', 'id')
                ->setCharset('utf8mb4')
                ->setCollation('utf8mb4_general_ci')
                ->addColumn('id', 'integer', ['autoincrement' => true, 'signed' => false])
                ->addColumn('name', 'string', ['default' => '', 'length' => 50])
                ->create();
        }

        if (!$this->tableExists('mt_permissions_group_mgrs')) {
            $this->table('mt_permissions_group_mgrs', 'id')
                ->setCharset('utf8mb4')
                ->setCollation('utf8mb4_general_ci')
                ->addColumn('id', 'integer', ['autoincrement' => true, 'signed' => false])
                ->addColumn('user_id', 'integer')
                ->addColumn('group_id', 'integer')
                ->addColumn('level', 'enum', ['default' => 'any', 'length' => 0, 'decimals' => 0, 'values' => ['any', 'org', 'client', 'self']])
                ->addColumn('action_id', 'integer')
                ->addColumn('resource_id', 'integer')
                ->addColumn('allow_deny', 'enum', ['default' => 'allow', 'length' => 0, 'decimals' => 0, 'values' => ['allow', 'deny']])
                ->addIndex(['user_id', 'group_id', 'action_id', 'resource_id', 'level'], '', 'btree', 'by_user_group_action_resource_level')
                ->create();
        }

        if (!$this->tableExists('mt_permissions_resources')) {
            $this->table('mt_permissions_resources', 'id')
                ->setCharset('utf8mb4')
                ->setCollation('utf8mb4_general_ci')
                ->addColumn('id', 'integer', ['autoincrement' => true, 'signed' => false])
                ->addColumn('name', 'string', ['default' => '', 'length' => 50])
                ->create();
        }

        if (!$this->tableExists('mt_permissions_roles')) {
            $this->table('mt_permissions_roles', 'id')
                ->setCharset('utf8mb4')
                ->setCollation('utf8mb4_general_ci')
                ->addColumn('id', 'integer', ['autoincrement' => true, 'signed' => false])
                ->addColumn('role_id', 'integer')
                ->addColumn('level', 'enum', ['default' => 'any', 'length' => 0, 'decimals' => 0, 'values' => ['any', 'org', 'client', 'self']])
                ->addColumn('action_id', 'integer')
                ->addColumn('resource_id', 'integer')
                ->addColumn('allow_deny', 'enum', ['default' => 'allow', 'length' => 0, 'decimals' => 0, 'values' => ['allow', 'deny']])
                ->addIndex(['role_id', 'action_id', 'resource_id', 'level'], '', 'btree', 'by_role_action_resource_level')
                ->create();
        }

        if (!$this->tableExists('mt_permissions_users')) {
            $this->table('mt_permissions_users', 'id')
                ->setCharset('utf8mb4')
                ->setCollation('utf8mb4_general_ci')
                ->addColumn('id', 'integer', ['autoincrement' => true, 'signed' => false])
                ->addColumn('user_id', 'integer')
                ->addColumn('level', 'enum', ['default' => 'any', 'length' => 0, 'decimals' => 0, 'values' => ['any', 'org', 'client', 'self']])
                ->addColumn('action_id', 'integer')
                ->addColumn('resource_id', 'integer')
                ->addColumn('allow_deny', 'enum', ['default' => 'allow', 'length' => 0, 'decimals' => 0, 'values' => ['allow', 'deny']])
                ->addIndex(['user_id', 'action_id', 'resource_id', 'level'], '', 'btree', 'by_user_action_resource_level')
                ->create();
        }

        if (!$this->tableExists('mt_roles')) {
            $this->table('mt_roles', 'id')
                ->setCharset('utf8mb4')
                ->setCollation('utf8mb4_general_ci')
                ->addColumn('id', 'integer', ['autoincrement' => true, 'signed' => false])
                ->addColumn('name', 'string', ['default' => '', 'length' => 50])
                ->create();
        }

        if (!$this->tableExists('mt_sso_identity_external')) {
            $this->table('mt_sso_identity_external', 'id')
                ->setCharset('utf8mb4')
                ->setCollation('utf8mb4_general_ci')
                ->addColumn('id', 'integer', ['autoincrement' => true, 'signed' => false])
                ->addColumn('sso_provider', 'string', ['null' => true, 'length' => 50])
                ->addColumn('sso_identifier', 'string', ['default' => ''])
                ->addColumn('email', 'string', ['null' => true])
                ->addColumn('email_verified', 'boolean', ['default' => false])
                ->addColumn('name', 'string', ['null' => true])
                ->addColumn('given_name', 'string', ['null' => true])
                ->addColumn('family_name', 'string', ['null' => true])
                ->addColumn('locale', 'string', ['null' => true, 'length' => 10])
                ->addColumn('picture_uri', 'string', ['null' => true])
                ->addIndex('sso_identifier', '', 'btree', 'by_ssoid')
                ->addColumn('ctime', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
                ->addColumn('mtime', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
                ->create();
        }

        if (!$this->tableExists('mt_sso_identity_internal')) {
            $this->table('mt_sso_identity_internal', 'id')
                ->setCharset('utf8mb4')
                ->setCollation('utf8mb4_general_ci')
                ->addColumn('id', 'integer', ['autoincrement' => true, 'signed' => false])
                ->addColumn('username', 'string', ['default' => ''])
                ->addColumn('password', 'string', ['default' => ''])
                ->addColumn('mobile', 'string', ['null' => true, 'length' => 20])
                ->addColumn('email', 'string', ['null' => true, 'default' => ''])
                ->addColumn('first_name', 'string', ['null' => true])
                ->addColumn('last_name', 'string', ['null' => true])
                ->addIndex('username', '', 'btree', 'by_username')
                ->addColumn('ctime', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
                ->addColumn('mtime', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
                ->create();
        }

        if (!$this->tableExists('mt_sso_identity_links')) {
            $this->table('mt_sso_identity_links', 'id')
                ->setCharset('utf8mb4')
                ->setCollation('utf8mb4_general_ci')
                ->addColumn('id', 'integer', ['autoincrement' => true, 'signed' => false])
                ->addColumn('user_id', 'integer', ['signed' => false])
                ->addColumn('sso_type', 'enum', ['default' => 'external', 'length' => 0, 'decimals' => 0, 'values' => ['internal', 'external']])
                ->addColumn('sso_id', 'string', ['default' => ''])
                ->addIndex(['sso_id', 'user_id'], '', 'btree', 'by_ssoid_user')
                ->addColumn('ctime', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
                ->addColumn('mtime', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
                ->create();
        }

        if (!$this->tableExists('mt_users')) {
            $this->table('mt_users', 'id')
                ->setCharset('utf8mb4')
                ->setCollation('utf8mb4_general_ci')
                ->addColumn('id', 'integer', ['autoincrement' => true, 'signed' => false])
                ->addColumn('client_id', 'integer', ['signed' => false])
                ->addColumn('group_id', 'integer', ['length' => 10, 'signed' => false])
                ->addColumn('first_name', 'string', ['default' => '', 'length' => 50])
                ->addColumn('last_name', 'string', ['null' => true, 'length' => 50])
                ->addColumn('enabled', 'boolean', ['default' => true])
                ->addColumn('deleted', 'boolean', ['default' => false])
                ->addColumn('ctime', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
                ->addColumn('mtime', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
                ->addColumn('atime', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
                ->addIndex(['client_id', 'id'], '', 'btree', 'by_org_user')
                ->create();
        }

        if (!$this->tableExists('mt_user_roles')) {
            $this->table('mt_user_roles', 'id')
                ->setCharset('utf8mb4')
                ->setCollation('utf8mb4_general_ci')
                ->addColumn('id', 'integer', ['autoincrement' => true, 'signed' => false])
                ->addColumn('user_id', 'integer')
                ->addColumn('role_id', 'integer')
                ->create();
        }
    
        $this->insert('mt_organizations', [
            [
                'id' => '1',
                'name' => 'Primary Organization',
                'deleted' => '0',
                'ctime' => '2022-12-25 00:09:44',
                'mtime' => '2022-12-25 00:09:44',
            ],
        ]);
    
        $this->insert('mt_clients', [
            [
                'id' => '1',
                'organization_id' => '1',
                'name' => 'Primary Client',
                'deleted' => '0',
                'ctime' => '2022-12-25 00:09:44',
                'mtime' => '2022-12-25 00:09:44',
            ],
        ]);
        
        $this->insert('mt_permissions_actions', [
            [
                'id' => '1',
                'name' => 'any',
            ],
        ]);
    
        $this->insert('mt_permissions_resources', [
            [
                'id' => '1',
                'name' => 'any',
            ],
        ]);
    
        $this->insert('mt_roles', [
            [
                'id' => '1',
                'name' => 'org_admin',
            ],
        ]);
    
        $this->insert('mt_sso_identity_internal', [
            [
                'id' => '1',
                'username' => 'admin',
                'password' => '$2y$10$TT0NCYEGEhNg5BedgNeTgeU0NxrUQ95FMaB31EivaUoQkPhU/Xeua',
                'mobile' => '2085551111',
                'email' => '',
                'first_name' => 'Admin',
                'last_name' => '',
            ],
        ]);
    
        $this->insert('mt_sso_identity_links', [
            [
                'id' => '1',
                'user_id' => '1',
                'sso_type' => 'internal',
                'sso_id' => '1',
            ],
        ]);
    
        $this->insert('mt_users', [
            [
                'id' => '1',
                'client_id' => '1',
                'group_id' => '0',
                'first_name' => 'Admin',
                'last_name' => '',
                'enabled' => '1',
                'deleted' => '0',
                'ctime' => '2022-12-25 00:09:44',
                'mtime' => '2022-12-25 00:09:44',
                'atime' => '2022-12-25 00:09:44',
            ],
        ]);
        
    }

    protected function down(): void
    {
        $this->table('mt_clients')
            ->drop();

        $this->table('mt_fields')
            ->drop();

        $this->table('mt_field_values')
            ->drop();

        $this->table('mt_field_values_users')
            ->drop();

        $this->table('mt_groups')
            ->drop();

        $this->table('mt_organizations')
            ->drop();

        $this->table('mt_permissions_actions')
            ->drop();

        $this->table('mt_permissions_group_mgrs')
            ->drop();

        $this->table('mt_permissions_resources')
            ->drop();

        $this->table('mt_permissions_roles')
            ->drop();

        $this->table('mt_permissions_users')
            ->drop();

        $this->table('mt_roles')
            ->drop();

        $this->table('mt_sso_identity_external')
            ->drop();

        $this->table('mt_sso_identity_internal')
            ->drop();

        $this->table('mt_sso_identity_links')
            ->drop();

        $this->table('mt_users')
            ->drop();

        $this->table('mt_user_roles')
            ->drop();
    }
}
